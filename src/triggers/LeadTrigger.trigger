trigger LeadTrigger on Lead (after insert, after update, after delete) {
    LeadTriggerHandler lth = new LeadTriggerHandler(Trigger.oldMap, Trigger.new);
    lth.handleTrigger();
}