trigger AccountTrigger on Account (after insert, after update, after delete) {
    AccountTriggerHandler ath = new AccountTriggerHandler(Trigger.oldMap, Trigger.new);
    ath.handleTrigger();
}