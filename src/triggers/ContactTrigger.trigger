trigger ContactTrigger on Contact (after insert, after update, after delete) {
    ContactTriggerHandler cth = new ContactTriggerHandler(Trigger.oldMap, Trigger.new);
    cth.handleTrigger();
}