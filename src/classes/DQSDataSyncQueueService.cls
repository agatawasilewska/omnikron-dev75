public class DQSDataSyncQueueService {
	// Operation__c
	public final static String OPERATION_FIELD = 'Operation__c';
    public final static String OPERATION_UPSERT = 'Upsert';
	public final static String OPERATION_DELETE = 'Delete';
	private final static Set<String> OPERATION_VALID = new Set<String>{OPERATION_UPSERT, OPERATION_DELETE};

	// Object_Type__c
	public final static String OBJ_TYPE_FIELD = 'Object_Type__c';
	public final static String OBJ_TYPE_ACCOUNT = 'Account';
	public final static String OBJ_TYPE_CONTACT = 'Contact';
	public final static String OBJ_TYPE_LEAD = 'Lead';
	private final static Set<String> OBJ_TYPE_VALID = new Set<String>{OBJ_TYPE_ACCOUNT, OBJ_TYPE_CONTACT, OBJ_TYPE_LEAD};

	// Operation_Step__c
	public final static String OP_STEP_FIELD = 'Operation_Step__c';
	public final static String OP_STEP_QUEUED = 'Queued';
	public final static String OP_STEP_PROCESSING = 'Processing';
	private final static Set<String> OP_STEP_VALID = new Set<String>{OP_STEP_QUEUED, OP_STEP_PROCESSING};

	// Object_Id__c
	public final static String OBJ_ID_FIELD = 'Object_Id__c';

	// Select statements for DB operations
	private final static String SELECT_QUERY = 'SELECT Id, '
												+ OBJ_ID_FIELD + ', '
												+ OBJ_TYPE_FIELD + ', '
												+ OPERATION_FIELD + ', '
												+ OP_STEP_FIELD
												+ ' FROM DQS_Data_Sync_Queue__c';
	private final static String COUNT_QUERY = 'SELECT count() FROM DQS_Data_Sync_Queue__c';

	// Exception class
	public class DQSDataSyncQueueServiceException extends Exception {}

	// Get all records with OperationStep equal 'Queued'
	public static List<DQS_Data_Sync_Queue__c> getQueuedRecords() {
		return getRecords(null, null, OP_STEP_QUEUED, null);
	}

	// Get all records with OperationStep equal 'Processing'
	public static List<DQS_Data_Sync_Queue__c> getProcessingRecords() {
		return getRecords(null, null, OP_STEP_PROCESSING, null);
	}

	// Get all records, that match specified criterias
	public static List<DQS_Data_Sync_Queue__c> getRecords(String objectType, String operation, String operationStep, List<Id> ids) {
		return Database.query(buildQuery(SELECT_QUERY, objectType, operation, operationStep, ids));
	}

	// Get number of records with OperationStep equal 'Queued'
	public static Integer getQueuedRecordsCount() {
		return getRecordsCount(null, null, OP_STEP_QUEUED, null);
	}

	// Get number of records, that match specified criterias
	public static Integer getRecordsCount(String objectType, String operation, String operationStep, List<Id> ids) {
		return Database.countQuery(buildQuery(COUNT_QUERY, objectType, operation, operationStep, ids));
	}

	// Build query depending on given criterias
	private static String buildQuery(String querySelect, String objectType, String operation, String operationStep, List<Id> ids) {
		String query = querySelect;
		String args;

		if (objectType != null || operation != null || operationStep != null || (ids != null && !ids.isEmpty())) {
			query += ' WHERE';

			if (objectType != null) {
				if (!OBJ_TYPE_VALID.contains(objectType)) {
					throw new DQSDataSyncQueueServiceException('Invalid object type.');
				}
				if (!String.isEmpty(args)) {
					args += ' AND';
				}
				query += ' ' + OBJ_TYPE_FIELD + '=:objectType';
			}

			if (operation != null) {
				if (!OPERATION_VALID.contains(operation)) {
					throw new DQSDataSyncQueueServiceException('Invalid operation type.');
				}
				if (!String.isEmpty(args)) {
					args += ' AND';
				}
				query += ' ' + OPERATION_FIELD + '=:operation';
			}

			if (operationStep != null) {
				if (!OP_STEP_VALID.contains(operationStep)) {
					throw new DQSDataSyncQueueServiceException('Invalid operation step.');
				}
				if (!String.isEmpty(args)) {
					args += ' AND';
				}
				query += ' ' + OP_STEP_FIELD + '=:operationStep';
			}

			if ((ids != null && !ids.isEmpty())) {
				if (!String.isEmpty(args)) {
					args += ' AND';
				}
				query += ' ' + OBJ_ID_FIELD + ' IN :ids';
			}
		}

		if (!String.isEmpty(args)) {
					query += args;
		}

		return query;
	}

	// Upsert objects in DQS_Data_Sync_Queue__c table with OperationStep equal 'Queued'
	public static void queueObjects(List<Id> idList, String objectType, String operation) {
		if (!(OBJ_TYPE_VALID.contains(objectType) && OPERATION_VALID.contains(operation))) {
			throw new DQSDataSyncQueueServiceException('Invalid object type or operation.');
		}

		// Construct Id-QueueObject map using Object_Id__c as a key, for given Object_Type__c and Operation__c
		Map<Id, DQS_Data_Sync_Queue__c> queueItems = new Map<Id, DQS_Data_Sync_Queue__c>();
        for (DQS_Data_Sync_Queue__c queueObj : [ SELECT Id, Operation_Step__c, Object_Id__c
									        	 FROM DQS_Data_Sync_Queue__c
									        	 WHERE Operation__c=:operation
									        	   AND Object_Type__c=:objectType
									        	   AND Object_Id__c IN :idList ]) {
            if (!queueItems.containsKey(queueObj.Object_Id__c)) {
                queueItems.put(queueObj.Object_Id__c, queueObj);
            }
        }

        // Iterate given idList, change existing objects and add new ones
        for (Id objId : idList) {
            if (queueItems.containsKey(objId)) {
                DQS_Data_Sync_Queue__c dataQueueItem = queueItems.get(objId);
                dataQueueItem.Operation_Step__c = OP_STEP_QUEUED;
                queueItems.put(objId, dataQueueItem);
            } else {
                queueItems.put(objId, new DQS_Data_Sync_Queue__c(Operation__c = operation,
                                                                 Object_Type__c = objectType,
                                                                 Operation_Step__c = OP_STEP_QUEUED,
                                                                 Object_Id__c = objId));
            }
        }

        // Upsert records into DB
        upsert queueItems.values();
	}

	// Delete objects from DQS_Data_Sync_Queue__c table with OperationStep equal 'Processing'
	public static void cleanObjects(List<Id> idList, String objectType, String operation) {
		if (!(OBJ_TYPE_VALID.contains(objectType) && OPERATION_VALID.contains(operation))) {
			throw new DQSDataSyncQueueServiceException('Invalid object type or operation.');
		}

		List<DQS_Data_Sync_Queue__c> deleteQueue = [ SELECT Id
													 FROM DQS_Data_Sync_Queue__c
													 WHERE Operation_Step__c = :OP_STEP_PROCESSING
													   AND Operation__c = :operation
													   AND Object_Type__c = :objectType
													   AND Object_Id__c IN :idList ];
        delete deleteQueue;
	}

	// Add object to appropriate list, based on OperationStep
	public static void sortPerOperation(sObject obj, List<Id> upsertList, List<Id> deleteList) {
        Id objId = (Id)obj.get(OBJ_ID_FIELD);
        String operation = (String)obj.get(OPERATION_FIELD);
        if (OPERATION_VALID.contains(operation)) {
        	if (operation.equals(OPERATION_UPSERT)) {
        		upsertList.add(objId);
    		} else {
        		deleteList.add(objId);	
        	}
        }
    }
}