public class AccountService implements IRecordManager {
	private final String inputFieldsFieldsetName = 'Omikron_New_Account_Display_Fields';
	private static final String ADDRESS_VALIDATION_WF_NAME = 'RBWF_PostalCorrection';
	private static final String DUPLICATE_CHECK_ACCOUNT_WF_NAME = 'RBWF_DQ_Search_Account';
	private static final String DUPLICATE_CHECK_CONTACT_WF_NAME = 'RBWF_DQ_Search_Contact';
	private static final String DUPLICATE_CHECK_LEAD_WF_NAME = 'RBWF_DQ_Search_Lead';
	private static final String UPSERT_ACCOUNT_WF_NAME = 'RBWF_Upsert_Account';
	private static final String DELETE_ACCOUNT_WF_NAME = 'RBWF_Delete_Account';
	private static final String[] ADDRESS_VALIDATION_WF_FIELDS = new String[]{'Name', 'PostalCode','Street','CountryCode','City', 'State', 'DunsNumber'};
	private static final String UPSERT_ACCOUNT_FIELDS_FIELDSET_NAME = 'RBWF_Upsert_Account_Fields';

	public static AccountService instance {
		get {
			if (instance == null) {
				instance = new AccountService();
			}
			return instance;
		}
	}

	private AccountService() {}

	public Boolean isOmikronFieldChanged(SObject oldObj, SObject newObj) {
		Account oldAcc = convertToConcreteType(oldObj);
		Account newAcc = convertToConcreteType(newObj);

		System.assert(newAcc != null, 'Argument cannot be null.');

		if (oldAcc == null) {
			return true;
		}

        for (String dqsAccountFieldName : getUpsertFieldNames()) {
            if (oldAcc.get(dqsAccountFieldName) != newAcc.get(dqsAccountFieldName)) {
                return true;
            }
        }
        
        return false;
    }   

	public DQSWebServiceWrapper.Address createAddressObject(SObject obj) {
		Account acc = convertToConcreteType(obj);
		return new DQSWebServiceWrapper.Address(acc);
	}

	public void updateAddressWithDQSData(SObject obj, DQSWebServiceWrapper.AddressCheckResponseData dqsRecord) {
		Account acc = convertToConcreteType(obj);

		acc.put(DQSParams.instance.dqsAccountStreet, dqsRecord.street);
		acc.put(DQSParams.instance.dqsAccountZipCode, dqsRecord.postCode);
		acc.put(DQSParams.instance.dqsAccountCity, dqsRecord.city);
		acc.put(DQSParams.instance.dqsAccountCountry, dqsRecord.country);		
	}

	public DQSWebServiceWrapper.AddressCheckResponse validateAddress(SObject obj) {
		DQSWebServiceWrapper.Address addressObject = createAddressObject(obj);
		String[] values = new String[] {
			addressObject.name,
			addressObject.billingPostalCode,
			addressObject.billingStreet,
			addressObject.billingCountry,
			addressObject.billingCity,
			addressObject.billingState,
			addressObject.dunsNumber};

		DQSWebServiceWrapper ws = new DQSWebServiceWrapper(addressObject);
		return ws.validateAddress(ADDRESS_VALIDATION_WF_NAME, ADDRESS_VALIDATION_WF_FIELDS, values);		
	}

	public DQSWebServiceWrapper.DuplicateCheckResponse checkDuplicateInAccounts(SObject obj) {
		DQSWebServiceWrapper.Address addressObject = createAddressObject(obj);
		String[] values = new String[] {
			addressObject.name,
			addressObject.billingPostalCode,
			addressObject.billingStreet,
			addressObject.billingCountry,
			addressObject.billingCity,
			addressObject.billingState,
			addressObject.dunsNumber};

		DQSWebServiceWrapper ws = new DQSWebServiceWrapper(addressObject);
		return ws.checkDuplicate(DUPLICATE_CHECK_ACCOUNT_WF_NAME);		
	}

	public DQSWebServiceWrapper.DuplicateCheckResponse checkDuplicateInContacts(SObject obj) {
		DQSWebServiceWrapper.Address addressObject = createAddressObject(obj);
		String[] values = new String[] {
			addressObject.name,
			addressObject.billingPostalCode,
			addressObject.billingStreet,
			addressObject.billingCountry,
			addressObject.billingCity,
			addressObject.billingState,
			addressObject.dunsNumber};

		DQSWebServiceWrapper ws = new DQSWebServiceWrapper(addressObject);
		return ws.checkDuplicate(DUPLICATE_CHECK_CONTACT_WF_NAME);		
	}	

	public DQSWebServiceWrapper.DuplicateCheckResponse checkDuplicateInLeads(SObject obj) {
		DQSWebServiceWrapper.Address addressObject = createAddressObject(obj);
		String[] values = new String[] {
			addressObject.name,
			addressObject.billingPostalCode,
			addressObject.billingStreet,
			addressObject.billingCountry,
			addressObject.billingCity,
			addressObject.billingState,
			addressObject.dunsNumber};

		DQSWebServiceWrapper ws = new DQSWebServiceWrapper(addressObject);
		return ws.checkDuplicate(DUPLICATE_CHECK_LEAD_WF_NAME);		
	}	

	public List<DQSWebServiceWrapper.ModifyRecordResponse> upsertRecords(List<Id> ids) {
		List<DQSWebServiceWrapper.ModifyRecordResponse> resultList = new List<DQSWebServiceWrapper.ModifyRecordResponse>();

		for (sObject obj : AccountSelector.getRecords(ids)) {
			DQSWebServiceWrapper.ModifyRecordResponse result = upsertRecord(obj);
			resultList.add(result);
		}

		return resultList;
	}

	public DQSWebServiceWrapper.ModifyRecordResponse upsertRecord(SObject obj) {
		Account acc = convertToConcreteType(obj);
		String[] fields = new String[]{};
		String[] values = new String[]{};

		for (String field : getUpsertFieldNames()) {
			String fieldValue = String.valueOf(acc.get(field));
			if (!String.isBlank(fieldValue)) {
				fields.add(field);
				values.add(fieldValue);
			}
		}

		if (fields.size() == 0 || values.size() == 0) {
			return new DQSWebServiceWrapper.ModifyRecordResponse(new DQSWebService.ArrayOfArrayOfString(), UPSERT_ACCOUNT_WF_NAME);
		}

		DQSWebServiceWrapper ws = new DQSWebServiceWrapper();
		return ws.modifyRecord(UPSERT_ACCOUNT_WF_NAME, fields, values);
	}

	public List<DQSWebServiceWrapper.ModifyRecordResponse> deleteRecords(List<Id> ids) {
		List<DQSWebServiceWrapper.ModifyRecordResponse> resultList = new List<DQSWebServiceWrapper.ModifyRecordResponse>();

		for (Id objId : ids) {
			DQSWebServiceWrapper.ModifyRecordResponse result = deleteRecord(objId);
			resultList.add(result);
		}

		return resultList;
	}

	public DQSWebServiceWrapper.ModifyRecordResponse deleteRecord(Id objId) {
		String[] fields = new String[] { 'Id' };
		String[] values = new String[] { objId };

		DQSWebServiceWrapper ws = new DQSWebServiceWrapper();
		return ws.modifyRecord(DELETE_ACCOUNT_WF_NAME, fields, values);
	}

	public Boolean fieldExists(String fieldName) {
		Set<String> objectFields = Schema.SObjectType.Account.fields.getMap().keySet();
		return objectFields.contains(fieldName.toLowerCase());
	}
	
	public List<String> getInputFieldNames() {
		return SObjectUtility.getFieldSetFieldNames(inputFieldsFieldsetName, Account.SObjectType);
	}

	public List<String> getUpsertFieldNames() {
		return SObjectUtility.getFieldSetFieldNames(UPSERT_ACCOUNT_FIELDS_FIELDSET_NAME, Account.SObjectType);
	}

	private Account convertToConcreteType(SObject obj) {
		return (Account)obj;
	}

	public Boolean searchInAccounts() {
		return DQSParams.instance.accountSearchAccount;
	}

	public Boolean searchInContacts() {
		return DQSParams.instance.accountSearchContact;
	}
	
	public Boolean searchInLeads() {
		return DQSParams.instance.accountSearchLead;
	}

	public Boolean searchOnlyInMyRecords() {
		return DQSParams.instance.accountOnlyMyRecords;
	}

	public Boolean validateAddressNew() {
		return DQSParams.instance.avc_Account_New;
	}

	public Boolean validateAddressEdit() {
		return DQSParams.instance.avc_Account_Edit;
	}
}