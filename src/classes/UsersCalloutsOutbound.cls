/**
 @file      UsersCalloutsOutbound
 @author    Piotr Bardziński
 @version   1.0
 @date      2016-02-16
 @brief     Helper class to make callouts related to Users.
 @details
 @section   DESCRIPTION
 Omikron
*/
 
 public class UsersCalloutsOutbound {
    
    /*  Method to make (standard) REST API call to get User name for User Id for default configuration.
    *   @author Piotr Bardziński
    *   @date   2016-02-16
    */
    public static String getUserNameForUserId(ID userId) {
        return getUserNameForUserId(userId, CalloutSettings.CALLOUT_SFDC_KEY);
    }
    
    /*  Method to make (standard) REST API call to get User name for User Id.
    *   @author Piotr Bardziński
    *   @date   2016-02-16
    */
    public static String getUserNameForUserId(ID userId, String configurationName) {  

        HttpResponse response = CalloutsService.makeCallGET('/services/data/v35.0/sobjects/User/', configurationName, userId, null);
        system.debug('### response: ' + response);
        system.debug('### response.getBody(): ' + response.getBody());
        //JSONParser parser = JSON.createParser(response.getBody());
        //User us = (User)parser.readValueAs(User.class);
        User us = (User)JSON.deserialize(response.getBody(), User.class);
        system.debug('### us: ' + us);
        String userName = us.userName;
        system.debug('### userName: ' + userName);

        return userName;
    }
    
    /*  Method to make (custom) REST API call to get User name for the current logged-in User for default configuration.
    *   @author Piotr Bardziński
    *   @date   2016-02-16
    */
    public static ID getUserId() {
        return getUserId(CalloutSettings.CALLOUT_SFDC_KEY);
    }
    
    /*  Method to make (custom) REST API call to get User name for the current logged-in User.
    *   @author Piotr Bardziński
    *   @date   2016-02-16
    */
    public static ID getUserId(String configurationName) {
        String sessionId = CalloutsService.getAccessToken(configurationName);
        return getUserIdForSessionId(sessionId);
    }
    
    /*  Method to make (custom) REST API call to get User name for the Session Id.
    *   @author Piotr Bardziński
    *   @date   2016-02-16
    */
    public static ID getUserIdForSessionId(String sessionId) {  

        HttpResponse response = CalloutsService.makeCallGET('/services/apexrest/GetUserId/', null, (sObject)null, sessionId);
        system.debug('### response: ' + response);
        system.debug('### response.getBody(): ' + response.getBody());
        
        ID userId = (ID)JSON.deserialize(response.getBody(), ID.class);
        system.debug('### userId: ' + userId);

        return userId;
    }

    /*  Method to make (custom) REST API call to get User name for the Session Id.
    *   @author Piotr Bardziński
    *   @date   2016-02-16
    
    public static ID getUserIdForSessionIdByWebservice(String sessionIdToCheck) {  
        String sessionId = UserInfo.getSessionId();
        return getUserIdForSessionIdByWebservice(sessionId, sessionIdToCheck);
    }
    
    /*  Method to make (custom) REST API call to get User name for the Session Id.
    *   @author Piotr Bardziński
    *   @date   2016-02-16
    
    public static ID getUserIdForSessionIdByWebservice(String sessionId, String sessionIdToCheck) {  

        HttpResponse response = CalloutsService.makeCallGET('/services/apexrest/GetUserIdForSessionId?sessionId=', sessionIdToCheck, (sObject)null, sessionId);
        system.debug('### response: ' + response);
        system.debug('### response.getBody(): ' + response.getBody());
        
        ID userId = (ID)JSON.deserialize(response.getBody(), ID.class);
        system.debug('### userId: ' + userId);

        return userId;
    }*/
    
    /*  FFS 03 - Method to make an example REST API call to get Account Ids with selected User access.
    *   @author Piotr Bardziński
    *   @date   2016-02-16
    */
    public static set<ID> getAccountIdsForUserId_ExampleCall() {
        ID userId = '00524000001tXHu';
        map<ID, Account> mapAccountsIn = new map<ID, Account>([SELECT   ID
                                                                FROM    Account]);
                                                                //limit 2]);
        set<ID> sAccountsIn = mapAccountsIn.keySet();
        return getAccountIdsForUserId(userId, sAccountsIn);
        
    }

    /*  FFS 03 - Method to make REST API call to get Account Ids with selected User access for default configuration.
    *   @author Piotr Bardziński
    *   @date   2016-02-16
    */    
    public static set<ID> getAccountIdsForUserId(ID userId, set<ID> sAccountsIn) {
        return getAccountIdsForUserId(userId, sAccountsIn, CalloutSettings.CALLOUT_SFDC_KEY);
    }
    
    /*  FFS 03 - Method to make REST API call to get Account Ids with selected User access for specified configuration.
    *   @author Piotr Bardziński
    *   @date   2016-02-16
    */    
    public static set<ID> getAccountIdsForUserId(ID userId, set<ID> sAccountsIn, String configurationName) {
        
        system.debug('### userId: ' + userId);
        system.debug('### sAccountsIn: ' + sAccountsIN);
    
        String stringParameter;
        stringParameter = 'UserId=' + userId;
        stringParameter += '&AccountIds=';
        stringParameter += JSON.serialize(sAccountsIN);
        system.debug('### stringParameter: ' + stringParameter);
        
        HttpResponse response = CalloutsService.makeCallGET('/services/apexrest/GetAccountIdsForUserId?', configurationName, stringParameter, null);
        
        system.debug('### response: ' + response);
        
        String responseBody = response.getBody();

        system.debug('### responseBody: ' + responseBody);
        responseBody = responseBody.replace('\\', '');
        system.debug('### responseBody: ' + responseBody);
        responseBody = responseBody.removeStart('"').removeEnd('"');

        system.debug('### responseBody: ' + responseBody);
        
        set<ID> sAccountIdsOut = (set<ID>)JSON.deserialize(responseBody, Set<ID>.class);
        system.debug('### sAccountIdsOut: ' + sAccountIdsOut);
        
        return sAccountIdsOut;
    }
    
}