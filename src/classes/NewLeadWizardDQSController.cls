public with sharing class NewLeadWizardDQSController {
    public IRecordManager recordManager { get; protected set; }
    public SObject currentObject { get; protected set; }

    public NewLeadWizardDQSController(ApexPages.StandardController std) {
        recordManager = LeadService.instance;   
        currentObject = std.getRecord();
    }
}