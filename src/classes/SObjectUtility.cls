public without sharing class SObjectUtility {
    /**
     *  Method reads list of fields of the fieldset defined on specifis SObject.
     *  Method consumes and log all exceptions.
     *  @param fieldSetName name of the fieldset
     *  @param objectDescribe object that contain fieldset
     *  @return list of fields (Schema.FieldSetMember) from specific fieldset
     */
    public static List<Schema.FieldSetMember> getFieldSet(String fieldSetName, SObjectType objType) {
        if (fieldSetName != null && fieldSetName != '') {
            if (objType.getDescribe().FieldSets.getMap().get(fieldSetName) != null) {
                return objType.getDescribe().FieldSets.getMap().get(fieldSetName).getFields();
            }   
        }
        
        //  in case of any error empty list is return
        return new List<Schema.FieldSetMember>();
    }

    public static List<String> getFieldSetFieldNames(String fieldSetName, SObjectType objType) {
        List<Schema.FieldsetMember> fields = SObjectUtility.getFieldSet(fieldSetName, objType);
        List<String> res = new List<String>();

        for (Schema.FieldSetMember field : fields) {
            res.add(field.getFieldPath());
        }

        return res;
    }
}