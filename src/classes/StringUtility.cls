/**
 @file      StringUtility
 @author    Outbox Piotr Bardziński
 @version   2.01
 @date      2016-02-02
 @brief     Class used as helper in the system.
 @details
 @section DESCRIPTION
 Omikron
*/
public with sharing class StringUtility {
    // method to split semi-comma separated String
    public static set<String> getValuesFromString(String inputStr, String separator) {
        
        set<String> setOfValues = new set<String>();
        
        try {
            //setOfValues.addAll(inputStr.split(separator, 0));
            setOfValues.addAll(inputStr.trim().split('[\\s]*[' + separator + ']{1}[\\s]*', 0));
            
            return setOfValues;
        }
        catch (NullPointerException e) {
            system.debug('### getValuesFromString:setOfValues: ' + setOfValues);
            return new set<String>();
        }
    }
    
    // method to split semi-comma separated String
    public static set<String> getValuesFromString(String inputStr) {
        return getValuesFromStringScS(inputStr);
    }
    
    // method to split semi-comma separated String
    public static set<String> getValuesFromStringScS(String inputStr) {
        return getValuesFromString(inputStr, ';');
    }

    // method to split semi-comma separated String
    public static set<String> getValuesFromStringCS(String inputStr) {
        return getValuesFromString(inputStr, ',');
    }

    /*
    Method returns true if
    @string2Check contains any of @substrings
    @substrings is comma separated string
    */
    public static boolean stringContainsSubstring(String string2Check, String substrings) {
        Pattern substringsPattern = Pattern.compile('^.*?(' + substrings.replace(',', '|').trim() + ').*$');
        Matcher substringsMatcher = substringsPattern.matcher(string2Check);
            
        return substringsMatcher.matches();
    }
    
    public static Boolean stringContainsSubstrings(String string2Check, set<String> sSubstrings) {
        Boolean contains = false;
        for (String substring : sSubstrings) {
            if (string2Check.contains(substring)) {
                contains = true;
                break;
            }
        }

        return contains;
    }
    
    public static Map<String, Account> translateHitColumnValue(Set<String> hitColumnValues) {
        Map<String, Account> valueToTranslatorObject = new Map<String, Account>();

        for (String hitColumnValue : hitColumnValues) {
            valueToTranslatorObject.put(hitColumnValue, new Account(HitColumnTranslations__c = hitColumnValue));
        }

        return valueToTranslatorObject;
    }
}