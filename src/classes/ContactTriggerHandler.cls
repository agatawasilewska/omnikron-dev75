public class ContactTriggerHandler extends ChangeDataCaptureTriggerHandlerBase {
    public ContactTriggerHandler(Map<Id, Contact> oldContacts, List<Contact> newContactsList) {
        super(oldContacts, newContactsList, ContactService.instance);
    }

    protected override void handleBeforeInsert() {
    }

    protected override void handleBeforeUpdate() {
    }

    protected override void handleAfterDelete() {}
}