public class LeadTriggerHandler extends ChangeDataCaptureTriggerHandlerBase {
    public LeadTriggerHandler(Map<Id, Lead> oldLeads, List<Lead> newLeadsList) {
        super(oldLeads, newLeadsList, LeadService.instance);
    }

    protected override void handleBeforeInsert() {
    }

    protected override void handleBeforeUpdate() {
    }

    protected override void handleAfterDelete() {}
}