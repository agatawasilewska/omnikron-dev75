public class DQSConfigurationController{
    
    public DQSParams configRecord {get; set;}

    private final Id WHITELIST_PERMISSIONSET_ID = [SELECT Id FROM PermissionSet WHERE Name = 'DQS_Whitelist'].Id;

    private final Id SKIPADDRESS_PERMISSIONSET_ID = [SELECT Id FROM PermissionSet WHERE Name = 'DQS_Skip_AddressCheck'].Id;

    private final Id SKIPDUPLICATE_PERMISSIONSET_ID = [SELECT Id FROM PermissionSet WHERE Name = 'DQS_Skip_DuplicateCheck'].Id;

    private final String WHITELIST_CODE = 'WL';

    private final String SKIPADD_CODE = 'SAC';

    private final String SKIPDUPE_CODE = 'SDC';

    public DQSFieldMapping__c accountFieldMapping {get; set;}

    public DQSFieldMapping__c contactFieldMapping {get; set;}

    public DQSFieldMapping__c leadFieldMapping {get; set;}

    public List<SelectOption> accountFieldsToPick {get; private set;}

    public List<SelectOption> contactFieldsToPick {get; private set;}

    public List<SelectOption> leadFieldsToPick {get; private set;}

    //public List<PermissionSetAssignment> whitelistPermsForUsers {get; set;}

    public List<PermissionSetAssignmentWrapper> whitelistPermsForUsers {get; set;}

    public List<PermissionSetAssignmentWrapper> skipAddressPermsForUsers {get; set;}

    public List<PermissionSetAssignmentWrapper> skipDuplicatePermsForUsers {get; set;}

    public List<DQSHitClTranslationWrapper> hitColumnTranslation {get; set;}

    //public List<DQSHitClTranslation__c> hitColumnTranslation {get; set;}

    public List<DQSHitClTranslation__c> hitColumnTranslationsToRemove;

    public Integer noHCToRemove {get; set;}

    public Integer whitelistUsrToRemove {get; set;}

    public Task userFieldContainer {get; set;}

    public Id assigneeIdToRemove {get; set;}

    public String whitelistSet {get; set;}

    public Boolean renderHCT {get; private set;}

    public Boolean renderAddWlPopup {get; private set;}

    public Boolean renderAddSACPopup {get; private set;}

    public Boolean renderAddSDCPopup {get; private set;}

    public Boolean isAdmin {get; set;} 

    private static final String adminProfileName = 'System Administrator';

    //=====Constructors=====
    public DQSConfigurationController(){
        isAdmin = determineIfAdmin();
        setConfig();
    }

    //=====Methods====
    private void setConfig(){
        configRecord = DQSParams.instance; 
        renderHCT = true;
        renderAddWlPopup = false;
        renderAddSACPopup = false;
        renderAddSDCPopup = false;
        accountFieldsToPick = new List<SelectOption>();
        contactFieldsToPick = new List<SelectOption>();
        leadFieldsToPick = new List<SelectOption>();
        //whitelistPermsForUsers = new List<PermissionSetAssignment>([SELECT Id, AssigneeId, Assignee.Name FROM PermissionSetAssignment WHERE PermissionSetId  =: WHITELIST_PERMISSIONSET_ID]);
        hitColumnTranslationsToRemove = new List<DQSHitClTranslation__c>();
        whitelistPermsForUsers = new List<PermissionSetAssignmentWrapper>();
        skipAddressPermsForUsers = new List<PermissionSetAssignmentWrapper>();
        skipDuplicatePermsForUsers = new List<PermissionSetAssignmentWrapper>();

        for(PermissionSetAssignment psa : [SELECT Id, AssigneeId, Assignee.Name, PermissionSetId FROM PermissionSetAssignment WHERE PermissionSetId  =: WHITELIST_PERMISSIONSET_ID OR PermissionSetId  =: SKIPADDRESS_PERMISSIONSET_ID OR PermissionSetId  =: SKIPDUPLICATE_PERMISSIONSET_ID ]){
            if(psa.PermissionSetId == WHITELIST_PERMISSIONSET_ID) whitelistPermsForUsers.add(new PermissionSetAssignmentWrapper(psa, psa.Assignee.Name));
            if(psa.PermissionSetId == SKIPADDRESS_PERMISSIONSET_ID) skipAddressPermsForUsers.add(new PermissionSetAssignmentWrapper(psa, psa.Assignee.Name));
            if(psa.PermissionSetId == SKIPDUPLICATE_PERMISSIONSET_ID) skipDuplicatePermsForUsers.add(new PermissionSetAssignmentWrapper(psa, psa.Assignee.Name));
        }
        
        //hitColumnTranslation = new List<DQSHitClTranslation__c>(DQSHitClTranslation__c.getAll().values());
        //if(hitColumnTranslation.isEmpty()) hitColumnTranslation.add(new DQSHitClTranslation__c());

        hitColumnTranslation = refreshHitColumnTranslation();

        /*
        for(DQSHitClTranslation__c dht: DQSHitClTranslation__c.getAll().values()){
            hitColumnTranslation.add(new DQSHitClTranslationWrapper(dht));
        }*/

        if(hitColumnTranslation.isEmpty()) hitColumnTranslation.add(new DQSHitClTranslationWrapper());

        Schema.DescribeFieldResult dfr;

        accountFieldsToPick.add(new SelectOption('', '-None-'));
        contactFieldsToPick.add(new SelectOption('', '-None-'));
        leadFieldsToPick.add(new SelectOption('', '-None-'));
        for(Schema.SObjectField fs : Schema.SObjectType.Account.fields.getMap().values()){
            dfr = fs.getDescribe();
            accountFieldsToPick.add(new SelectOption(dfr.getName(), dfr.getLabel()));
        }
        for(Schema.SObjectField fs : Schema.SObjectType.Contact.fields.getMap().values()){
            dfr = fs.getDescribe();
            contactFieldsToPick.add(new SelectOption(dfr.getName(), dfr.getLabel()));
        }
        for(Schema.SObjectField fs : Schema.SObjectType.Lead.fields.getMap().values()){
            dfr = fs.getDescribe();
            leadFieldsToPick.add(new SelectOption(dfr.getName(), dfr.getLabel()));
        }

        
    }

    private List<DQSHitClTranslationWrapper> refreshHitColumnTranslation(){
        List<DQSHitClTranslationWrapper> refreshedHTCL = new List<DQSHitClTranslationWrapper>();
        for(DQSHitClTranslation__c dht: DQSParams.instance.mapHitClTranslations.values()){
            refreshedHTCL.add(new DQSHitClTranslationWrapper(dht));
        }
        return refreshedHTCL;
    }

    public void removeHCRecord(){
        System.debug('### noHCToRemove '+noHCToRemove);
        try{
            if(!hitColumnTranslation[noHCToRemove].isNew) hitColumnTranslationsToRemove.add(hitColumnTranslation[noHCToRemove].dqsHCTrans);
            hitColumnTranslation.remove(noHCToRemove);
        }catch(ListException e){
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 'Page didn\t passed value to controller - again :( '));
            System.debug('*** Page didn\t passed value to controller - again :( ***');
            System.debug('***'+e.getCause());
            System.debug('***'+e.getMessage());
            hitColumnTranslation.remove(hitColumnTranslation.size()-1);
        }
        if(hitColumnTranslation.size() == 0) renderHCT = false;
    }

    public void removeWLRecord(){
        Integer cnt = 0;
        for(PermissionSetAssignmentWrapper psaw : whitelistPermsForUsers){
            if(psaw.permissionSetAssignmentObject.AssigneeId == assigneeIdToRemove) break;
            cnt++;
        }
        removeRecordFromList(new RemoveReturn(cnt, 'WL'));
    }

    public void removeSACRecord(){
        Integer cnt = 0;
        for(PermissionSetAssignmentWrapper psaw : skipAddressPermsForUsers){
            if(psaw.permissionSetAssignmentObject.AssigneeId == assigneeIdToRemove) break;
            cnt++;
        }
        removeRecordFromList(new RemoveReturn(cnt, 'SAC'));
    }

    public void removeSDCRecord(){
        Integer cnt = 0;
        for(PermissionSetAssignmentWrapper psaw : skipDuplicatePermsForUsers){
            if(psaw.permissionSetAssignmentObject.AssigneeId == assigneeIdToRemove) break;
            cnt++;
        }
        removeRecordFromList(new RemoveReturn(cnt, 'SDC'));
    }

    private void removeRecordFromList(RemoveReturn retVal){
        System.debug('### RETURN VAL: '+retVal);
        try{
            if(retVal.wlListType == WHITELIST_CODE) whitelistPermsForUsers.remove(retVal.cnt);
            if(retVal.wlListType == SKIPADD_CODE) skipAddressPermsForUsers.remove(retVal.cnt);
            if(retVal.wlListType == SKIPDUPE_CODE) skipDuplicatePermsForUsers.remove(retVal.cnt);
            
        }catch(ListException e){
            //Add apex message
            System.debug('***'+e.getCause());
            System.debug('***'+e.getMessage());
        }
    }


    private String findWhichTable(Id assigneeId){

        return null;
    }

    public void removeWLRecordCP(){
        try{
            whitelistPermsForUsers.remove(whitelistUsrToRemove);
        }catch(ListException e){
            System.debug('***'+e.getCause());
            System.debug('***'+e.getMessage());
        }
    }

    private Boolean determineIfAdmin(){
        if([SELECT Name FROM Profile WHERE Id =:UserInfo.getProfileId()].Name == adminProfileName){
            return true;
        }
        return false;
    }

    public void saveGeneralConfig(){
        configRecord.saveConfig();
    }

    public void addNewHCConfig(){
        hitColumnTranslation.add(new DQSHitClTranslationWrapper());
        if(!renderHCT) renderHCT = true;
    }

    public Integer addNewWLUser(){
        Boolean flag = true;
        List<PermissionSetAssignmentWrapper> tempWhitelist;
        Id tempPermSetId;
        if(whitelistSet == WHITELIST_CODE) {tempWhitelist = whitelistPermsForUsers; tempPermSetId = WHITELIST_PERMISSIONSET_ID;}
        else if(whitelistSet == SKIPADD_CODE) {tempWhitelist = skipAddressPermsForUsers; tempPermSetId = SKIPADDRESS_PERMISSIONSET_ID;}
        else if(whitelistSet == SKIPDUPE_CODE) {tempWhitelist = skipDuplicatePermsForUsers; tempPermSetId = SKIPDUPLICATE_PERMISSIONSET_ID;}    

        if(!tempWhitelist.isEmpty()){
            for(PermissionSetAssignmentWrapper psaw : tempWhitelist){
                if(psaw.permissionSetAssignmentObject.AssigneeId == userFieldContainer.OwnerId){
                    flag = false;
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'User already has whitelist permission'));
                    return null;
                }
            }
        }

        if(flag && userFieldContainer.OwnerId != null){
            PermissionSetAssignmentWrapper newUser = new PermissionSetAssignmentWrapper(new PermissionSetAssignment(AssigneeId = userFieldContainer.OwnerId,
                PermissionSetId = tempPermSetId), [SELECT Name FROM User WHERE Id =: userFieldContainer.OwnerId].Name);
            /*whitelistPermsForUsers.add(new PermissionSetAssignmentWrapper(new PermissionSetAssignment(AssigneeId = userFieldContainer.OwnerId,
                PermissionSetId = WHITELIST_PERMISSIONSET_ID), [SELECT Name FROM User WHERE Id =: userFieldContainer.OwnerId].Name));*/
                if(whitelistSet == WHITELIST_CODE) {whitelistPermsForUsers.add(newUser); renderAddWlPopup = false;}
                else if(whitelistSet == SKIPADD_CODE) {skipAddressPermsForUsers.add(newUser); renderAddSACPopup = false;}
                else if(whitelistSet == SKIPDUPE_CODE) {skipDuplicatePermsForUsers.add(newUser); renderAddSDCPopup = false;}
            
        } else {
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'User field is empty'));
        }
        return null;
    }

    public void saveWLecords(){
         saveWhitelistRecords(WHITELIST_CODE);
    }

    public void saveSACRecords(){
         saveWhitelistRecords(SKIPADD_CODE);
    }

    public void saveSDCRecords(){
         saveWhitelistRecords(SKIPDUPE_CODE);
    }

    private void saveWhitelistRecords(String pemrType){
        Set<Id> usersIdForPerms = new Set<Id>{};
        Set<Id> existingUsers = new Set<Id>{};
        List<PermissionSetAssignmentWrapper> tempWhitelist;
        List<PermissionSetAssignment> permsToAdd = new List<PermissionSetAssignment>();
        List<PermissionSetAssignment> permsForExistingUsers;
        Id tempPermSetId;
        
        if(pemrType == WHITELIST_CODE) {tempWhitelist = whitelistPermsForUsers; tempPermSetId = WHITELIST_PERMISSIONSET_ID;}
        else if(pemrType == SKIPADD_CODE) {tempWhitelist = skipAddressPermsForUsers; tempPermSetId = SKIPADDRESS_PERMISSIONSET_ID;}
        else if(pemrType == SKIPDUPE_CODE) {tempWhitelist = skipDuplicatePermsForUsers; tempPermSetId = SKIPDUPLICATE_PERMISSIONSET_ID;}    
        for(PermissionSetAssignmentWrapper psaw : tempWhitelist){
            usersIdForPerms.add(psaw.permissionSetAssignmentObject.AssigneeId);
        }
        if(!usersIdForPerms.isEmpty()){
            //Add cascade removal of PermissionSetAssignment related records, otherwise delete DML will trip 
            //delete [SELECT Id FROM PermissionSetAssignment WHERE AssigneeId NOT IN :usersIdForPerms AND PermissionSetId =: WHITELIST_PERMISSIONSET_ID];
        }
        for(PermissionSetAssignment psa : [SELECT AssigneeId FROM PermissionSetAssignment WHERE AssigneeId IN :usersIdForPerms AND PermissionSetId =: tempPermSetId]){
            existingUsers.add(psa.AssigneeId);
        }
        for(PermissionSetAssignmentWrapper psaw : tempWhitelist){
            if(!existingUsers.contains(psaw.permissionSetAssignmentObject.AssigneeId)) permsToAdd.add(psaw.permissionSetAssignmentObject);
        }
        System.debug('### usersIdForPerms '+usersIdForPerms);
        System.debug('### tempPermSetId '+tempPermSetId);
        System.debug('### Current Whitelist Permissions Set '+tempWhitelist);
        System.debug('### Existing users Set '+existingUsers);
        System.debug('### Permission Set to Add List '+permsToAdd);
        if(!permsToAdd.isEmpty()){
            insert permsToAdd;
        }
    }

    public void addNewPopup(){
        userFieldContainer = new Task();
        if(whitelistSet == WHITELIST_CODE) {renderAddWlPopup = true;}
        else if(whitelistSet == SKIPADD_CODE) {renderAddSACPopup = true;}
        else if(whitelistSet == SKIPDUPE_CODE) {renderAddSDCPopup = true;}  
    }

    public void cancelNewPopup(){
        if(whitelistSet == WHITELIST_CODE) {renderAddWlPopup = false;}
        else if(whitelistSet == SKIPADD_CODE) {renderAddSACPopup = false;}
        else if(whitelistSet == SKIPDUPE_CODE) {renderAddSDCPopup = false;} 
    }

    public void saveHitColMap(){
        List<DQSHitClTranslation__c> insertHitClT = new List<DQSHitClTranslation__c>();
        List<DQSHitClTranslation__c> updateHitClT = new List<DQSHitClTranslation__c>();
        Boolean isEmptyValid = true;
        for(DQSHitClTranslationWrapper dqsWrap : hitColumnTranslation){
            if(dqsWrap.dqsHCTrans.Name == null || dqsWrap.dqsHCTrans.Value__c == null){
                isEmptyValid = false;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 'Each translation must have Name and Value!'));
            }
            if(dqsWrap.isNew){
                insertHitClT.add(dqsWrap.dqsHCTrans);
            }else{
                updateHitClT.add(dqsWrap.dqsHCTrans);
            }
        }
        if(isEmptyValid){
            try{
            System.debug('### insertHitClT '+insertHitClT);
            System.debug('### updateHitClT '+updateHitClT);
            System.debug('### hitColumnTranslationsToRemove '+hitColumnTranslationsToRemove);
            if(!insertHitClT.isEmpty()) insert insertHitClT;
            if(!updateHitClT.isEmpty()) update updateHitClT;
            if(!hitColumnTranslationsToRemove.isEmpty()) delete hitColumnTranslationsToRemove;
            } catch(DmlException e){
                System.debug(e.getMessage());
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Fatal DML Error Occured: ' + e.getMessage()));
            }
            hitColumnTranslationsToRemove.clear();
            hitColumnTranslation = refreshHitColumnTranslation();
        }
    }

    public PageReference redirectToDQSNamedCredential() {
        NamedCredential dqsConf;

        try {
            dqsConf = NamedCredentialSelector.getByDeveloperName('DQS');
            return URLUtility.constructIdURL(dqsConf.Id);
        } catch (NamedCredentialSelector.NamedCredentialException ncex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Named NamedCredentials not configured properly.'));
        } catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Fatal Error Occured: ' + ex.getMessage()));
        }

        return null;
    }

    public class PermissionSetAssignmentWrapper {
        public PermissionSetAssignment permissionSetAssignmentObject {get; set;}
        public String displayUsername {get; set;}

        public PermissionSetAssignmentWrapper(PermissionSetAssignment newPsa, String userName){
            permissionSetAssignmentObject = newPsa;
            displayUsername = userName;
        }
    }

    public class DQSHitClTranslationWrapper{
        public DQSHitClTranslation__c dqsHCTrans {get; set;}
        public Boolean isNew {get; set;}

        public DQSHitClTranslationWrapper(){
            isNew = true;
            dqsHCTrans = new DQSHitClTranslation__c();
        }

        public DQSHitClTranslationWrapper(DQSHitClTranslation__c inputRec){
            isNew = false;
            dqsHCTrans = inputRec;
        }
    }

    public class RemoveReturn{
        public Integer cnt;
        public String wlListType;

        public RemoveReturn(Integer c, String type){
            cnt = c;
            wlListType = type;
        }
    }
}