public class NamedCredentialSelector {
    public static NamedCredential getByDeveloperName(String name) {
        List<NamedCredential> nc = [SELECT Id, DeveloperName, MasterLabel FROM NamedCredential WHERE DeveloperName = :name];
        if (nc.size() == 1) {
            return nc[0];
        }

        throw new NamedCredentialException('Named Credentials select error for Developer Name \'' + name + '\'');
    }

    public class NamedCredentialException extends Exception {}
}