public class DQSDataSyncLogService {
    public final static String UPSERT_OPERATION = 'U';
    public final static String DELETE_OPERATION = 'D';

    /**
     * if trigger is called multiple times, one object is logged only once
     */
    private Map<String, Set<Id>> loggedObjects;

    public static DQSDataSyncLogService instance {
        get {
            if (instance == null) {
                instance = new DQSDataSyncLogService();
            }
            return instance;
        }
        protected set;
    }

    private DQSDataSyncLogService() {
        loggedObjects = new Map<String, Set<Id>>();
    }

    private boolean isObjectIdLogged(Id objectId, String operation) {
        Set<Id> loggedObjectIds = loggedObjects.get(operation);
        return (loggedObjectIds != null && loggedObjectIds.contains(objectId));
    }


    private void logObjectId(Id objectId, String operation) {
        Set<Id> loggedObjectIds = loggedObjects.get(operation);
        if (loggedObjectIds == null) {
            loggedObjectIds = new Set<Id>();
            loggedObjects.put(operation, loggedObjectIds);
        }
        loggedObjectIds.add(objectId);
    }

    private void logOperation(List<SObject> sObjects, String operation) {
        List<DQS_Data_Sync_Log__c> dataSyncLogs = new List<DQS_Data_Sync_Log__c>();
        for (SObject sObj : sObjects) {
            if (!isObjectIdLogged(sObj.Id, operation)) {
                dataSyncLogs.add(
                    new DQS_Data_Sync_Log__c(
                        Object_Id__c = sObj.Id,
                        CRUD_Operation__c = operation
                    )
                );

                logObjectId(sObj.Id, operation);
            }
        }

        insert dataSyncLogs;
    }

    public void logUpsert(List<SObject> sObjects) {
        logOperation(sObjects, UPSERT_OPERATION);
    }

    public void logDelete(List<SObject> sObjects) {
        logOperation(sObjects, DELETE_OPERATION);
    }

    public void logOperation(Id objId, String operation, String dqsStatus, String ffStatus) {
        DQS_Data_Sync_Log__c dataSyncLog = new DQS_Data_Sync_Log__c( Object_Id__c = objId,
                                                                     CRUD_Operation__c = operation,
                                                                     DQS_Status__c = dqsStatus,
                                                                     FF_Status__c = ffStatus);
        insert dataSyncLog;
    }
}