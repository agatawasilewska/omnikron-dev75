public class ObjectTestUtilities {
    private String objType;
    private Map<String, Schema.SObjectField> fieldMap;
    private static final Integer MAX_STRING_LEN = 300;

    public ObjectTestUtilities(String objType) {
        this.objType = objType;
        fieldMap = Schema.getGlobalDescribe().get(objType).getDescribe().fields.getMap();
    }

    public Schema.DisplayType getFieldType(String fieldName) {
        return fieldMap.get(fieldName).getDescribe().getType();
    }

    public Boolean isFieldEditable(String fieldName) {
        return fieldMap.get(fieldName).getDescribe().isUpdateable();
    }

    public Object getRandomFieldValue(String fieldName) {
        return getRandomValue(getFieldType(fieldName));
    }

    public static Object getRandomValue(Schema.DisplayType type) {
        if (type == Schema.DisplayType.Boolean) { return getRandomBoolean(); }
        if (type == Schema.DisplayType.Currency) { return getRandomDouble(); }
        if (type == Schema.DisplayType.Date) { return getRandomDate(); }
        if (type == Schema.DisplayType.DateTime) { return getRandomDatetime(); }
        if (type == Schema.DisplayType.Double) { return getRandomDouble(); }
        if (type == Schema.DisplayType.Email) { return getRandomEmail(); }
        if (type == Schema.DisplayType.Id) { return getRandomId(); }
        if (type == Schema.DisplayType.Integer) { return getRandomInteger(); }
        if (type == Schema.DisplayType.MultiPicklist) { return getRandomString(); }
        if (type == Schema.DisplayType.Percent) { return getRandomDouble(); }
        if (type == Schema.DisplayType.Phone) { return getRandomPhone(9); }
        if (type == Schema.DisplayType.Picklist) { return getRandomString(); }
        if (type == Schema.DisplayType.Reference) { return getRandomId(); }
        if (type == Schema.DisplayType.String) { return getRandomString(); }
        if (type == Schema.DisplayType.TextArea) { return getRandomString(); }
        if (type == Schema.DisplayType.Time) { return getRandomTime(); }
        if (type == Schema.DisplayType.URL) { return getRandomString(); }
        return null;
    }


    private static Boolean getRandomBoolean() {
        if (Math.signum(Math.random() - 0.5) > 0) {
            return true;
        }
        return false;
    }

    private static String getRandomString() {
        return getRandomString(Math.mod(Math.abs(getRandomInteger()), MAX_STRING_LEN));
    }

    private static String getRandomString(Integer len) {
        String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String[] result = new String[len];

        for (Integer idx = 0; idx < len; idx++) {
            Integer chr = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
            result[idx] = chars.substring(chr, chr + 1);
        }
        return String.join(result,'');
    }

    private static Integer getRandomInteger() {
        return Crypto.getRandomInteger();
    }

    private static Double getRandomDouble() {
        return Math.random();
    }

    private static Datetime getRandomDatetime() {
        return Datetime.newInstance(Crypto.getRandomLong());
    }

    private static Date getRandomDate() {
        return getRandomDatetime().date();
    }

    private static Time getRandomTime() {
        return getRandomDatetime().time();
    }

    private static String getRandomEmail() {
        String em = getRandomString(10);
        em += '@email.com';
        return em;
    }

    private static String getRandomIdStr(Integer len) {
        String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        String[] result = new String[len];

        for (Integer idx = 0; idx < len; idx++) {
            Integer chr = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
            result[idx] = chars.substring(chr, chr + 1);
        }
        return String.join(result,'');
    }

    private static Id getRandomId() {
        return '0012200000' + getRandomIdStr(8);
    }

    private static String getRandomPhone(Integer len) {
        return String.valueOf(getRandomIdStr(len));
    }
}