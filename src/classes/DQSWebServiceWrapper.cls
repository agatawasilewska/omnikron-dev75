public class DQSWebServiceWrapper {

    private static final Integer ADDRESS_CHECK_RESPONSE_IDX_STREET = 0;
    private static final Integer ADDRESS_CHECK_RESPONSE_IDX_POSTAL_CODE = 1;
    private static final Integer ADDRESS_CHECK_RESPONSE_IDX_CITY = 2;
    private static final Integer ADDRESS_CHECK_RESPONSE_IDX_COUNTRY = 3;
    private static final Integer ADDRESS_CHECK_RESPONSE_IDX_LATITUDE = 4;
    private static final Integer ADDRESS_CHECK_RESPONSE_IDX_LONGITUDE = 5;
    private static final Integer ADDRESS_CHECK_RESPONSE_IDX_PC_STATUS = 6;
    private static final Integer ADDRESS_CHECK_RESPONSE_IDX_PC_STATUS_LONG = 7;

    private static final Integer DUPLICATE_CHECK_RESPONSE_IDX_HIT_COLUMN = 0;
    private static final Integer DUPLICATE_CHECK_RESPONSE_IDX_AVG_SIMI = 1;
    private static final Integer DUPLICATE_CHECK_RESPONSE_IDX_ID = 2;
    private static final Integer DUPLICATE_CHECK_RESPONSE_IDX_NAME = 5;
    private static final Integer DUPLICATE_CHECK_RESPONSE_IDX_STREET = 8;
    private static final Integer DUPLICATE_CHECK_RESPONSE_IDX_POSTAL_CODE = 11;
    private static final Integer DUPLICATE_CHECK_RESPONSE_IDX_CITY = 9;
    private static final Integer DUPLICATE_CHECK_RESPONSE_IDX_COUNTRY = 12; 

    private static final String DUPLICATE_CHECK_WF_NAME = 'RBWF_DQ_Search_Account';
    private static final String[] DUPLICATE_FIELDS_VALIDATION_WF = new String[]{'Name','PostalCode','Street','CountryCode','City', 'State', 'DunsNumber', 'Phone', 'Fax','Website'};
    private static final String RESULT_TABLENAME = 'Results';
    private static final String STATUS_SEPARATOR = ' ';

    private static final List<String> STREET_MARKING_STATUSES = new List<String>{'FH', 'STR'};
    private static final String CITY_MARKING_STATUS = 'LOC';
    private static final String POSTAL_CODE_MARKING_STATUS = 'PCC';

    private Address addressObject;

    /*
    public DQSWebServiceWrapper(SObject sObjectInput) {
        //sObjectType = sObjectInput.getSObjectType().getDescribe().getName();
        addressObject = new Address(sObjectInput);
    }
    */

    public DQSWebServiceWrapper(Address objectAddress) {
        //sObjectType = sObjectInput.getSObjectType().getDescribe().getName();
        addressObject = objectAddress;
    }

    public DQSWebServiceWrapper() {
    }

    //======== Methods ==============
    
    public AddressCheckResponse validateAddress(String workflowName, String[] fieldNames, String[] fieldValues) {
        DQSWebService.ArrayOfString fieldNamesParam = new DQSWebService.ArrayOfString();
        DQSWebService.ArrayOfString records = new DQSWebService.ArrayOfString();
        DQSWebService.ArrayOfArrayOfString result;
        
        fieldNamesParam.string_x = fieldNames;
        records.string_x = fieldValues;
        DQSWebService.DQServerSoap dqsws = new DQSWebService.DQServerSoap(); 
        //try{
            System.debug('DQSWebServiceWrapper.validateAddress records=' + records);
            result = dqsws.DoWorkflowExtended(DQSParams.instance.userName, DQSParams.instance.password, workflowName, fieldNamesParam, records, RESULT_TABLENAME);
            System.debug('DQSWebServiceWrapper.validateAddress result=' + result);
        /*
        } catch(CalloutException e){
            System.debug(e.getMessage());
            return new AddressCheckResponse(false, 'Callout Exception Error: ' + e.getMessage());
        }*/
        return new AddressCheckResponse(result, workflowName);
    }

    public DuplicateCheckResponse checkDuplicate(String workflowName) {
        DQSWebService.ArrayOfString fieldNames = new DQSWebService.ArrayOfString();
        DQSWebService.ArrayOfString records = new DQSWebService.ArrayOfString();
        DQSWebService.ArrayOfArrayOfString result;
        fieldNames.string_x = DUPLICATE_FIELDS_VALIDATION_WF;
        records.string_x = new String[]{
            addressObject.name,
            addressObject.billingPostalCode,
            addressObject.billingStreet,
            addressObject.billingCountry,
            addressObject.billingCity,
            addressObject.billingState,
            addressObject.dunsNumber,
            addressObject.phone,
            addressObject.fax,
            addressObject.website
        };
        DQSWebService.DQServerSoap dqsws = new DQSWebService.DQServerSoap(); 
        //try {
            System.debug('DQSWebServiceWrapper.checkDuplicate records=' + records);
            result = dqsws.DoWorkflowExtended(DQSParams.instance.userName, DQSParams.instance.password, workflowName, fieldNames, records, RESULT_TABLENAME);
            System.debug('DQSWebServiceWrapper.checkDuplicate result=' + result);
        /*
        } catch(CalloutException e){
            System.debug(e.getMessage());
            return new DuplicateCheckResponse(false, 'Callout Exception Error: ' + e.getMessage());
        }*/
        return new DuplicateCheckResponse(result, DUPLICATE_CHECK_WF_NAME);
    }

    public ModifyRecordResponse modifyRecord(String workflowName, String[] fieldNames, String[] fieldValues) {
        system.debug('modifyRecord!');
        DQSWebService.ArrayOfString fieldNamesParam = new DQSWebService.ArrayOfString();
        DQSWebService.ArrayOfString records = new DQSWebService.ArrayOfString();
        DQSWebService.ArrayOfArrayOfString result;
        
        fieldNamesParam.string_x = fieldNames;
        records.string_x = fieldValues;
        DQSWebService.DQServerSoap dqsws = new DQSWebService.DQServerSoap(); 
        //try{
            System.debug('DQSWebServiceWrapper.modifyRecord fields=' + fieldNamesParam);
            System.debug('DQSWebServiceWrapper.modifyRecord values=' + records);
            result = dqsws.DoWorkflow(DQSParams.instance.userName, DQSParams.instance.password, workflowName, fieldNamesParam, records);
            System.debug('DQSWebServiceWrapper.modifyRecord result=' + result);
        /*
        } catch(CalloutException e){
            System.debug(e.getMessage());
            return new AddressCheckResponse(false, 'Callout Exception Error: ' + e.getMessage());
        }*/
        return new ModifyRecordResponse(result, workflowName);
    }

    //======== Helper Objects =======
    public virtual class DoWorkflowResponse {
        public Boolean resultFlag { get; protected set; }
        public String resultFlagDescription { get; protected set; }
        public List<String> headerFields = new List<String>();
        public List<List<String>> results = new List<List<String>>();

        public DoWorkflowResponse() {
            resultFlag = true;
        }

        public DoWorkflowResponse(Boolean resultFlag, String resultDescription) {
            this.resultFlagDescription = resultDescription;
            this.resultFlag = resultFlag;
        }

        public DoWorkflowResponse(DQSWebService.ArrayOfArrayOfString rawResponse, String workflow) {
            Integer counter = 0;

            this.resultFlag = true;
            if (rawResponse.ArrayOfString != null) {
                for (DQSWebService.ArrayOfString aos : rawResponse.ArrayOfString) {
                    if (counter > 0) this.results.add(new List<String>());

                    for (String s : aos.string_x) {
                        if (counter == 0) {
                            this.headerFields.add(s);
                        }
                        if (counter == 1 && (s == 'NOF' || s == 'nothing found')) {
                            this.resultFlag = false;
                            this.resultFlagDescription = 'Nothing found';
                            break;
                        }
                        if (counter > 0 && aos.string_x.size() > 2) {
                            //if(resultObject.results[counter] == null) resultObject.results.add(new List<String>());
                            this.results[counter-1].add(s);
                        }
                    }

                    counter++;
                }
            } else {
                this.resultFlag = false;
                this.resultFlagDescription = 'Nothing found';//'DQS Server WS Error: Response Array is null';
            }
        }       
    }

    public class AddressCheckResponse extends DoWorkflowResponse {
        public List<AddressCheckResponseData> responseData { get; protected set; }

        public AddressCheckResponse(Boolean resultFlag, String resultDescription) {
            super(resultFlag, resultDescription);
        }

        public AddressCheckResponse(DQSWebService.ArrayOfArrayOfString rawResponse, String workflow) {
            super(rawResponse, workflow);

            this.responseData = new List<AddressCheckResponseData>();
            for (Integer i = 0; i < this.results.size(); i++) {
                AddressCheckResponseData responseRecord = new AddressCheckResponseData(i, this.results[i]);
                responseData.add(responseRecord);
            }
        }
    }

    public class ModifyRecordResponse {
        public Boolean resultFlag { get; protected set; }
        public String resultFlagDescription { get; protected set; }
        public ModifyRecordResponseData responseData;
        
        public ModifyRecordResponse(Boolean resultFlag, String resultDescription) {
            this.resultFlag = resultFlag;
            this.resultFlagDescription = resultDescription;
        }

        public ModifyRecordResponse(DQSWebService.ArrayOfArrayOfString rawResponse, String workflow) {
            Integer counter = 0;

            this.resultFlag = false;
            List<List<String>> results = new List<List<String>>();
            if (rawResponse.ArrayOfString != null) {
                for (DQSWebService.ArrayOfString aos : rawResponse.ArrayOfString) {
                    if (!aos.string_x.isEmpty()) {
                        results.add(aos.string_x);
                    }
                }
            } 

            if (!results.isEmpty()) {
                responseData = new ModifyRecordResponseData(results);
            } else {
                responseData = new ModifyRecordResponseData();
            }

            if (responseData.resultDQ.equalsIgnoreCase(responseData.resultFF)) {
                String descStr = responseData.resultDQ;
                Boolean flag = true;
                if (descStr.equalsIgnoreCase('Error')) {
                    flag = false;
                }
                this.resultFlag = flag;
                this.resultFlagDescription = descStr;
            } else {
                this.resultFlag = false;
                this.resultFlagDescription = getMessage();
            }
        }

        public String getMessage() {
            return 'ResultDQ: \'' + responseData.resultDQ + '\', ResultFF: \'' + responseData.resultFF + '\'.';
        }
    }

    public class DuplicateCheckResponse extends DoWorkflowResponse {
        public List<DuplicateCheckResponseData> responseData { get; protected set; }

        public DuplicateCheckResponse(Boolean resultFlag, String resultDescription) {
            super(resultFlag, resultDescription);
        }

        public DuplicateCheckResponse(DQSWebService.ArrayOfArrayOfString rawResponse, String workflow) {
            super(rawResponse, workflow);

            this.responseData = new List<DuplicateCheckResponseData>();
            for (List<String> result : this.results) {
                responseData.add(new DuplicateCheckResponseData(result));
            }
        }

        public Set<String> getHitColumnValues() {
            Set<String> hitColumnValues = new Set<String>();

            for (DuplicateCheckResponseData responseDataRow : responseData) {
                hitColumnValues.add(responseDataRow.hitColumn);
            }

            return hitColumnValues;
        }
    }   

    public virtual class DoWorkflowResponseData {
        public Integer addressId { get; protected set; }
        public Id id { get; protected set; }        
        public String hitColumn { get; protected set; }
        public String name { get; protected set; }
        public Decimal avgSimi { get; protected set; }
        public String dunsNumber { get; protected set; }
        public String street { get; protected set; }
        public Boolean streetMarked { get; protected set; }
        public String postCode { get; protected set; }
        public Boolean postCodeMarked { get; protected set; }
        public String city { get; protected set; }
        public Boolean cityMarked { get; protected set; }
        public String country { get; protected set; }
        public String latitude { get; protected set; }
        public String longitude { get; protected set; }
        public String out_PC_Status { get; protected set; }
        public Set<String> out_PC_StatusSet { get; protected set; }
        public String out_PC_Status_Long { get; protected set; }

        public DoWorkflowResponseData() {}

        public DoWorkflowResponseData(DoWorkflowResponseData sourceData) {
            this.addressId = sourceData.addressId;
            this.id = sourceData.id;
            this.hitColumn = sourceData.hitColumn;
            this.name = sourceData.name;
            this.avgSimi = sourceData.avgSimi;
            this.dunsNumber = sourceData.dunsNumber;
            this.street = sourceData.street;
            this.streetMarked = sourceData.streetMarked;
            this.postCode = sourceData.postCode;
            this.postCodeMarked = sourceData.postCodeMarked;
            this.city = sourceData.city;
            this.cityMarked = sourceData.cityMarked;
            this.country = sourceData.country;
            this.latitude = sourceData.latitude;
            this.longitude = sourceData.longitude;
            this.out_PC_Status = sourceData.out_PC_Status;
            this.out_PC_StatusSet = sourceData.out_PC_StatusSet;
            this.out_PC_Status_Long = sourceData.out_PC_Status_Long;        
        }
    }

    public class AddressCheckResponseData extends DoWorkflowResponseData {
        public AddressCheckResponseData(Integer index, List<String> values) {
            //System.debug('AddressCheckResponseData values=' + values);
            addressId = index;
            street = values[ADDRESS_CHECK_RESPONSE_IDX_STREET];
            postCode = values[ADDRESS_CHECK_RESPONSE_IDX_POSTAL_CODE];
            city = values[ADDRESS_CHECK_RESPONSE_IDX_CITY];
            country = values[ADDRESS_CHECK_RESPONSE_IDX_COUNTRY];
            latitude = values[ADDRESS_CHECK_RESPONSE_IDX_LATITUDE];
            longitude = values[ADDRESS_CHECK_RESPONSE_IDX_LONGITUDE];

            //different lenght of response data
            //proper response
            //string_x=(Peter-Dörfler-Str. , 86199, Augsburg, DE, 0, 0, FH LOC DIS, wrong house number, locality corrected, district corrected)
            //unproper response
            //string_x=(Teststr. , 81000, Hamburg, Germany, 0, 0, NOF, nothing found)
            if (values.size() > ADDRESS_CHECK_RESPONSE_IDX_PC_STATUS) {
                out_PC_Status = values[ADDRESS_CHECK_RESPONSE_IDX_PC_STATUS];

                streetMarked = false;
                cityMarked = false;
                postCodeMarked = false;
                out_PC_StatusSet = new Set<String>();
                if (String.isNotBlank(out_PC_Status)) {
                    out_PC_StatusSet.addAll(out_PC_Status.split(STATUS_SEPARATOR));
                }
                if (out_PC_StatusSet.size() > 0) {
                    for (String markingStatus : STREET_MARKING_STATUSES) {
                        if (out_PC_StatusSet.contains(markingStatus)) {
                            streetMarked = true;
                        }
                    }
                    cityMarked = out_PC_StatusSet.contains(CITY_MARKING_STATUS);
                    postCodeMarked = out_PC_StatusSet.contains(POSTAL_CODE_MARKING_STATUS);
                }

                if (values.size() > ADDRESS_CHECK_RESPONSE_IDX_PC_STATUS_LONG) {
                    out_PC_Status_Long = values[ADDRESS_CHECK_RESPONSE_IDX_PC_STATUS_LONG];
                }
            }       
        }
    }

    public virtual class DuplicateCheckResponseData extends DoWorkflowResponseData {
        public DuplicateCheckResponseData(List<String> values) {
            if (DQSParams.instance.disableHitColumnTranslations) {
                this.hitColumn = values[DUPLICATE_CHECK_RESPONSE_IDX_HIT_COLUMN];
            } else {
                //translate
                this.hitColumn = values[DUPLICATE_CHECK_RESPONSE_IDX_HIT_COLUMN];
            }
            this.avgSimi = Decimal.valueOf(values[DUPLICATE_CHECK_RESPONSE_IDX_AVG_SIMI]);
            this.id = values[DUPLICATE_CHECK_RESPONSE_IDX_ID];
            this.name = values[DUPLICATE_CHECK_RESPONSE_IDX_NAME];
            this.street = values[DUPLICATE_CHECK_RESPONSE_IDX_STREET];
            this.postCode = values[DUPLICATE_CHECK_RESPONSE_IDX_POSTAL_CODE];
            this.city = values[DUPLICATE_CHECK_RESPONSE_IDX_CITY];
            this.country = values[DUPLICATE_CHECK_RESPONSE_IDX_COUNTRY];            
        }
    }

    public class ModifyRecordResponseData {
        public String resultDQ;
        public String resultFF;

        public ModifyRecordResponseData() {
            resultDQ = '';
            resultFF = '';
        }

        public ModifyRecordResponseData(List<List<String>> values) {
            resultDQ = '';
            resultFF = '';
            for (List<String> strArray : values) {
                if (strArray[0].equalsIgnoreCase('ResultDQ')) {
                    resultDQ = strArray[1];
                }

                if (strArray[0].equalsIgnoreCase('ResultFF')) {
                    resultFF = strArray[1];
                }
            }

        }

    }

    public class Address {
        public String HitColumn;
        public String AvgSimi;
        public String Id;
        public String IsDeleted;
        public String MasterRecordId;
        public String Name;
        public String FirstName;
        public String LastName;
        public String Email;
        public String MobilePhone;
        public String Type;
        public String ParentId;
        public String BillingStreet;
        public String BillingCity;
        public String BillingState;
        public String BillingPostalCode;
        public String BillingCountry;
        public String BillingLatitude;
        public String BillingLongitude;
        public String BillingAddress;
        public String ShippingStreet;
        public String ShippingCity;
        public String ShippingState;
        public String ShippingPostalCode;
        public String ShippingCountry;
        public String ShippingLatitude;
        public String ShippingLongitude;
        public String ShippingAddress;
        public String Phone;
        public String Fax;
        public String AccountNumber;
        public String Website;
        public String PhotoUrl;
        public String Sic;
        public String Industry;
        public String AnnualRevenue;
        public String NumberOfEmployees;
        public String Ownership;
        public String TickerSymbol;
        public String Description;
        public String Rating;
        public String Site;
        public String OwnerId;
        public String CreatedDate;
        public String CreatedById;
        public String LastModifiedDate;
        public String LastModifiedById;
        public String SystemModstamp;
        public String LastActivityDate;
        public String LastViewedDate;
        public String LastReferencedDate;
        public String Jigsaw;
        public String JigsawCompanyId;
        public String AccountSource;
        public String DunsNumber;
        public String Tradestyle;
        public String NaicsCode;
        public String NaicsDesc;
        public String YearStarted;
        public String SicDesc;
        public String DandbCompanyId;
        public String CustomerPriority;
        public String SLA;
        public String Active;
        public String NumberofLocations;
        public String UpsellOpportunity;
        public String SLASerialNumber;
        public String SLAExpirationDate;

        public Address(Account acc) {
            if (DQSParams.instance.dqsAccountName == null) {
                throw new DQSWebServiceWrapperException(Label.DQS_Account_Mapping_Empty_Name);
            }
            name = (String)acc.get(DQSParams.instance.dqsAccountName);
            billingPostalCode = DQSParams.instance.dqsAccountZipCode != null ? (String)acc.get(DQSParams.instance.dqsAccountZipCode) : null;
            billingStreet = DQSParams.instance.dqsAccountStreet != null ? (String)acc.get(DQSParams.instance.dqsAccountStreet) : null;
            billingCountry = DQSParams.instance.dqsAccountCountry != null ? (String)acc.get(DQSParams.instance.dqsAccountCountry) : null;
            billingCity = DQSParams.instance.dqsAccountCity != null ? (String)acc.get(DQSParams.instance.dqsAccountCity) : null;
            billingState = DQSParams.instance.dqsAccountStateProvince != null ? (String)acc.get(DQSParams.instance.dqsAccountStateProvince) : null;
            dunsNumber = DQSParams.instance.dqsAccountDUNSNumber != null ? (String)acc.get(DQSParams.instance.dqsAccountDUNSNumber) : null;         
            System.debug('### addressContainer: ' + this);          
        }

        public Address(Contact contact) {
            if (DQSParams.instance.dqsContactLastName == null) {
                throw new DQSWebServiceWrapperException(Label.DQS_Contact_Mapping_Empty_Name);
            }           
            firstName = DQSParams.instance.dqsContactFirstName != null ? (String)contact.get(DQSParams.instance.dqsContactFirstName) : null;
            lastName = (String)contact.get(DQSParams.instance.dqsContactLastName);
            billingPostalCode = DQSParams.instance.dqsContactZipCode != null ? (String)contact.get(DQSParams.instance.dqsContactZipCode) : null;
            billingStreet = DQSParams.instance.dqsContactStreet != null ? (String)contact.get(DQSParams.instance.dqsContactStreet) : null;
            billingCountry = DQSParams.instance.dqsContactCountry != null ? (String)contact.get(DQSParams.instance.dqsContactCountry) : null;
            billingCity = DQSParams.instance.dqsContactCity != null ? (String)contact.get(DQSParams.instance.dqsContactCity) : null;
            billingState = DQSParams.instance.dqsContactStateProvince != null ? (String)contact.get(DQSParams.instance.dqsContactStateProvince) : null;
            email = DQSParams.instance.dqsContactEmail != null ? (String)contact.get(DQSParams.instance.dqsContactEmail) : null;
            mobilePhone = DQSParams.instance.dqsContactMobile != null ? (String)contact.get(DQSParams.instance.dqsContactMobile) : null;
            System.debug('### addressContainer: ' + this);          
        }

        public Address(Lead lead) {
            if (DQSParams.instance.dqsLeadLastName == null) {
                throw new DQSWebServiceWrapperException(Label.DQS_Lead_Mapping_Empty_Name);
            }           
            firstName = DQSParams.instance.dqsLeadFirstName != null ? (String)lead.get(DQSParams.instance.dqsLeadFirstName) : null;
            lastName = (String)lead.get(DQSParams.instance.dqsLeadLastName);
            billingPostalCode = DQSParams.instance.dqsLeadZipCode != null ? (String)lead.get(DQSParams.instance.dqsLeadZipCode) : null;
            billingStreet = DQSParams.instance.dqsLeadStreet != null ? (String)lead.get(DQSParams.instance.dqsLeadStreet) : null;
            billingCountry = DQSParams.instance.dqsLeadCountry != null ? (String)lead.get(DQSParams.instance.dqsLeadCountry) : null;
            billingCity = DQSParams.instance.dqsLeadCity != null ? (String)lead.get(DQSParams.instance.dqsLeadCity) : null;
            billingState = DQSParams.instance.dqsLeadStateProvince != null ? (String)lead.get(DQSParams.instance.dqsLeadStateProvince) : null;
            email = DQSParams.instance.dqsLeadEmail != null ? (String)lead.get(DQSParams.instance.dqsLeadEmail) : null;
            mobilePhone = DQSParams.instance.dqsLeadMobile != null ? (String)lead.get(DQSParams.instance.dqsLeadMobile) : null;
            System.debug('### addressContainer: ' + this);  
        }

        public Address(SObject obj) {
            throw new DQSWebServiceWrapperException('Not implemented');
        }
    }

    public class DQSWebServiceWrapperException extends Exception {}
}