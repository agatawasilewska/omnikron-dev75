@RestResource(urlMapping='/GetUserIdForSessionId/*')

global with sharing class REST_GetUserIdForSessionId {

@HttpGet
  global static String GetUserIdForSessionId() {
    
    String sessionId = RestContext.request.params.get('sessionId');
    System.debug('### SessionId: ' + SessionId);
    
    ID userId = UsersCalloutsOutbound.getUserIdForSessionId(sessionId);
    System.debug('### userId: ' + userId);
    
    return userId;

  }
}