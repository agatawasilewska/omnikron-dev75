public class AccountTriggerHandler extends ChangeDataCaptureTriggerHandlerBase {
    public AccountTriggerHandler(Map<Id, Account> oldAccounts, List<Account> newAccountsList) {
        super(oldAccounts, newAccountsList, AccountService.instance);
    }

    private void putUpsertIntoDb() {
        putUpsertIntoDb(DQSDataSyncQueueService.OBJ_TYPE_ACCOUNT);
    }

    private void putDeleteIntoDb() {
        putDeleteIntoDb(DQSDataSyncQueueService.OBJ_TYPE_ACCOUNT);
    }

    protected override void handleAfterInsert() {
        if (Test.isRunningTest()) {
            return;
        }
        system.debug('handleAfterInsert');
        putUpsertIntoDb();
        if (!BatchSyncRecords.isRunning()) {
            Database.executeBatch(new BatchSyncRecords(), DQSParams.instance.batchSize);
        }
    }

    protected override void handleAfterUpdate() {
        if (Test.isRunningTest()) {
            return;
        }
        system.debug('handleAfterUpdate');
        putUpsertIntoDb();
        if (!BatchSyncRecords.isRunning()) {
            Database.executeBatch(new BatchSyncRecords(), DQSParams.instance.batchSize);
        }
    }

    protected override void handleAfterDelete() {
        if (Test.isRunningTest()) {
            return;
        }
        system.debug('handleAfterDelete');
        putDeleteIntoDb();
        if (!BatchSyncRecords.isRunning()) {
            Database.executeBatch(new BatchSyncRecords(), DQSParams.instance.batchSize);
        }
    }
}