public without sharing class DQSParams {
    /* Exception */
    private class IntegrationParamsException extends Exception {}

    //Constant Values with param names

    private final String PN_ACCOUNT_SEARCH_ACCOUNT = 'Account_Search_Account';
    private final String PN_ACCOUNT_SEARCH_CONTACT = 'Account_Search_Contact';
    private final String PN_ACCOUNT_SEARCH_LEAD = 'Account_Search_Lead';
    private final String PN_CONTACT_SEARCH_ACCOUNT = 'Contact_Search_Account';
    private final String PN_CONTACT_SEARCH_CONTACT = 'Contact_Search_Contact';
    private final String PN_CONTACT_SEARCH_LEAD = 'Contact_Search_Lead';
    private final String PN_LEAD_SEARCH_ACCOUNT = 'Lead_Search_Account';
    private final String PN_LEAD_SEARCH_CONTACT = 'Lead_Search_Contact';
    private final String PN_LEAD_SEARCH_LEAD = 'Lead_Search_Lead';

    private final String PN_AVC_ACCOUNT_EDIT = 'AVC_Account_Edit';
    private final String PN_AVC_ACCOUNT_NEW = 'AVC_Account_New';
    private final String PN_AVC_CONTACT_EDIT = 'AVC_Contact_Edit';
    private final String PN_AVC_CONTACT_NEW = 'AVC_Contact_New';
    private final String PN_AVC_LEAD_EDIT = 'AVC_Lead_Edit';
    private final String PN_AVC_LEAD_NEW = 'AVC_Lead_New';

    private final String PN_ONLY_MY_RECORDS = 'Only_My_Records';

    private final String PN_ACCOUNT_SEARCH_ONLYMYREC = 'Account_Search_OnlyMyRec';
    private final String PN_CONTACT_SEARCH_ONLYMYREC = 'Contact_Search_OnlyMyRec';
    private final String PN_LEAD_SEARCH_ONLYMYREC = 'Lead_Search_OnlyMyRec';

    private final String PN_DQS_ENDPOINT = 'DQS_Endpoint';
    private final String PN_USERNAME = 'UserName';
    private final String PN_PASSWORD = 'Password';

    private final String PN_HIGHLIGHT_NEGATIVE = 'Highlight_Negative';
    private final String PN_HIGHLIGHT_NEUTRAL = 'Highlight_Neutral';
    private final String PN_HIGHLIGHT_POSITIVE = 'Highlight_Positive';

    private final String PN_DISABLETRANSLATIONS = 'disableTranslations';
    private final String PN_BATCH_SIZE = 'BatchSize';

    private final String PN_DQSACCOUNTCITY = 'DQSAccountCity';
    private final String PN_DQSACCOUNTCOUNTRY = 'DQSAccountCountry';
    private final String PN_DQSACCOUNTSTATEPROVINCE = 'DQSAccountStateProvince';
    private final String PN_DQSACCOUNTDUNSNUMBER = 'DQSAccountDUNSNumber';
    private final String PN_DQSACCOUNTNAME = 'DQSAccountName';
    private final String PN_DQSACCOUNTSTREET = 'DQSAccountStreet';
    private final String PN_DQSACCOUNTTAXID = 'DQSAccountTaxId';
    private final String PN_DQSACCOUNTVATID = 'DQSAccountVatId';
    private final String PN_DQSACCOUNTZIPCODE = 'DQSAccountZipCode';

    private final String PN_DQSCONTACTCITY = 'DQSContactCity';
    private final String PN_DQSCONTACTCOUNTRY = 'DQSContactCountry';
    private final String PN_DQSCONTACTSTATEPROVINCE = 'DQSContactStateProvince';
    private final String PN_DQSCONTACTEMAIL = 'DQSContactEmail';
    private final String PN_DQSCONTACTFIRSTNAME = 'DQSContactFirstName';
    private final String PN_DQSCONTACTLASTNAME = 'DQSContactLastName';
    private final String PN_DQSCONTACTMOBILE = 'DQSContactMobile';
    private final String PN_DQSCONTACTSTREET = 'DQSContactStreet';
    private final String PN_DQSCONTACTZIPCODE = 'DQSContactZipCode';

    private final String PN_DQSLEADCITY = 'DQSLeadCity';
    private final String PN_DQSLEADCOUNTRY = 'DQSLeadCountry';
    private final String PN_DQSLEADSTATEPROVINCE = 'DQSLeadStateProvince';
    private final String PN_DQSLEADEMAIL = 'DQSLeadEmail';
    private final String PN_DQSLEADFIRSTNAME = 'DQSLeadFirstName';
    private final String PN_DQSLEADLASTNAME = 'DQSLeadLastName';
    private final String PN_DQSLEADMOBILE = 'DQSLeadMobile';
    private final String PN_DQSLEADSTREET = 'DQSLeadStreet';
    private final String PN_DQSLEADZIPCODE = 'DQSLeadZipCode';
    private final String PN_DQSLEADLATITUDE = 'DQSLeadLatitude';
    private final String PN_DQSLEADLONGITUDE = 'DQSLeadLongitude';
    
    /*** Instance variables ***/
    private Map<String, DQSConfig__c> mapParams;
    private Map<String, DQSFieldMapping__c> mapFields;
    public  Map<String, DQSHitClTranslation__c> mapHitClTranslations;

    /*
    public Boolean accountSearchAccount {
        get{return convertStringToBool(mapParams.get('Account_Search_Account').Value__c);} 
        set{mapParams.get('Account_Search_Account').Value__c = convertBoolToString(value);}
    }*/

    public Boolean accountSearchAccount {
        get{return convertStringToBool(getMapParamsValueFromConfig(PN_ACCOUNT_SEARCH_ACCOUNT));} 
        set{setMapParamsValueOnConfig(PN_ACCOUNT_SEARCH_ACCOUNT,convertBoolToString(value));}
    }

    public Boolean accountSearchContact {
        get{ return convertStringToBool(getMapParamsValueFromConfig(PN_ACCOUNT_SEARCH_CONTACT));} 
        set{setMapParamsValueOnConfig(PN_ACCOUNT_SEARCH_CONTACT,convertBoolToString(value));}
    }
    
    public Boolean accountSearchLead {
        get{return convertStringToBool(getMapParamsValueFromConfig(PN_ACCOUNT_SEARCH_LEAD));} 
        set{setMapParamsValueOnConfig(PN_ACCOUNT_SEARCH_LEAD, convertBoolToString(value));}
    }
    
    public Boolean contactSearchAccount {
        get{return convertStringToBool(getMapParamsValueFromConfig(PN_CONTACT_SEARCH_ACCOUNT));} 
        set{setMapParamsValueOnConfig(PN_CONTACT_SEARCH_ACCOUNT, convertBoolToString(value));}
    }
    
    public Boolean contactSearchContact {
        get{return convertStringToBool(getMapParamsValueFromConfig(PN_CONTACT_SEARCH_CONTACT));} 
        set{setMapParamsValueOnConfig(PN_CONTACT_SEARCH_CONTACT, convertBoolToString(value));}
    }
        
    public Boolean contactSearchLead {
        get{return convertStringToBool(getMapParamsValueFromConfig(PN_CONTACT_SEARCH_LEAD));} 
        set{setMapParamsValueOnConfig(PN_CONTACT_SEARCH_LEAD, convertBoolToString(value));}
    }
        
    public Boolean leadSearchAccount {
        get{return convertStringToBool(getMapParamsValueFromConfig(PN_LEAD_SEARCH_ACCOUNT));} 
        set{setMapParamsValueOnConfig(PN_LEAD_SEARCH_ACCOUNT, convertBoolToString(value));}
    }
        
    public Boolean leadSearchContact {
        get{return convertStringToBool(getMapParamsValueFromConfig(PN_LEAD_SEARCH_CONTACT));} 
        set{setMapParamsValueOnConfig(PN_LEAD_SEARCH_CONTACT, convertBoolToString(value));}
    }

    public Boolean leadSearchLead {
        get{return convertStringToBool(getMapParamsValueFromConfig(PN_LEAD_SEARCH_LEAD));} 
        set{setMapParamsValueOnConfig(PN_LEAD_SEARCH_LEAD, convertBoolToString(value));}
    }

    public Boolean avc_Account_Edit {
        get{return convertStringToBool(getMapParamsValueFromConfig(PN_AVC_ACCOUNT_EDIT));} 
        set{setMapParamsValueOnConfig(PN_AVC_ACCOUNT_EDIT, convertBoolToString(value));}
    }

    public Boolean avc_Account_New {
        get{return convertStringToBool(getMapParamsValueFromConfig(PN_AVC_ACCOUNT_NEW));} 
        set{setMapParamsValueOnConfig(PN_AVC_ACCOUNT_NEW, convertBoolToString(value));}
    }

    public Boolean avc_Contact_Edit {
        get{return convertStringToBool(getMapParamsValueFromConfig(PN_AVC_CONTACT_EDIT));} 
        set{setMapParamsValueOnConfig(PN_AVC_CONTACT_EDIT, convertBoolToString(value));}
    }

    public Boolean avc_Contact_New {
        get{return convertStringToBool(getMapParamsValueFromConfig(PN_AVC_CONTACT_NEW));} 
        set{setMapParamsValueOnConfig(PN_AVC_CONTACT_NEW, convertBoolToString(value));}
    }

    public Boolean avc_Lead_Edit {
        get{return convertStringToBool(getMapParamsValueFromConfig(PN_AVC_LEAD_EDIT));} 
        set{setMapParamsValueOnConfig(PN_AVC_LEAD_EDIT, convertBoolToString(value));}
    }

    public Boolean avc_Lead_New {
        get{return convertStringToBool(getMapParamsValueFromConfig(PN_AVC_LEAD_NEW));} 
        set{setMapParamsValueOnConfig(PN_AVC_LEAD_NEW, convertBoolToString(value));}
    }

    public Boolean onlyMyRecords {
        get{return convertStringToBool(getMapParamsValueFromConfig(PN_ONLY_MY_RECORDS));} 
        set{setMapParamsValueOnConfig(PN_ONLY_MY_RECORDS,convertBoolToString(value));}
    }

     public Boolean accountOnlyMyRecords {
        get{return convertStringToBool(getMapParamsValueFromConfig(PN_ACCOUNT_SEARCH_ONLYMYREC));} 
        set{setMapParamsValueOnConfig(PN_ACCOUNT_SEARCH_ONLYMYREC, convertBoolToString(value));}
    }

     public Boolean contactOnlyMyRecords {
        get{return convertStringToBool(getMapParamsValueFromConfig(PN_CONTACT_SEARCH_ONLYMYREC));} 
        set{setMapParamsValueOnConfig(PN_CONTACT_SEARCH_ONLYMYREC, convertBoolToString(value));}
    }

     public Boolean leadOnlyMyRecords {
        get{return convertStringToBool(getMapParamsValueFromConfig(PN_LEAD_SEARCH_ONLYMYREC));} 
        set{setMapParamsValueOnConfig(PN_LEAD_SEARCH_ONLYMYREC, convertBoolToString(value));}
    }
    
    public String dqs_Endpoint {
        get{return getMapParamsValueFromConfig(PN_DQS_ENDPOINT);} 
        set{setMapParamsValueOnConfig(PN_DQS_ENDPOINT, value);}
    }
    
    public String userName {
        get{return getMapParamsValueFromConfig(PN_USERNAME);} 
        set{setMapParamsValueOnConfig(PN_USERNAME, value);}
    }
    
    public String password {
        get{return getMapParamsValueFromConfig(PN_PASSWORD);} 
        set{setMapParamsValueOnConfig(PN_PASSWORD, value);}
    }
        
    public String highlight_Negative {
        get{return getMapParamsValueFromConfig(PN_HIGHLIGHT_NEGATIVE);} 
        set{setMapParamsValueOnConfig(PN_HIGHLIGHT_NEGATIVE, value);}
    }
        
    public String highlight_Neutral {
        get{return getMapParamsValueFromConfig(PN_HIGHLIGHT_NEUTRAL);} 
        set{setMapParamsValueOnConfig(PN_HIGHLIGHT_NEUTRAL, value);}
    }
        
    public String highlight_Positive {
        get{return getMapParamsValueFromConfig(PN_HIGHLIGHT_POSITIVE);} 
        set{setMapParamsValueOnConfig(PN_HIGHLIGHT_POSITIVE, value);}
    }

    public Boolean disableHitColumnTranslations {
        get{return convertStringToBool(getMapParamsValueFromConfig(PN_DISABLETRANSLATIONS));} 
        set{setMapParamsValueOnConfig(PN_DISABLETRANSLATIONS, convertBoolToString(value));}
    }

    public Integer batchSize {
        get{return Integer.valueOf(getMapParamsValueFromConfig(PN_BATCH_SIZE));} 
        set{setMapParamsValueOnConfig(PN_BATCH_SIZE, String.valueOf(value));}
    }

    public List<String> dqsAccountFields {
        get {
            if (dqsAccountFields == null) {
                dqsAccountFields = new List<String>();

                if (String.isNotBlank(dqsAccountCity)) {
                    dqsAccountFields.add(dqsAccountCity);
                }
                if (String.isNotBlank(dqsAccountCountry)) {
                    dqsAccountFields.add(dqsAccountCountry);
                }
                if (String.isNotBlank(dqsAccountStateProvince)) {
                    dqsAccountFields.add(dqsAccountStateProvince);
                }
                if (String.isNotBlank(dqsAccountDUNSNumber)) {
                    dqsAccountFields.add(dqsAccountDUNSNumber);
                }
                if (String.isNotBlank(dqsAccountName)) {
                    dqsAccountFields.add(dqsAccountName);
                }
                if (String.isNotBlank(dqsAccountStreet)) {
                    dqsAccountFields.add(dqsAccountStreet);
                }
                if (String.isNotBlank(dqsAccountTaxId)) {
                    dqsAccountFields.add(dqsAccountTaxId);
                }
                if (String.isNotBlank(dqsAccountVatId)) {
                    dqsAccountFields.add(dqsAccountVatId);
                }
                if (String.isNotBlank(dqsAccountZipCode)) {
                    dqsAccountFields.add(dqsAccountZipCode);
                }

                System.debug('DQSParams.dqsAccountFields accountFields=' +  dqsAccountFields);
            }    
            
            return dqsAccountFields;                
        }
        protected set;
    } 


    //============ Field Mapping Data ==========================
    public String dqsAccountCity { get{return  getMapFieldsValueFromConfig(PN_DQSACCOUNTCITY);} set{setMapFieldsValueOnConfig(PN_DQSACCOUNTCITY, value);}}
    public String dqsAccountCountry { get{return getMapFieldsValueFromConfig(PN_DQSACCOUNTCOUNTRY);} set{setMapFieldsValueOnConfig(PN_DQSACCOUNTCOUNTRY, value);}}
    public String dqsAccountStateProvince { get{return getMapFieldsValueFromConfig(PN_DQSACCOUNTSTATEPROVINCE);} set{setMapFieldsValueOnConfig(PN_DQSACCOUNTSTATEPROVINCE, value);}}
    public String dqsAccountDUNSNumber { get{return getMapFieldsValueFromConfig(PN_DQSACCOUNTDUNSNUMBER);} set{setMapFieldsValueOnConfig(PN_DQSACCOUNTDUNSNUMBER, value);}}
    public String dqsAccountName { get{return getMapFieldsValueFromConfig(PN_DQSACCOUNTNAME);} set{setMapFieldsValueOnConfig(PN_DQSACCOUNTNAME, value);}}
    public String dqsAccountStreet { get{return getMapFieldsValueFromConfig(PN_DQSACCOUNTSTREET);} set{setMapFieldsValueOnConfig(PN_DQSACCOUNTSTREET, value);}}
    public String dqsAccountTaxId { get{return getMapFieldsValueFromConfig(PN_DQSACCOUNTTAXID);} set{setMapFieldsValueOnConfig(PN_DQSACCOUNTTAXID, value);}}
    public String dqsAccountVatId { get{return getMapFieldsValueFromConfig(PN_DQSACCOUNTVATID);} set{setMapFieldsValueOnConfig(PN_DQSACCOUNTVATID, value);}}
    public String dqsAccountZipCode { get{return getMapFieldsValueFromConfig(PN_DQSACCOUNTZIPCODE);} set{setMapFieldsValueOnConfig(PN_DQSACCOUNTZIPCODE, value);}}

    public List<String> dqsContactFields {
        get {
            if (dqsContactFields == null) {
                dqsContactFields = new List<String>();

                if (String.isNotBlank(dqsContactCity)) {
                    dqsContactFields.add(dqsContactCity);
                }
                if (String.isNotBlank(dqsContactCountry)) {
                    dqsContactFields.add(dqsContactCountry);
                }
                if (String.isNotBlank(dqsContactStateProvince)) {
                    dqsContactFields.add(dqsContactStateProvince);
                }
                if (String.isNotBlank(dqsContactEmail)) {
                    dqsContactFields.add(dqsContactEmail);
                }
                if (String.isNotBlank(dqsContactFirstName)) {
                    dqsContactFields.add(dqsContactFirstName);
                }
                if (String.isNotBlank(dqsContactLastName)) {
                    dqsContactFields.add(dqsContactLastName);
                }
                if (String.isNotBlank(dqsContactMobile)) {
                    dqsContactFields.add(dqsContactMobile);
                }
                if (String.isNotBlank(dqsContactStreet)) {
                    dqsContactFields.add(dqsContactStreet);
                }
                if (String.isNotBlank(dqsContactZipCode)) {
                    dqsContactFields.add(dqsContactZipCode);
                }
            }    
            
            return dqsContactFields;                
        }
        protected set;
    }

    public String dqsContactCity { get{return getMapFieldsValueFromConfig(PN_DQSCONTACTCITY);} set{setMapFieldsValueOnConfig(PN_DQSCONTACTCITY, value);}}
    public String dqsContactCountry { get{return getMapFieldsValueFromConfig(PN_DQSCONTACTCOUNTRY);} set{setMapFieldsValueOnConfig(PN_DQSCONTACTCOUNTRY, value);}}
    public String dqsContactStateProvince { get{return getMapFieldsValueFromConfig(PN_DQSCONTACTSTATEPROVINCE);} set{setMapFieldsValueOnConfig(PN_DQSCONTACTSTATEPROVINCE, value);}}
    public String dqsContactEmail { get{return getMapFieldsValueFromConfig(PN_DQSCONTACTEMAIL);} set{setMapFieldsValueOnConfig(PN_DQSCONTACTEMAIL,value );}}
    public String dqsContactFirstName { get{return getMapFieldsValueFromConfig(PN_DQSCONTACTFIRSTNAME);} set{setMapFieldsValueOnConfig(PN_DQSCONTACTFIRSTNAME, value);}}
    public String dqsContactLastName { get{return getMapFieldsValueFromConfig(PN_DQSCONTACTLASTNAME);} set{setMapFieldsValueOnConfig(PN_DQSCONTACTLASTNAME, value);}}
    public String dqsContactMobile { get{return getMapFieldsValueFromConfig(PN_DQSCONTACTMOBILE);} set{setMapFieldsValueOnConfig(PN_DQSCONTACTMOBILE, value);}}
    public String dqsContactStreet { get{return getMapFieldsValueFromConfig(PN_DQSCONTACTSTREET);} set{setMapFieldsValueOnConfig(PN_DQSCONTACTSTREET, value);}}
    public String dqsContactZipCode { get{return getMapFieldsValueFromConfig(PN_DQSCONTACTZIPCODE);} set{setMapFieldsValueOnConfig(PN_DQSCONTACTZIPCODE, value);}}

    public List<String> dqsLeadFields {
        get {
            if (dqsLeadFields == null) {
                dqsLeadFields = new List<String>();

                if (String.isNotBlank(dqsLeadCity)) {
                    dqsLeadFields.add(dqsLeadCity);
                }
                if (String.isNotBlank(dqsLeadCountry)) {
                    dqsLeadFields.add(dqsLeadCountry);
                }
                if (String.isNotBlank(dqsLeadStateProvince)) {
                    dqsLeadFields.add(dqsLeadStateProvince);
                }
                if (String.isNotBlank(dqsLeadEmail)) {
                    dqsLeadFields.add(dqsLeadEmail);
                }
                if (String.isNotBlank(dqsLeadFirstName)) {
                    dqsLeadFields.add(dqsLeadFirstName);
                }
                if (String.isNotBlank(dqsLeadLastName)) {
                    dqsLeadFields.add(dqsLeadLastName);
                }
                if (String.isNotBlank(dqsLeadMobile)) {
                    dqsLeadFields.add(dqsLeadMobile);
                }
                if (String.isNotBlank(dqsLeadStreet)) {
                    dqsLeadFields.add(dqsLeadStreet);
                }
                if (String.isNotBlank(dqsLeadZipCode)) {
                    dqsLeadFields.add(dqsLeadZipCode);
                }
                if (String.isNotBlank(dqsLeadLatitude)) {
                    dqsLeadFields.add(dqsLeadLatitude);
                }
                if (String.isNotBlank(dqsLeadLongitude)) {
                    dqsLeadFields.add(dqsLeadLongitude);
                }
            }    
            
            return dqsLeadFields;                
        }
        protected set;
    }
        
    public String dqsLeadCity { get{return getMapFieldsValueFromConfig(PN_DQSLEADCITY);} set{setMapFieldsValueOnConfig(PN_DQSLEADCITY, value);}}
    public String dqsLeadCountry { get{return getMapFieldsValueFromConfig(PN_DQSLEADCOUNTRY);} set{setMapFieldsValueOnConfig(PN_DQSLEADCOUNTRY, value);}}
    public String dqsLeadStateProvince { get{return getMapFieldsValueFromConfig(PN_DQSLEADSTATEPROVINCE);} set{setMapFieldsValueOnConfig(PN_DQSLEADSTATEPROVINCE, value);}}
    public String dqsLeadEmail { get{return getMapFieldsValueFromConfig(PN_DQSLEADEMAIL);} set{setMapFieldsValueOnConfig(PN_DQSLEADEMAIL, value);}}
    public String dqsLeadFirstName { get{return getMapFieldsValueFromConfig(PN_DQSLEADFIRSTNAME);} set{setMapFieldsValueOnConfig(PN_DQSLEADFIRSTNAME, value);}}
    public String dqsLeadLastName { get{return getMapFieldsValueFromConfig(PN_DQSLEADLASTNAME);} set{setMapFieldsValueOnConfig(PN_DQSLEADLASTNAME, value);}}
    public String dqsLeadMobile { get{return getMapFieldsValueFromConfig(PN_DQSLEADMOBILE);} set{setMapFieldsValueOnConfig(PN_DQSLEADMOBILE, value);}}
    public String dqsLeadStreet { get{return getMapFieldsValueFromConfig(PN_DQSLEADSTREET);} set{setMapFieldsValueOnConfig(PN_DQSLEADSTREET, value);}}
    public String dqsLeadZipCode { get{return getMapFieldsValueFromConfig(PN_DQSLEADZIPCODE);} set{setMapFieldsValueOnConfig(PN_DQSLEADZIPCODE, value);}}
    public String dqsLeadLatitude {get{return getMapFieldsValueFromConfig(PN_DQSLEADLATITUDE);} set{setMapFieldsValueOnConfig(PN_DQSLEADLATITUDE, value);}}
    public String dqsLeadLongitude {get{return getMapFieldsValueFromConfig(PN_DQSLEADLONGITUDE);} set{setMapFieldsValueOnConfig(PN_DQSLEADLONGITUDE, value);}}
    
        
    /*** Instance methods ***/
    /* Constructors */
    public DQSParams () {
        if (Test.isRunningTest()) {
            mapParams = createDefaultMapParams();
            mapFields = createDefaultMapFields();
            mapHitClTranslations = createDefaultHitCLTranslations();
        } else {
            mapParams = DQSConfig__c.getAll();
            mapFields = DQSFieldMapping__c.getAll();
            mapHitClTranslations = DQSHitClTranslation__c.getAll();
        }
    }

    public Map<String, DQSConfig__c> getMapParams() {
        return mapParams;
    }

    public Map<String, DQSFieldMapping__c> getMapFields() {
        return mapFields;
    }
    
    /*** Other Static variables ***/

    public static DQSParams instance {// cache instance
        get {
            if (instance == null) {
                instance = new DQSParams();
            }
            return instance;
        }
    }
    
    /*** Static methods ***/
    public static String getMapParamsValueFromConfig(String name) {
        try {
            return instance.mapParams.get(name).Value__c;
        }
        catch (NullPointerException ex) {
            String errorMessage = 'getMapParamsValueFromConfig: There is no defined parameter: \'' + name + '\' Config Custom Setting.';
            System.debug(LoggingLevel.ERROR, errorMessage);
            throw new IntegrationParamsException(errorMessage);
            return null;
        }
    }

    public static void setMapParamsValueOnConfig(String name, String value){
        try {
            instance.mapParams.get(name).Value__c = value;
        }catch(NullPointerException ex){
            String errorMessage = 'MgetValueFromConfig: There is no defined parameter: \'' + name + '\' Config Parameter Custom Setting.';
            System.debug(LoggingLevel.ERROR, errorMessage);
            throw new IntegrationParamsException(errorMessage);
        }   
    }

    public static String getMapFieldsValueFromConfig(String name) {
        try {
            return instance.mapFields.get(name).Value__c;
        }
        catch (NullPointerException ex) {
            String errorMessage = 'getMapFieldsValueFromConfig: There is no defined parameter: \'' + name + '\' Config Custom Setting.';
            System.debug(LoggingLevel.ERROR, errorMessage);
            throw new IntegrationParamsException(errorMessage);
            return null;
        }
    }

    public static void setMapFieldsValueOnConfig(String name, String value){
        try {
            instance.mapFields.get(name).Value__c = value;
        }catch(NullPointerException ex){
            String errorMessage = 'setMapFieldsValueOnConfig: There is no defined parameter: \'' + name + '\' Config Parameter Custom Setting.';
            System.debug(LoggingLevel.ERROR, errorMessage);
            throw new IntegrationParamsException(errorMessage);
        }   
    }
    
    public static Integer deleteConfigParams() {
        List<DQSConfig__c> params = DQSConfig__c.getAll().values(); 
        if (params != null && params.size() > 0) {
            delete params;
            return params.size();
        }
        return 0;
    }

    public void saveConfig(){
        update instance.mapParams.values();
        System.debug(mapFields.values());
        update instance.mapFields.values();
    }

    //========= Helper Methods =======

    private String convertBoolToString(Boolean value){
        if(value == true) return 'true';
        if(value == false) return 'false';
        return null;
    }

    private Boolean convertStringToBool(String value){
        if(value == 'true') return true;
        if(value == 'false') return false;
        return null;
    }

    private static Map<String, DQSHitClTranslation__c> createDefaultHitCLTranslations() {
        Map<String, DQSHitClTranslation__c> resultMap = new Map<String, DQSHitClTranslation__c>();

        resultMap.put('2', new DQSHitClTranslation__c(Name='2', Value__c='Value 2'));
        resultMap.put('5', new DQSHitClTranslation__c(Name='5', Value__c='Value 5'));
        resultMap.put('7', new DQSHitClTranslation__c(Name='7', Value__c='Value 7'));

        return resultMap;
    }

    private static Map<String, DQSFieldMapping__c> createDefaultMapFields() {
        Map<String, DQSFieldMapping__c> resultMap = new Map<String, DQSFieldMapping__c>();

        resultMap.put('DQSAccountCity', new DQSFieldMapping__c(Name='DQSAccountCity', Value__c='BillingCity'));
        resultMap.put('DQSAccountCountry', new DQSFieldMapping__c(Name='DQSAccountCountry', Value__c='BillingCountry'));
        resultMap.put('DQSAccountDUNSNumber', new DQSFieldMapping__c(Name='DQSAccountDUNSNumber', Value__c=''));
        resultMap.put('DQSAccountName', new DQSFieldMapping__c(Name='DQSAccountName', Value__c='Name'));
        resultMap.put('DQSAccountStateProvince', new DQSFieldMapping__c(Name='DQSAccountStateProvince', Value__c=''));
        resultMap.put('DQSAccountStreet', new DQSFieldMapping__c(Name='DQSAccountStreet', Value__c='BillingStreet'));
        resultMap.put('DQSAccountTaxId', new DQSFieldMapping__c(Name='DQSAccountTaxId', Value__c=''));
        resultMap.put('DQSAccountVatId', new DQSFieldMapping__c(Name='DQSAccountVatId', Value__c=''));
        resultMap.put('DQSAccountZipCode', new DQSFieldMapping__c(Name='DQSAccountZipCode', Value__c='BillingPostalCode'));

        resultMap.put('DQSContactCity', new DQSFieldMapping__c(Name='DQSContactCity', Value__c='MailingCity'));
        resultMap.put('DQSContactCountry', new DQSFieldMapping__c(Name='DQSContactCountry', Value__c='MailingCountry'));
        resultMap.put('DQSContactEmail', new DQSFieldMapping__c(Name='DQSContactEmail', Value__c='Email'));
        resultMap.put('DQSContactFirstName', new DQSFieldMapping__c(Name='DQSContactFirstName', Value__c='FirstName'));
        resultMap.put('DQSContactLastName', new DQSFieldMapping__c(Name='DQSContactLastName', Value__c='LastName'));
        resultMap.put('DQSContactMobile', new DQSFieldMapping__c(Name='DQSContactMobile', Value__c='MobilePhone'));
        resultMap.put('DQSContactStateProvince', new DQSFieldMapping__c(Name='DQSContactStateProvince', Value__c=''));
        resultMap.put('DQSContactStreet', new DQSFieldMapping__c(Name='DQSContactStreet', Value__c='MailingStreet'));
        resultMap.put('DQSContactZipCode', new DQSFieldMapping__c(Name='DQSContactZipCode', Value__c='MailingPostalCode'));

        resultMap.put('DQSLeadCity', new DQSFieldMapping__c(Name='DQSLeadCity', Value__c='City'));
        resultMap.put('DQSLeadCountry', new DQSFieldMapping__c(Name='DQSLeadCountry', Value__c='Country'));
        resultMap.put('DQSLeadEmail', new DQSFieldMapping__c(Name='DQSLeadEmail', Value__c=''));
        resultMap.put('DQSLeadFirstName', new DQSFieldMapping__c(Name='DQSLeadFirstName', Value__c='FirstName'));
        resultMap.put('DQSLeadLastName', new DQSFieldMapping__c(Name='DQSLeadLastName', Value__c='LastName'));
        resultMap.put('DQSLeadMobile', new DQSFieldMapping__c(Name='DQSLeadMobile', Value__c='MobilePhone'));
        resultMap.put('DQSLeadStateProvince', new DQSFieldMapping__c(Name='DQSLeadStateProvince', Value__c=''));
        resultMap.put('DQSLeadStreet', new DQSFieldMapping__c(Name='DQSLeadStreet', Value__c='Street'));
        resultMap.put('DQSLeadZipCode', new DQSFieldMapping__c(Name='DQSLeadZipCode', Value__c='PostalCode'));
        resultMap.put('DQSLeadLatitude', new DQSFieldMapping__c(Name='DQSLeadLatitude', Value__c='Latitude'));
        resultMap.put('DQSLeadLongitude', new DQSFieldMapping__c(Name='DQSLeadLongitude', Value__c='Longitude'));

        return resultMap;
    }

    private static Map<String, DQSConfig__c> createDefaultMapParams() {
        Map<String, DQSConfig__c> resultMap = new Map<String, DQSConfig__c>();

        resultMap.put('Account_Search_Account', new DQSConfig__c(Name='Account_Search_Account', Value__c='true'));
        resultMap.put('Account_Search_Contact', new DQSConfig__c(Name='Account_Search_Contact', Value__c='true'));
        resultMap.put('Account_Search_Lead', new DQSConfig__c(Name='Account_Search_Lead', Value__c='true'));

        resultMap.put('Contact_Search_Account', new DQSConfig__c(Name='Contact_Search_Account', Value__c='true'));
        resultMap.put('Contact_Search_Contact', new DQSConfig__c(Name='Contact_Search_Contact', Value__c='true'));
        resultMap.put('Contact_Search_Lead', new DQSConfig__c(Name='Contact_Search_Lead', Value__c='true'));

        resultMap.put('Lead_Search_Account', new DQSConfig__c(Name='Lead_Search_Account', Value__c='true'));
        resultMap.put('Lead_Search_Contact', new DQSConfig__c(Name='Lead_Search_Contact', Value__c='true'));
        resultMap.put('Lead_Search_Lead', new DQSConfig__c(Name='Lead_Search_Lead', Value__c='true'));

        resultMap.put('Highlight_Positive', new DQSConfig__c(Name='Highlight_Positive', Value__c='228A22'));               
        resultMap.put('Highlight_Neutral', new DQSConfig__c(Name='Highlight_Neutral', Value__c='FFC3B2'));
        resultMap.put('Highlight_Negative', new DQSConfig__c(Name='Highlight_Negative', Value__c='75FFA3'));

        resultMap.put('AVC_Account_New', new DQSConfig__c(Name='AVC_Account_New', Value__c='true'));
        resultMap.put('AVC_Account_Edit', new DQSConfig__c(Name='AVC_Account_Edit', Value__c='true'));

        resultMap.put('AVC_Contact_New', new DQSConfig__c(Name='AVC_Contact_New', Value__c='true'));
        resultMap.put('AVC_Contact_Edit', new DQSConfig__c(Name='AVC_Contact_Edit', Value__c='true'));

        resultMap.put('AVC_Lead_New', new DQSConfig__c(Name='AVC_Lead_New', Value__c='true'));
        resultMap.put('AVC_Lead_Edit', new DQSConfig__c(Name='AVC_Lead_Edit', Value__c='true'));

        resultMap.put('Only_My_Records', new DQSConfig__c(Name='Only_My_Records', Value__c='false'));
        resultMap.put('Account_Search_OnlyMyRec', new DQSConfig__c(Name='Account_Search_OnlyMyRec', Value__c='false'));
        resultMap.put('Contact_Search_OnlyMyRec', new DQSConfig__c(Name='Contact_Search_OnlyMyRec', Value__c='false'));
        resultMap.put('Lead_Search_OnlyMyRec', new DQSConfig__c(Name='Lead_Search_OnlyMyRec', Value__c='false'));  

        resultMap.put('UserName', new DQSConfig__c(Name='UserName', Value__c='admin'));
        resultMap.put('Password', new DQSConfig__c(Name='Password', Value__c='admin'));

        resultMap.put('DQS_Endpoint', new DQSConfig__c(Name='DQS_Endpoint', Value__c='http://80.237.235.59/DQServer_PWC/2.0.0/DQServer.asmx'));
        resultMap.put('disableTranslations', new DQSConfig__c(Name='disableTranslations', Value__c='false'));
        resultMap.put('BatchSize', new DQSConfig__c(Name='BatchSize', Value__c='100'));

        return resultMap;
    }

    public static void initializeParams() {
        upsert createDefaultMapParams().values();
        upsert createDefaultMapFields().values();
        upsert createDefaultHitCLTranslations().values();
    }
}