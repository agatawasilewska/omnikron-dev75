global class BatchSyncRecords implements Database.Batchable<sObject> {

    private static final List<String> VALID_STATUS = new List<String>{'Queued', 'Preparing', 'Processing'};
    private static Id classId {
        get {
            if (classId == null) {
                classId = [SELECT Id FROM ApexClass WHERE Name='BatchSyncRecords'].Id;
            }
            return classId;
        }
        set;
    }

    public static Boolean isRunning() {
        return (Integer)[SELECT count() FROM AsyncApexJob WHERE ApexClassID = :classId AND Status IN :VALID_STATUS LIMIT 1] > 0;
    }

    global BatchSyncRecords() {}

    global Iterable<sObject> start(Database.BatchableContext BC){
        return DQSDataSyncQueueService.getQueuedRecords();
   }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
        for(sObject obj : scope) {
            obj.put(DQSDataSyncQueueService.OP_STEP_FIELD, DQSDataSyncQueueService.OP_STEP_PROCESSING);
        }

        update scope;
    }

   global void finish(Database.BatchableContext BC) {
        Database.executeBatch(new BatchSyncRecords2(), DQSParams.instance.batchSize);
   }

}