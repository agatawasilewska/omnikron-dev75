public with sharing class NewAccountWizardDQSController {
    public IRecordManager recordManager { get; protected set; }
    public SObject currentObject { get; protected set; }

    public NewAccountWizardDQSController(ApexPages.StandardController std) {
        recordManager = AccountService.instance;
        SObject obj = std.getRecord();
        List<Account> accs = AccountSelector.getRecords(new List<Id>{ obj.Id });    
        if (accs.isEmpty()) {
        	currentObject = obj;	
        } else {
        	currentObject = accs[0];	
        }
    }
}