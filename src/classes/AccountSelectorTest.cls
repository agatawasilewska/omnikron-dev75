@isTest
private class AccountSelectorTest {

    static testMethod void getRecordsNormal() {
        List<Account> accs = new List<Account>();
        for (Integer i = 0; i < 20; i++) {
            String accName = 'AccTest' + String.valueOf(i);
            accs.add(new Account(Name = accName));
        }

        Set<Id> chosenAccs = new Set<Id>();
        insert accs;

        for (Integer i = 0; i < accs.size() ; i += 2) {
            chosenAccs.add(accs.get(i).Id);
        }

        Test.startTest();
        for (sObject obj : AccountSelector.getRecords(new List<Id>(chosenAccs))) {
            System.assert(chosenAccs.contains(obj.Id));
        }
        Test.stopTest();
    }

    static testMethod void getRecordsEmptyResult() {
        List<Account> accs = new List<Account>();
        for (Integer i = 0; i < 20; i++) {
            String accName = 'AccTest' + String.valueOf(i);
            accs.add(new Account(Name = accName));
        }

        insert accs;

        Contact con = new Contact(FirstName = 'ContTest', LastName = 'ContTest');
        insert con;

        Test.startTest();
        System.assert(AccountSelector.getRecords(null).isEmpty());
        System.assert(AccountSelector.getRecords(new List<Id>()).isEmpty());
        System.assert(AccountSelector.getRecords(new List<Id>{ con.Id }).isEmpty());
        Test.stopTest();
    }
}