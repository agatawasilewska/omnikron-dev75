public class LeadService implements IRecordManager {
    private final String inputFieldsFieldsetName = 'Omikron_New_Lead_Display_Fields';
    private static final String ADDRESS_VALIDATION_WF_NAME = 'RBWF_PostalCorrection';
    private static final String DUPLICATE_CHECK_ACCOUNT_WF_NAME = 'RBWF_DQ_Search_Account';
    private static final String DUPLICATE_CHECK_CONTACT_WF_NAME = 'RBWF_DQ_Search_Contact';
    private static final String DUPLICATE_CHECK_LEAD_WF_NAME = 'RBWF_DQ_Search_Lead';
    private static final String[] ADDRESS_FIELDS_VALIDATION_WF = new String[]{'FirstName', 'LastName', 'PostalCode','Street','CountryCode','City', 'State', 'DunsNumber'};

    public static LeadService instance {
        get {
            if (instance == null) {
                instance = new LeadService();
            }
            return instance;
        }
    }

    private LeadService() {}

    public Boolean isOmikronFieldChanged(SObject oldObj, SObject newObj) {
        Lead oldLead = convertToConcreteType(oldObj);
        Lead newLead = convertToConcreteType(newObj);

        System.assert(newLead != null, 'Argument cannot be null.');

        if (oldLead == null) {
            return true;
        }

        for (String dqsLeadFieldName : DQSParams.instance.dqsLeadFields) {
            if (oldLead.get(dqsLeadFieldName) != newLead.get(dqsLeadFieldName)) {
                return true;
            }
        }
        
        return false;
    }

    public DQSWebServiceWrapper.Address createAddressObject(SObject obj) {
        Lead lead = convertToConcreteType(obj);
        return new DQSWebServiceWrapper.Address(lead);
    }

    public void updateAddressWithDQSData(SObject obj, DQSWebServiceWrapper.AddressCheckResponseData dqsRecord) {
        Lead lead = convertToConcreteType(obj);

        lead.put(DQSParams.instance.dqsLeadStreet, dqsRecord.street);
        lead.put(DQSParams.instance.dqsLeadZipCode, dqsRecord.postCode);
        lead.put(DQSParams.instance.dqsLeadCity, dqsRecord.city);
        lead.put(DQSParams.instance.dqsLeadCountry, dqsRecord.country); 
        lead.put(DQSParams.instance.dqsLeadLatitude, dqsRecord.latitude);     
        lead.put(DQSParams.instance.dqsLeadLongitude, dqsRecord.longitude);   
    }

    public DQSWebServiceWrapper.AddressCheckResponse validateAddress(SObject obj) {
        DQSWebServiceWrapper.Address addressObject = createAddressObject(obj);
        String[] values = new String[] {
            addressObject.firstName,
            addressObject.lastName,
            addressObject.billingPostalCode,
            addressObject.billingStreet,
            addressObject.billingCountry,
            addressObject.billingCity,
            addressObject.billingState,
            addressObject.dunsNumber};

        DQSWebServiceWrapper ws = new DQSWebServiceWrapper(addressObject);
        return ws.validateAddress(ADDRESS_VALIDATION_WF_NAME, ADDRESS_FIELDS_VALIDATION_WF, values);        
    }

    public DQSWebServiceWrapper.DuplicateCheckResponse checkDuplicateInAccounts(SObject obj) {
        DQSWebServiceWrapper.Address addressObject = createAddressObject(obj);
        String[] values = new String[] {
            addressObject.name,
            addressObject.billingPostalCode,
            addressObject.billingStreet,
            addressObject.billingCountry,
            addressObject.billingCity,
            addressObject.billingState,
            addressObject.dunsNumber};

        DQSWebServiceWrapper ws = new DQSWebServiceWrapper(addressObject);
        return ws.checkDuplicate(DUPLICATE_CHECK_ACCOUNT_WF_NAME);      
    }

    public DQSWebServiceWrapper.DuplicateCheckResponse checkDuplicateInContacts(SObject obj) {
        DQSWebServiceWrapper.Address addressObject = createAddressObject(obj);
        String[] values = new String[] {
            addressObject.name,
            addressObject.billingPostalCode,
            addressObject.billingStreet,
            addressObject.billingCountry,
            addressObject.billingCity,
            addressObject.billingState,
            addressObject.dunsNumber};

        DQSWebServiceWrapper ws = new DQSWebServiceWrapper(addressObject);
        return ws.checkDuplicate(DUPLICATE_CHECK_CONTACT_WF_NAME);      
    }   

    public DQSWebServiceWrapper.DuplicateCheckResponse checkDuplicateInLeads(SObject obj) {
        DQSWebServiceWrapper.Address addressObject = createAddressObject(obj);
        String[] values = new String[] {
            addressObject.name,
            addressObject.billingPostalCode,
            addressObject.billingStreet,
            addressObject.billingCountry,
            addressObject.billingCity,
            addressObject.billingState,
            addressObject.dunsNumber};

        DQSWebServiceWrapper ws = new DQSWebServiceWrapper(addressObject);
        return ws.checkDuplicate(DUPLICATE_CHECK_LEAD_WF_NAME);     
    }   

    public Boolean fieldExists(String fieldName) {
        Set<String> objectFields = Schema.SObjectType.Lead.fields.getMap().keySet();
        return objectFields.contains(fieldName);
    }

    public List<String> getInputFieldNames() {
        return SObjectUtility.getFieldSetFieldNames(inputFieldsFieldsetName, Lead.SObjectType);
    }

    public List<String> getUpsertFieldNames() {
        return new List<String>();
    }

    private Lead convertToConcreteType(SObject obj) {
        return (Lead)obj;
    }

    // CHANGES
    public Boolean searchInAccounts() {
        return DQSParams.instance.leadSearchAccount;
    }

    public Boolean searchInContacts() {
        return DQSParams.instance.leadSearchContact;
    }
    
    public Boolean searchInLeads() {
        return DQSParams.instance.leadSearchLead;
    }

    public Boolean searchOnlyInMyRecords() {
        return DQSParams.instance.leadOnlyMyRecords;
    }

    public Boolean validateAddressNew() {
        return DQSParams.instance.avc_Lead_New;
    }

    public Boolean validateAddressEdit() {
        return DQSParams.instance.avc_Lead_Edit;
    }
}