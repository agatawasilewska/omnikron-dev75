global class BatchSyncRecords2 implements Database.Batchable<sObject>, Database.AllowsCallouts {

    public List<Id> accountUpserts;
    public List<Id> accountDeletes;

    global BatchSyncRecords2() {
        accountUpserts = new List<id>();
        accountDeletes = new List<id>();
    }

    private void cleanUp(List<DQSWebServiceWrapper.ModifyRecordResponse> respList, List<Id> idList, String operation) {
        if (respList.size() > 0) {
            String op;
            if (operation.equalsIgnoreCase(DQSDataSyncQueueService.OPERATION_UPSERT)) {
                op = DQSDataSyncLogService.UPSERT_OPERATION;
            } else {
                op = DQSDataSyncLogService.DELETE_OPERATION;
            }
            Integer counter = 0;
            for (DQSWebServiceWrapper.ModifyRecordResponse resp : respList) {
                DQSDataSyncLogService.instance.logOperation(idList[counter], op, resp.responseData.resultDQ, resp.responseData.resultFF);
                counter++;
            }
            DQSDataSyncQueueService.cleanObjects(idList, DQSDataSyncQueueService.OBJ_TYPE_ACCOUNT, operation);
        } 
    }

    global Iterable<sObject> start(Database.BatchableContext BC){
        return DQSDataSyncQueueService.getProcessingRecords();
   }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
        // Sort scope objects per Type and Operation
        for(sObject obj : scope) {
            String objectType = (String)obj.get(DQSDataSyncQueueService.OBJ_TYPE_FIELD);
            if (objectType.equals(DQSDataSyncQueueService.OBJ_TYPE_ACCOUNT)) {
                DQSDataSyncQueueService.sortPerOperation(obj, accountUpserts, accountDeletes);
            }
        }

        // Execute callouts if needed
        List<DQSWebServiceWrapper.ModifyRecordResponse> accUpResps = new List<DQSWebServiceWrapper.ModifyRecordResponse>();
        List<DQSWebServiceWrapper.ModifyRecordResponse> accDelResps = new List<DQSWebServiceWrapper.ModifyRecordResponse>();

        if (accountUpserts.size() > 0) {
            accUpResps = AccountService.instance.upsertRecords(accountUpserts);
        }
        if (accountDeletes.size() > 0) {
            accDelResps = AccountService.instance.deleteRecords(accountDeletes);
        }

        cleanUp(accUpResps, accountUpserts, DQSDataSyncQueueService.OPERATION_UPSERT);
        cleanUp(accDelResps, accountDeletes, DQSDataSyncQueueService.OPERATION_DELETE);
    }

   global void finish(Database.BatchableContext BC) {
        if ((DQSDataSyncQueueService.getQueuedRecordsCount() > 0) && !BatchSyncRecords.isRunning()) {
            Database.executeBatch(new BatchSyncRecords(), DQSParams.instance.batchSize);
        }
   }

}