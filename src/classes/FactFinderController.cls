public with sharing class FactFinderController {
    
    public String factFinderProxyUriWithParameters {get; private set;}
    public String factFinderUriWithParameters {get; private set;}
    public String sessionId {get; private set;}
    
    public FactFinderController() {
        
        this.factFinderProxyUriWithParameters = FactFinderParams.FACT_FINDER_PROXY_URI;
        this.sessionId = UserInfo.getSessionID();
        this.factFinderProxyUriWithParameters += this.sessionId;
        // call PROXY and get FF ssid
        
        Http h = new Http();
        HttpRequest request = new HttpRequest(); 
        HttpResponse response;
        //request.setHeader('Authorization', 'Bearer ' + sessionId);
        request.setMethod('GET');
        request.setEndpoint(this.factFinderProxyUriWithParameters);
        system.debug('### request: ' + request);
        
        response = CalloutsService.sendCallout(h, request);
        //HttpResponse response = CalloutsService.makeCallGET(this.factFinderProxyUriWithParameters, configurationName, null, null);
        
        system.debug('### response: ' + response);
        
        //this.factFinderUriWithParameters = FactFinderParams.FACT_FINDER_URI;
        
        this.factFinderUriWithParameters = FactFinderParams.FACT_FINDER_URI_WITH_SSID;
        this.factFinderUriWithParameters += response.getBody();
        
    }
    
}