public with sharing class DatabaseSharingUtilities {

    public static Set<Id> getAccounts() {
        return new Map<Id, Account>([SELECT Id FROM Account]).keySet();
    }
    
    public static Set<Id> getLeads() {
        return new Map<Id, Lead>([SELECT Id FROM Lead]).keySet();
    }
    
    public static Set<Id> getContacts() {
        return new Map<Id, Contact>([SELECT Id FROM Contact]).keySet();
    }

}