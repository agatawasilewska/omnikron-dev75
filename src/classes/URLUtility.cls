/**
 @file      URLUtility
 @author    Piotr Bardziński
 @version 1.0
 @date 2016-02-10
 @brief     Helper class for constructing URLs that allow redirect pages.
 @details
 @section   DESCRIPTION
 Omikron
*/

public class URLUtility {
    
    /* Exception */
    public class URLUtilityException extends Exception {}

    private static final String EDIT_ACTION = 'e';
    private static final String URL_SEPARATOR = '/';
    private static final String URL_RECORD_TYPE_SELECT = '/setup/ui/recordtypeselect.jsp';
    private static final String PARAM_NOOVERRIDE = 'nooverride';
    public static final String PARAM_ENTITY_ID = 'ent';
    public static final String PARAM_RET_URL = 'retURL';
    public static final String PARAM_RECORD_TYPE = 'RecordType';
    private static final String PARAM_SAVE_NEW = 'save_new';
    private static final String PARAM_SAVE_NEW_URL = 'save_new_url';
    private static final Set<String> PARAMS_2_COPY = new Set<String> {PARAM_RET_URL};
    private static final Set<String> PARAMS_2_REMOVE = new Set<String> {PARAM_SAVE_NEW};
    private static final Integer ENT_ID_LENGTH = 15;

    /**
     * Builds URL for 'New' action of specific object.
     * URL has prepopulation params set (only for lookups).
     * @param  type          Type of object that is to be 
     * @param  fieldId2Value Field Id that will be prepopulated with value (map Id->Value).
     * @param  currentParams Current Page params - will be copied to newly created URL.
     * @param  noOverride    If original version of page is to be called. If true, nooverride parameter will be added to URL.
     * @return               Constructed PageReference.
     */
    public static PageReference constructNewActionURL(SObjectType type, Map<String, String> fieldId2Value, Map<String, String> currentParams, boolean noOverride) {
        Schema.DescribeSObjectResult dso = type.getDescribe();
        PageReference res = new PageReference(URL_SEPARATOR + dso.getKeyPrefix() + URL_SEPARATOR + EDIT_ACTION); // for example: /a2I/e
        res.setRedirect(false);
        
        Map<String, String> newParams = res.getParameters();

        if (currentParams != null) {
            for (String currentParam : currentParams.keySet()) {
                if (!PARAMS_2_REMOVE.contains(currentParam)) {
                    newParams.put(currentParam, currentParams.get(currentParam));
                }
            }
        }

        newParams.put(PARAM_NOOVERRIDE, noOverride ? '1' : '0');

        System.debug('URLUtility.constructNewActionURL fieldId2Value=' + fieldId2Value);
        
        if (fieldId2Value != null) {
            newParams.putAll(fieldId2Value);
        }

        return res;     
    }

    /**
     * Builds URL for 'Edit' action of specific object.
     * URL has prepopulation params set (only for lookups).
     * @param  objectId      Id of object that is to be updated 
     * @param  fieldId2Value Field Id that will be prepopulated with value (map Id->Value).
     * @param  currentParams Current Page params - will be copied to newly created URL.
     * @param  noOverride    If original version of page is to be called. If true, nooverride parameter will be added to URL.
     * @return               Constructed PageReference.
     */
    public static PageReference constructEditActionURL(String objectId, Map<String, String> fieldId2Value, Map<String, String> currentParams, boolean noOverride) {
        PageReference res = new PageReference(URL_SEPARATOR + objectId + URL_SEPARATOR + EDIT_ACTION); // for example: /a2I/e
        res.setRedirect(false);
        
        Map<String, String> newParams = res.getParameters();

        if (currentParams != null) {
            for (String currentParam : currentParams.keySet()) {
                if (!PARAMS_2_REMOVE.contains(currentParam)) {
                    newParams.put(currentParam, currentParams.get(currentParam));
                }
            }
        }

        newParams.put(PARAM_NOOVERRIDE, noOverride ? '1' : '0');

        System.debug('URLUtility.constructNewActionURL fieldId2Value=' + fieldId2Value);
        
        if (fieldId2Value != null) {
            newParams.putAll(fieldId2Value);
        }

        return res;     
    }

    /**
     * Builds URL for SObject action (standard list view)
     */
    public static PageReference constructSObjectURL(SObjectType type) {
        Schema.DescribeSObjectResult dso = type.getDescribe();
        
        PageReference res = new PageReference(URL_SEPARATOR + dso.getKeyPrefix()); // for example: /a2I
        res.setRedirect(false);

        return res;
    }

    public static PageReference constructIdURL(Id objectId) {
        PageReference res = new PageReference(URL_SEPARATOR + objectId);
        res.setRedirect(false);

        return res;
    }

    public static PageReference constructRecordTypeSelectURL(Id entityId, Map<String, String> currentParams, PageReference newActionURL, boolean noOverride) {
        //full id does not work, we have to get only 15 signs
        String shortId = String.valueOf(entityId);
        if (String.isBlank(shortId) || shortId.length() < ENT_ID_LENGTH) {
            throw new URLUtilityException('Wrong configuration data. Unproper Entity Id.');
        }
        shortId = shortId.substring(0, ENT_ID_LENGTH);
        PageReference res = new PageReference(URL_RECORD_TYPE_SELECT);
        res.getParameters().put(PARAM_ENTITY_ID, shortId);
        String retURL = currentParams.get(PARAM_RET_URL);
        if (String.isNotBlank(retURL)) {
            res.getParameters().put(PARAM_RET_URL, retURL);
        }
        res.getParameters().put(PARAM_NOOVERRIDE, noOverride == true ? '1' : '0');
        res.getParameters().put(PARAM_SAVE_NEW_URL, newActionURL.getURL());

        System.debug(logginglevel.ERROR, '### URLUtility.constructRecordTypeSelectURL res=' + res);
        return res;
    }

    /**
    @brief  Method to get Salesforce URL.
    @return URL of Salesforce instance.
    @author Piotr Bardziński
    */
    public static String getHttpsSalesforceBaseURL() {
        
        String baseURL = URL.getSalesforceBaseUrl().toExternalForm();
        
        if (baseURL.startsWith('http:')){
            baseURL = baseURL.replaceFirst('http:', 'https:');
        }
        
        return baseURL;
    }   

}