public class FactFinderProxy {
    public String getSessionId() {
        String responseFromNet;
        
        HttpRequest reqData = new HttpRequest();
        Http http = new Http();
            
        reqData.setHeader('Content-Type','application/json');
        reqData.setHeader('Connection','keep-alive');
        reqData.setHeader('Content-Length','0');
        reqData.setTimeout(20000); 
        reqData.setEndpoint('http://api.geonames.org/postalCodeLookupJSON?postalcode=6600&country=AT&username=demo');
        reqData.setMethod('GET');
        
        try
        {    
             HTTPResponse res = http.send(reqData);
             responseFromNet = res.getBody();
             
             //JSONObject jSon=new JSONObject('{"aa":'+responseFromNet+' }'); 
             system.debug(responseFromNet);
             return responseFromNet;
         }
         catch(Exception exp)
         {
              System.debug('exception ' + exp);
              return 'exception ' + exp;
         }
    } 
}