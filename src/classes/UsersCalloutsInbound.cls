/**
 @file      UsersCalloutsInbound
 @author    Piotr Bardziński
 @version   1.0
 @date      2016-02-16
 @brief     APEX class to implement REST API based Web-services related to Users.
 @details
 @section   DESCRIPTION
 Omikron
*/
@RestResource(urlMapping='/GetUserIdOld/*')
global with sharing class UsersCalloutsInbound {

    /*  Method implements REST API Web-service to get User name for the current logged-in User (based on the authorization sessions Id).
    *   @date   2016-02-16
    */
    @HttpGet
    global static ID GetUserIdOld() {
        
        /*RestRequest req = RestContext.request;
        System.debug('### req: ' + req);
        RestResponse res = RestContext.response;
        System.debug('### res: ' + res);    
        map<String, String> mapParams = RestContext.request.params;
        System.debug('### mapParams: ' + mapParams);

        ID userId = UserInfo.getUserId();
        system.debug('### userId: ' + userId);
        //String SessionId = RestContext.request.params.get('SessionId');
        //System.debug('### SessionId in: '+SessionId);
        String SessionId = UserInfo.getSessionId();
        System.debug('### SessionId: ' + SessionId);
        */
        ID userId;// = UsersCalloutsOutbound.getUserIdForSessionIdByWebservice('00D24000000HYiF!AQ4AQDFkUEzY3PghGDsnXDo1YxQ637cN2ivDKMrUepEZX.dNX7qM6pA_7WMwh_jVwAxkZ08_42aMqk.QdVw3yNePB2x66kb7');
        system.debug('### userId: ' + userId);

        return userId;
    }
}