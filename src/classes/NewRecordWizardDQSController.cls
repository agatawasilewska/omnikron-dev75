public virtual class NewRecordWizardDQSController {
    private final static List<String> DEFAULT_HEADERS_ADDRESS_CHECK = new List<String>{ 'Street', 'PostalCode', 'City', 'Country', 'Latitude', 'Longitude', 'Out_PC_Status', 'Out_PC_Status_Long'};
    private final static List<String> DEFAULT_HEADERS_DUPLICATES = new List<String>{ 'HitColumn', 'AvgSimi', 'Name', 'Street', 'PostCode', 'City', 'State'};
    private final static Set<String> ADDITIONAL_HEADERS_DUPLICATES = new Set<String>{'HitColumn','AvgSimi'};
    private final static String RECORD_TYPE_ID_FIELD_NAME = 'RecordTypeId';

    public SObject currentObject { get; set; }
    public VisibilityControl elementsVisibility { get; protected set; }
    public List<String> inputFields { get; protected set; }
    public HighlightColors highlights { get; protected set; }

    public List<String> addressCheckHeaders { get; protected set; }
    public List<DQSWebServiceWrapper.AddressCheckResponseData> addressResult { get; protected set; }

    public List<String> accDupHeaders { get; protected set; }
    public List<String> conDupHeaders { get; protected set; }
    public List<String> leadDupHeaders { get; protected set; }
    public List<DuplicateCheckResponseTranslated> accDupResult { get; protected set; }
    public List<DuplicateCheckResponseTranslated> conDupResult { get; protected set; }
    public List<DuplicateCheckResponseTranslated> leadDupResult { get; protected set; }

    public Boolean disableHitColumnTranslations { get; protected set; }
    public Integer chosenAddressId { get; set; }

    private Boolean addressValidationDone;
    private Boolean deDuplicationDone;

    public IRecordManager recordManager {
        get;
        set {
                recordManager = value;
                inputFields = recordManager.getInputFieldNames();
                updateVisibility();
        }
    }

    public NewRecordWizardDQSController() {
        highlights = new HighlightColors();
        
        elementsVisibility = new VisibilityControl();
        elementsVisibility.addressSectionVisible = false;
        elementsVisibility.addressDataTableVisible = false;
        elementsVisibility.duplicatesSectionVisible = false;
        elementsVisibility.searchForRecordButtonDisabled = true;
        elementsVisibility.createAccountButtonDisabled = true;
        elementsVisibility.createRecordButtonDisabled = true;
        
        disableHitColumnTranslations =  DQSParams.instance.disableHitColumnTranslations;   

        elementsVisibility.accDuplicatesDataTableVisible = false;
        elementsVisibility.conDuplicatesDataTableVisible = false;
        elementsVisibility.leadDuplicatesDataTableVisible = false;
        elementsVisibility.searchForRecordButtonVisible = false;
        elementsVisibility.validateAddressButtonVisible = false;
        accDupResult = new List<DuplicateCheckResponseTranslated>();
        conDupResult = new List<DuplicateCheckResponseTranslated>();
        leadDupResult = new List<DuplicateCheckResponseTranslated>();
        accDupHeaders = new List<String>();
        conDupHeaders = new List<String>();
        leadDupHeaders = new List<String>();

        addressValidationDone = false;
        deDuplicationDone = false;
    }

    private Boolean parseDuplicateResponse(DQSWebServiceWrapper.DuplicateCheckResponse resp, Set<String> displayFields, List<String> duplicateHeaderList, List<DuplicateCheckResponseTranslated> duplicateResultList) {
        duplicateHeaderList.clear();
        if (!resp.resultFlag || resp.headerFields == null || resp.headerFields.isEmpty()) {
            duplicateHeaderList.addAll(DEFAULT_HEADERS_DUPLICATES);
        }
        if (resp.resultFlag) {
            for (String hf : resp.headerFields) {
                if (displayFields.contains(hf)) {
                    duplicateHeaderList.add(hf);
                }
            }

            Set<String> uniqueHitColumnValues = resp.getHitColumnValues();
            Map<String, Account> translationObjects = StringUtility.translateHitColumnValue(uniqueHitColumnValues);
            List<DQSWebServiceWrapper.DuplicateCheckResponseData> duplicateRawResult = resp.responseData;
            duplicateResultList.clear();
            for (DQSWebServiceWrapper.DuplicateCheckResponseData responseDataRow : duplicateRawResult) {
                duplicateResultList.add(new DuplicateCheckResponseTranslated(responseDataRow, translationObjects.get(responseDataRow.hitColumn)));
            }
        } else {
            //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, wsResult.resultFlagDescription)); //was: 'Fatal Error: CalloutException occured'
        }

        return resp.resultFlag && !resp.headerFields.isEmpty();
    }

    public PageControllerUtility.ColumnDefinition[] getAccDuplicatesColumns() {
        return getDuplicatesColumns(accDupHeaders);
    }

    public PageControllerUtility.ColumnDefinition[] getConDuplicatesColumns() {
        return getDuplicatesColumns(conDupHeaders);
    }

    public PageControllerUtility.ColumnDefinition[] getLeadDuplicatesColumns() {
        return getDuplicatesColumns(leadDupHeaders);
    }   

    public PageControllerUtility.ColumnDefinition[] getDuplicatesColumns(List<String> duplicateHeaders) {
        if (duplicateHeaders == null || duplicateHeaders.size() < 7) {
            return new PageControllerUtility.ColumnDefinition[]{};
        }

        return new PageControllerUtility.ColumnDefinition[] {
            new PageControllerUtility.ColumnDefinition('name', 'id', duplicateHeaders[2]),
            new PageControllerUtility.ColumnDefinition('street', duplicateHeaders[3]),
            new PageControllerUtility.ColumnDefinition('postCode', duplicateHeaders[6]),
            new PageControllerUtility.ColumnDefinition('city', duplicateHeaders[4]),
            new PageControllerUtility.ColumnDefinition('country', duplicateHeaders[5])
        };
    }

    private void updateVisibility() {
        if (!deDuplicationDone) {
            if (!addressValidationDone) { // START STATE
                elementsVisibility.validateAddressButtonVisible = recordManager.validateAddressNew();
                elementsVisibility.searchForRecordButtonVisible = recordManager.searchInAccounts() || recordManager.searchInContacts() || recordManager.searchInLeads();
                elementsVisibility.createRecordButtonDisabled = elementsVisibility.validateAddressButtonVisible || elementsVisibility.searchForRecordButtonVisible;
                elementsVisibility.searchForRecordButtonDisabled = !(!elementsVisibility.validateAddressButtonVisible && elementsVisibility.searchForRecordButtonVisible);
            } 
            else { // AFTER ADDRESS VALIDATION, when av is enabled
                if (elementsVisibility.searchForRecordButtonVisible) {
                    elementsVisibility.searchForRecordButtonDisabled = false;
                } else {
                    elementsVisibility.createRecordButtonDisabled = false;
                }
            }
        }
        else { // AFTER DEDUPLICATION
            elementsVisibility.createRecordButtonDisabled = false;
        }

    }

    public void checkAddress() {
        try {
            addressValidationDone = true;
            updateVisibility();
            addressCheckHeaders = DEFAULT_HEADERS_ADDRESS_CHECK;
            elementsVisibility.addressSectionVisible = true;
            
            DQSWebServiceWrapper.AddressCheckResponse wsResult = recordManager.validateAddress(currentObject);  
            
            elementsVisibility.addressDataTableVisible = wsResult.resultFlag;
            if (wsResult.resultFlag) {
                if (!wsResult.headerFields.isEmpty()) {
                    addressCheckHeaders.addAll(wsResult.headerFields);
                }
                addressResult = wsResult.responseData;
            } else {
                //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, wsResult.resultFlagDescription)); //was: 'Fatal Error: CalloutException occured'
            }
        } catch(CalloutException e) {
            System.debug('NewRecordWizardDQSController.checkAddress e.getMessage()=' + e.getMessage());
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Callout Exception Error: ' + e.getMessage()));
        } catch(Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
        }
    }

    public void checkDuplicate() {
        try {
            deDuplicationDone = true;
            updateVisibility();

            Set<String> displayFieldsFilter = ADDITIONAL_HEADERS_DUPLICATES;
            displayFieldsFilter.addAll(inputFields);
            
            elementsVisibility.addressSectionVisible = false;
            elementsVisibility.addressDataTableVisible = false;
            elementsVisibility.duplicatesSectionVisible = true;
            elementsVisibility.accDuplicatesDataTableVisible = false;
            elementsVisibility.conDuplicatesDataTableVisible = false;
            elementsVisibility.leadDuplicatesDataTableVisible = false;
            
            if (recordManager.searchInAccounts()) {
                DQSWebServiceWrapper.DuplicateCheckResponse accResp= recordManager.checkDuplicateInAccounts(currentObject);
                elementsVisibility.accDuplicatesDataTableVisible = parseDuplicateResponse(accResp, displayFieldsFilter, accDupHeaders, accDupResult);

                if (recordManager.searchOnlyInMyRecords()) {
                    system.debug('AccountDups before filtering: ' + accDupResult);
                    Set<Id> sharedAccounts = DatabaseSharingUtilities.getAccounts();
                    List<DuplicateCheckResponseTranslated> filteredDupResult = new List<DuplicateCheckResponseTranslated>();
                    for (DuplicateCheckResponseTranslated dupRestData : accDupResult) {
                        if (sharedAccounts.contains(dupRestData.id)) {
                            filteredDupResult.add(dupRestData);
                        }
                    }
                    accDupResult = filteredDupResult;
                    system.debug('AccountDups after filtering: ' + accDupResult);
                }
            }

            if (recordManager.searchInContacts()) {
                DQSWebServiceWrapper.DuplicateCheckResponse conResponse = recordManager.checkDuplicateInContacts(currentObject);
                elementsVisibility.conDuplicatesDataTableVisible = parseDuplicateResponse(conResponse, displayFieldsFilter, conDupHeaders, conDupResult);

                if (recordManager.searchOnlyInMyRecords()) {
                    system.debug('conDupResult before filtering: ' + conDupResult);
                    Set<Id> sharedContacts = DatabaseSharingUtilities.getContacts();
                    List<DuplicateCheckResponseTranslated> filteredDupResult = new List<DuplicateCheckResponseTranslated>();
                    for (DuplicateCheckResponseTranslated dupRestData : conDupResult) {
                        if (sharedContacts.contains(dupRestData.id)) {
                            filteredDupResult.add(dupRestData);
                        }
                    }
                    conDupResult = filteredDupResult;
                    system.debug('conDupResult after filtering: ' + conDupResult);
                }   
            }

            if (recordManager.searchInLeads()) {
                DQSWebServiceWrapper.DuplicateCheckResponse leadResponse = recordManager.checkDuplicateInLeads(currentObject);
                elementsVisibility.leadDuplicatesDataTableVisible = parseDuplicateResponse(leadResponse, displayFieldsFilter, leadDupHeaders, leadDupResult);

                if (recordManager.searchOnlyInMyRecords()) {
                    system.debug('leadDupResult before filtering: ' + leadDupResult);
                    Set<Id> sharedLeads = DatabaseSharingUtilities.getLeads();
                    List<DuplicateCheckResponseTranslated> filteredDupResult = new List<DuplicateCheckResponseTranslated>();
                    for (DuplicateCheckResponseTranslated dupRestData : leadDupResult) {
                        if (sharedLeads.contains(dupRestData.id)) {
                            filteredDupResult.add(dupRestData);
                        }
                    }
                    leadDupResult = filteredDupResult;
                    system.debug('leadDupResult after filtering: ' + leadDupResult);
                }   
            }
        } catch(CalloutException e) {
            System.debug('NewRecordWizardDQSController.checkDuplicate e.getMessage()=' + e.getMessage());
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Callout Exception Error: ' + e.getMessage()));
        } catch(Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
        }       
    }

    public PageReference cancel() {
        return URLUtility.constructSObjectURL(currentObject.getSObjectType());
    }

    public PageReference createObject() {
        Map<String, String> urlParams = new Map<String, String>();

        addFieldValuesToURL(urlParams);
        addStandardParamsToURL(urlParams);

        String currObjId = String.valueOf(currentObject.get('Id'));
        if (currObjId != null && String.isNotBlank(currObjId)) {
            return URLUtility.constructEditActionURL(currObjId, urlParams, null, true);
        }

        return URLUtility.constructNewActionURL(currentObject.getSObjectType(), urlParams, null, true);
        //https://omikron-dev-ed.my.salesforce.com/001/e?retURL=%2F001%2Fo&RecordType=012240000002e9S&ent=Account&nooverride=1

        //String prefix = currentObject.getSObjectType().getDescribe().getKeyPrefix();
        //return new PageReference('/'+prefix+'/e?retURL=%2F'+prefix+'%2Fo&RecordType='+currentObject.get('RecordTypeId')+'&ent='+currentObject.getSObjectType().getDescribe().getName()+'&nooverride=1');  
    }

    public void useDQSAddress() {
        if (chosenAddressId != null) {
            for (DQSWebServiceWrapper.AddressCheckResponseData responseRecord : addressResult) {
                if (responseRecord.addressId == chosenAddressId) {
                    recordManager.updateAddressWithDQSData(currentObject, addressResult.get(chosenAddressId));
                    break;
                }
            }
        }
    }

    private void addFieldValuesToURL(Map<String, String> urlParams) {
        for (String fieldName : inputFields) {
            String fieldId = DQSFieldsPopulationParams.getFieldId(currentObject.getSObjectType(), fieldName);
            String fieldValue = (String)currentObject.get(fieldName);
            if (String.isNotBlank(fieldId) && String.isNotBlank(fieldValue)) {
                urlParams.put(fieldId, fieldValue);
            }
        }       
    }

    private void addStandardParamsToURL(Map<String, String> urlParams) {
        if (recordManager.fieldExists(RECORD_TYPE_ID_FIELD_NAME)) {
            urlParams.put(URLUtility.PARAM_RECORD_TYPE, (String)currentObject.get(RECORD_TYPE_ID_FIELD_NAME));
        }
        urlParams.put(URLUtility.PARAM_ENTITY_ID, currentObject.getSObjectType().getDescribe().getName());
        urlParams.put(URLUtility.PARAM_RET_URL, URLUtility.constructSObjectURL(currentObject.getSObjectType()).getURL());       
    }

    public class VisibilityControl {
        public Boolean addressSectionVisible { get; protected set; }
        public Boolean addressDataTableVisible { get; protected set; }
        public Boolean duplicatesSectionVisible { get; protected set; }
        public Boolean duplicatesFound { 
            get {
                return !(accDuplicatesDataTableVisible || conDuplicatesDataTableVisible || leadDuplicatesDataTableVisible);
                }
            protected set;
        }
        public Boolean accDuplicatesDataTableVisible { get; protected set; }
        public Boolean conDuplicatesDataTableVisible { get; protected set; }
        public Boolean leadDuplicatesDataTableVisible { get; protected set; }

        public Boolean createRecordButtonDisabled { get; protected set; }
        public Boolean searchForRecordButtonDisabled { get; protected set; }
        public Boolean createAccountButtonDisabled { get; protected set; }
        
        public Boolean searchForRecordButtonVisible { get; protected set; }
        public Boolean validateAddressButtonVisible { get; protected set; }

        public VisibilityControl() {}
    }

    public class HighlightColors {
        public String highlightPositive  { get; protected set; }
        public String highlightNegative  { get; protected set; }
        public String highlightNeutral  { get; protected set; }

        public HighlightColors() {
            highlightPositive = DQSParams.instance.highlight_Positive;
            highlightNegative = DQSParams.instance.highlight_Negative;
            highlightNeutral = DQSParams.instance.highlight_Neutral;
        }
    }

    public class DuplicateCheckResponseTranslated extends DQSWebServiceWrapper.DoWorkflowResponseData {
        public Account hitColumnTranslator { get; protected set; }

        public DuplicateCheckResponseTranslated(DQSWebServiceWrapper.DoWorkflowResponseData wsResponseData, Account translator) {
            super(wsResponseData);
            hitColumnTranslator = translator;
        }
    }
}