public class DQSTranslations {

    public static String ACCOUNT_OBJECT_TYPENAME = 'Account';

    public static String CONTACT_OBJECT_TYPENAME = 'Contact';

    public static String LEAD_OBJECT_TYPENAME = 'Lead';

    public static String TRANSLATION_PICKLIST_NAME = '';

    public static List<List<String>> getHitColumnTranslationsForObject(List<List<String>> responseResults, String objectType){
        Map<String, String> translationsHCMap = new Map<String, String>();
        List<Schema.PicklistEntry> picklistVal = Schema.SObjectType.Account.fields.HitColumnTranslations__c.getPicklistValues();
        if(objectType == ACCOUNT_OBJECT_TYPENAME) picklistVal = Schema.SObjectType.Account.fields.HitColumnTranslations__c.getPicklistValues();
        if(objectType == CONTACT_OBJECT_TYPENAME) picklistVal = Schema.SObjectType.Contact.fields.HitColumnTranslations__c.getPicklistValues();
        if(objectType == LEAD_OBJECT_TYPENAME) picklistVal = Schema.SObjectType.Lead.fields.HitColumnTranslations__c.getPicklistValues();
        for(Schema.PicklistEntry spe : picklistVal){
            translationsHCMap.put(spe.getValue(), spe.getLabel());
        }
        for(List<String> lst : responseResults){
            lst[0] = translationsHCMap.get(lst[0]);
        }
        return responseResults;
    }
}