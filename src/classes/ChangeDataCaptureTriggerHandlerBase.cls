public abstract class ChangeDataCaptureTriggerHandlerBase extends TriggerHandlerBase {
    List<SObject> newObjList;
    Map<Id, SObject> oldObjs;

    protected IRecordManager recordManager;

    public ChangeDataCaptureTriggerHandlerBase(Map<Id, SObject> oldObjs, List<SObject> newObjList, IRecordManager recordManager) {
        this.oldObjs = oldObjs;
        this.newObjList = newObjList;
        this.recordManager = recordManager;
    }

    protected virtual void putUpsertIntoDb(String objectType) {
        List<Id> idList = new List<Id>();
        for (sObject obj : newObjList) {
            if (isOmikronNotificationNeeded(obj)) {
                idList.add(obj.Id);
            }
        }
        DQSDataSyncQueueService.queueObjects(idList, objectType, DQSDataSyncQueueService.OPERATION_UPSERT);
    }

    protected virtual void putDeleteIntoDb(String objectType) {
        DQSDataSyncQueueService.queueObjects(new List<Id>(oldObjs.keySet()), objectType, DQSDataSyncQueueService.OPERATION_DELETE);
    }

    protected virtual Boolean isOmikronNotificationNeeded(SObject newObj) {
        return (
            isInsert 
            || (
                isUpdate 
                && recordManager.isOmikronFieldChanged(getOldObject(newObj), newObj)
            )
        );
    }
}