public class ConversionService {
    public static String convertToString(Boolean value) {
        try {
            return String.valueOf(value);
        } catch (Exception ex) {
            //function is wrapped to consume exception, ivalid input from User
        }

        return null;
    }

    public static Boolean convertToBool(String value) {
        try {
            return Boolean.valueOf(value);
        } catch (Exception ex) {
            //function is wrapped to consume exception, ivalid input from User
        }

        return null;
    }
}