/**
 @file      REST_GetUserId
 @author    Piotr Bardziński
 @version   1.0
 @date      2016-02-16
 @brief     APEX class to implement REST API based Web-services related to Users.
 @details
 @section   DESCRIPTION
 Omikron
*/

@RestResource(urlMapping='/GetUserId/*')
global with sharing class REST_GetUserId {

    /*  Method implements REST API Web-service to get User name for the current logged-in User (based on the authorization sessions Id).
    *   @date   2016-02-16
    */
    @HttpGet
    global static ID GetUserId() {
        
        RestRequest req = RestContext.request;
        System.debug('### req: ' + req);
        RestResponse res = RestContext.response;
        System.debug('### res: ' + res);    
        map<String, String> mapParams = RestContext.request.params;
        System.debug('### mapParams: ' + mapParams);

        ID userId = UserInfo.getUserId();
        system.debug('### userId: ' + userId);
        //String SessionId = RestContext.request.params.get('SessionId');
        //System.debug('### SessionId in: '+SessionId);
        String SessionId = UserInfo.getSessionId();
        System.debug('### SessionId: ' + SessionId);
        return userId;
    }
}