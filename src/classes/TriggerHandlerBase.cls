/**
 *  Base class for trigger handler.
 */
public abstract class TriggerHandlerBase {

    protected boolean isInsert {
        get {
            return Trigger.isInsert;
        }
    }

    protected boolean isUpdate {
        get {
            return Trigger.isUpdate;
        }
    }

    public void handleTrigger() {
        
        
        // Call the correct handle method
        if (trigger.isBefore && trigger.isInsert) {
            handleBeforeInsert();
        }
        if (trigger.isAfter && trigger.isInsert) {
            handleAfterInsert();
        }
        if (trigger.isBefore && trigger.isUpdate) {
            handleBeforeUpdate();
        }
        if (trigger.isAfter && trigger.isUpdate) {
            handleAfterUpdate();
        }
        if (trigger.isBefore && trigger.isDelete) {
            handleBeforeDelete();
        }
        if (trigger.isAfter && trigger.isDelete) {
            handleAfterDelete();
        }
        if (trigger.isBefore && trigger.isUndelete) {
            handleBeforeUndelete();
        }
    }
    
    protected virtual void handleBeforeInsert() {
    }
    protected virtual void handleAfterInsert() {
    }
    protected virtual void handleBeforeUpdate() {
    }
    protected virtual void handleAfterUpdate() {
    }
    protected virtual void handleBeforeDelete() {
    }
    protected virtual void handleAfterDelete() {
    }
    protected virtual void handleBeforeUndelete() {
    }

    protected virtual SObject getOldObject(SObject newObject) {
        System.assert(newObject != null, 'Argument cannot be null.');
        return (Trigger.oldMap != null ? Trigger.oldMap.get(newObject.Id) : null);        
    }    
}