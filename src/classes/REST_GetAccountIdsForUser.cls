@RestResource(urlMapping='/GetAccountIdsForUser/*')

global with sharing class REST_GetAccountIdsForUser {

@HttpGet
  global static String GetAccountIdsForUser() {
    
    //ID UserId = RestContext.request.params.get('UserId');
    string stringAccountIds = RestContext.request.params.get('AccountIds');
    system.debug('### stringAccountIds: ' + stringAccountIds);
    
    set<String> sAccounIdsIn = (set<String>)JSON.deserialize(stringAccountIds, Set<String>.class);
    system.debug('### sAccounIdsIn: ' + sAccounIdsIn);
    
    map<ID, Account> mapAccounts = new map<ID, Account>([   SELECT  ID, Name, Type, BillingState
                                                            FROM    Account
                                                            WHERE   ID in :sAccounIdsIn]);
    system.debug('### mapAccounts: ' + mapAccounts);
    
    set<ID> sAccountIdsOut = mapAccounts.keySet();
    system.debug('### sAccountIdsOut: ' + sAccountIdsOut);
    
    String stringAccountIdsOut = JSON.serialize(sAccountIdsOut);
    system.debug('### stringAccountIdsOut: ' + stringAccountIdsOut);

    /*list<ID> lAccountIdsOut = new list<ID>(sAccountIdsOut);
    stringAccountIdsOut = String.join(lAccountIdsOut, ',');
    system.debug('### stringAccountIdsOut: ' + stringAccountIdsOut);
    stringAccountIdsOut = '["0012400000Vy6AdAAJ","0012400000W13Y6AAJ"]';
    system.debug('### stringAccountIdsOut: ' + stringAccountIdsOut);*/
    return stringAccountIdsOut;
  }
}