public class ContactService implements IRecordManager {
    private final String inputFieldsFieldsetName = 'Omikron_New_Contact_Display_Fields';
    private static final String ADDRESS_VALIDATION_WF_NAME = 'RBWF_PostalCorrection';
    private static final String DUPLICATE_CHECK_ACCOUNT_WF_NAME = 'RBWF_DQ_Search_Account';
    private static final String DUPLICATE_CHECK_CONTACT_WF_NAME = 'RBWF_DQ_Search_Contact';
    private static final String DUPLICATE_CHECK_LEAD_WF_NAME = 'RBWF_DQ_Search_Lead';
    private static final String[] ADDRESS_FIELDS_VALIDATION_WF = new String[]{'FirstName', 'LastName', 'PostalCode','Street','CountryCode','City', 'State', 'DunsNumber'};

    public static ContactService instance {
        get {
            if (instance == null) {
                instance = new ContactService();
            }
            return instance;
        }
    }

    private ContactService() {}

    public Boolean isOmikronFieldChanged(SObject oldObj, SObject newObj) {
        Contact oldContact = convertToConcreteType(oldObj);
        Contact newContact = convertToConcreteType(newObj);

        System.assert(newContact != null, 'Argument cannot be null.');

        if (oldContact == null) {
            return true;
        }

        for (String dqsContactFieldName : DQSParams.instance.dqsContactFields) {
            if (oldContact.get(dqsContactFieldName) != newContact.get(dqsContactFieldName)) {
                return true;
            }
        }
        
        return false;
    }

    public DQSWebServiceWrapper.Address createAddressObject(SObject obj) {
        Contact contact = convertToConcreteType(obj);
        return new DQSWebServiceWrapper.Address(contact);
    }

    public void updateAddressWithDQSData(SObject obj, DQSWebServiceWrapper.AddressCheckResponseData dqsRecord) {
        Contact contact = convertToConcreteType(obj);

        contact.put(DQSParams.instance.dqsContactStreet, dqsRecord.street);
        contact.put(DQSParams.instance.dqsContactZipCode, dqsRecord.postCode);
        contact.put(DQSParams.instance.dqsContactCity, dqsRecord.city);
        contact.put(DQSParams.instance.dqsContactCountry, dqsRecord.country);       
    }

    public DQSWebServiceWrapper.AddressCheckResponse validateAddress(SObject obj) {
        DQSWebServiceWrapper.Address addressObject = createAddressObject(obj);
        String[] values = new String[] {
            addressObject.firstName,
            addressObject.lastName,
            addressObject.billingPostalCode,
            addressObject.billingStreet,
            addressObject.billingCountry,
            addressObject.billingCity,
            addressObject.billingState,
            addressObject.dunsNumber};

        DQSWebServiceWrapper ws = new DQSWebServiceWrapper(addressObject);
        return ws.validateAddress(ADDRESS_VALIDATION_WF_NAME, ADDRESS_FIELDS_VALIDATION_WF, values);    
    }

    public DQSWebServiceWrapper.DuplicateCheckResponse checkDuplicateInAccounts(SObject obj) {
        DQSWebServiceWrapper.Address addressObject = createAddressObject(obj);
        String[] values = new String[] {
            addressObject.name,
            addressObject.billingPostalCode,
            addressObject.billingStreet,
            addressObject.billingCountry,
            addressObject.billingCity,
            addressObject.billingState,
            addressObject.dunsNumber};

        DQSWebServiceWrapper ws = new DQSWebServiceWrapper(addressObject);
        return ws.checkDuplicate(DUPLICATE_CHECK_ACCOUNT_WF_NAME);      
    }

    public DQSWebServiceWrapper.DuplicateCheckResponse checkDuplicateInContacts(SObject obj) {
        DQSWebServiceWrapper.Address addressObject = createAddressObject(obj);
        String[] values = new String[] {
            addressObject.name,
            addressObject.billingPostalCode,
            addressObject.billingStreet,
            addressObject.billingCountry,
            addressObject.billingCity,
            addressObject.billingState,
            addressObject.dunsNumber};

        DQSWebServiceWrapper ws = new DQSWebServiceWrapper(addressObject);
        return ws.checkDuplicate(DUPLICATE_CHECK_CONTACT_WF_NAME);      
    }   

    public DQSWebServiceWrapper.DuplicateCheckResponse checkDuplicateInLeads(SObject obj) {
        DQSWebServiceWrapper.Address addressObject = createAddressObject(obj);
        String[] values = new String[] {
            addressObject.name,
            addressObject.billingPostalCode,
            addressObject.billingStreet,
            addressObject.billingCountry,
            addressObject.billingCity,
            addressObject.billingState,
            addressObject.dunsNumber};

        DQSWebServiceWrapper ws = new DQSWebServiceWrapper(addressObject);
        return ws.checkDuplicate(DUPLICATE_CHECK_LEAD_WF_NAME);     
    }   

    public Boolean fieldExists(String fieldName) {
        Set<String> objectFields = Schema.SObjectType.Contact.fields.getMap().keySet();
        return objectFields.contains(fieldName);
    }
    
    public List<String> getInputFieldNames() {
        return SObjectUtility.getFieldSetFieldNames(inputFieldsFieldsetName, Contact.SObjectType);
    }

    public List<String> getUpsertFieldNames() {
        return new List<String>();
    }   

    private Contact convertToConcreteType(SObject obj) {
        return (Contact)obj;
    }

    public Boolean searchInAccounts() {
        return DQSParams.instance.contactSearchAccount;
    }

    public Boolean searchInContacts() {
        return DQSParams.instance.contactSearchContact;
    }
    
    public Boolean searchInLeads() {
        return DQSParams.instance.contactSearchLead;
    }

    public Boolean searchOnlyInMyRecords() {
        return DQSParams.instance.contactOnlyMyRecords;
    }

    public Boolean validateAddressNew() {
        return DQSParams.instance.avc_Contact_New;
    }

    public Boolean validateAddressEdit() {
        return DQSParams.instance.avc_Contact_Edit;
    }
}