@isTest
global class MockWebServiceResponseGenerator implements WebServiceMock {
    
    private static final String ENDPOINT_STR = 'http://www.omikron.net/webservices/dataqualityserver/2.0.0/dqserver.asmx';
    private static final String ADDRESS_VALIDATION_WF_NAME = 'RBWF_PostalCorrection';
    private static final String DUPLICATE_CHECK_ACCOUNT_WF_NAME = 'RBWF_DQ_Search_Account';
    private static final String DUPLICATE_CHECK_CONTACT_WF_NAME = 'RBWF_DQ_Search_Contact';
    private static final String DUPLICATE_CHECK_LEAD_WF_NAME = 'RBWF_DQ_Search_Lead';
    private static final String UPSERT_ACCOUNT_WF_NAME = 'RBWF_Upsert_Account';
    private static final String DELETE_ACCOUNT_WF_NAME = 'RBWF_Delete_Account';

    private Boolean hasError;

    public MockWebServiceResponseGenerator(Boolean hasError) {
        this.hasError = hasError;
    }

    global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
        DQSWebService.DoWorkflow_element workflowRequest = (DQSWebService.DoWorkflow_element) request;
        String username = workflowRequest.Username;
        String passwd = workflowRequest.Password;
        String workflowName = workflowRequest.WorkflowName;
        DQSWebService.ArrayOfString fieldNames = workflowRequest.FieldNames;
        DQSWebService.ArrayOfString records = workflowRequest.Records;

        System.assertEquals(username, DQSParams.instance.userName);
        System.assertEquals(passwd, DQSParams.instance.password);
        System.assertEquals(endpoint, ENDPOINT_STR);

        DQSWebService.DoWorkflowResponse_element responseElement = new DQSWebService.DoWorkflowResponse_element();

        if (workflowName.equals(UPSERT_ACCOUNT_WF_NAME)) {
            responseElement.DoWorkflowResult = dummyUpsertAccountCallout(fieldNames.string_x, records.string_x);
        }
        else if (workflowName.equals(DELETE_ACCOUNT_WF_NAME)) {
            responseElement.DoWorkflowResult = dummyDeleteAccountCallout(fieldNames.string_x, records.string_x);
        }

        response.put('response_x', responseElement); 
    }

    private DQSWebService.ArrayOfArrayOfString dummyUpsertAccountCallout(List<String> fields, List<String> values) {
        Set<String> validFields = new Set<String>(AccountService.instance.getUpsertFieldNames());
        System.assert(validFields.containsAll(fields));

        DQSWebService.ArrayOfArrayOfString result = new DQSWebService.ArrayOfArrayOfString();
        DQSWebService.ArrayOfString tempAOS = new DQSWebService.ArrayOfString();
        String resValue = 'Updated';
        if (hasError) {
            resValue = 'Error';
        }
        tempAOS.string_x.addAll(new List<String> { 'ResultDQ', resValue });
        result.ArrayOfString.add(tempAOS);
        tempAOS.string_x.clear();
        tempAOS.string_x.addAll(new List<String> { 'ResultFF', resValue });
        result.ArrayOfString.add(tempAOS);

        return result;
    }

    private DQSWebService.ArrayOfArrayOfString dummyDeleteAccountCallout(List<String> fields, List<String> values) {
        System.assert(fields.size() == 1);
        System.assert(fields[0].equalsIgnoreCase('id'));

        DQSWebService.ArrayOfArrayOfString result = new DQSWebService.ArrayOfArrayOfString();
        result.ArrayOfString.add(new DQSWebService.ArrayOfString());
        return result;
    }

}