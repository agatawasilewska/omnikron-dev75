public class CalloutsService {

    public static String CALL_METHOD_GET = 'GET';
    private static String CALL_METHOD_POST = 'POST';
    private static String CALL_METHOD_DELETE = 'DELETE';
    private static Integer CALL_METHOD_GET_SUCCESS_CODE = 200;
    private static Integer CALL_METHOD_POST_SUCCESS_CODE = 201;
    private static Integer CALL_METHOD_DELETE_SUCCESS_CODE = 204;    

    private static String CALL_ENDPOINT_LOGIN = '/services/oauth2/token';
    private static String CALL_ENDPOINT_USER_TERRITORY = '/services/data/v26.0/sobjects/UserTerritory/';

    private static String CALL_AUTH = 'Authorization';
    private static String CALL_AUTH_BEARER = 'Bearer ';
    private static String CALL_AUTH_BODY = 'grant_type={0}&client_id={1}&client_secret={2}&username={3}&password={4}';
    private static String CALL_AUTH_GRANT_TYPE = 'password';
    private static String CALL_CONTENT_TYPE = 'Content-Type';    
    private static String CALL_CONTENT_TYPE_JSON = 'application/json';    

    /**
     * Singleton implementation
     */
    private static CalloutsService instance   {
        get {
            if (instance == null) {
                instance = new CalloutsService();
            }
            return instance;
        }
    }

    private Map<String, String> mapKey2AccessToken;
    public Boolean catchExceptions { get; set; }

    /**
     * Constructor
     */
    private CalloutsService() {
        this.mapKey2AccessToken = new Map<String, String>();
        this.CatchExceptions = false;
    }

    /**
    @brief  Method to ger Access token.
    @return Access token as string.
    @author Piotr Bardziński
    */
    private static String createAccessToken(String key) {       
        try {
            //return '00D24000000HYiF!AQ4AQDGVFihh.rzqXA5vZJOy6tlio6j7mzHKvLjS4phwKtnf50MDvTTH2BdzkJnlZrJNrvZmSk0GGQsReXPe99U59IvTuvps';
            //return '00D24000000HYiF!AQ4AQApswUadotERMyaR5ISZPeU5p6T3Axf6Iw8csAraGf_LuoAu255uro2kMjgvHCKbvwSLjX9nFdDPbyc4DPuiw_r9Kofl';
            //return '00D24000000HYiF!AQ4AQKiXfYkPWW6Mp8whZZ5PI_wCdW98GZP_L7rgPwpFn155Dm9nby_MPysYlK.YIg6FubDq1TUz0Km2cB4gAVUEoDUwgjOR';
            
            Http h = new Http();
            HttpRequest request = new HttpRequest(); 
            
            request.setEndpoint(URLUtility.getHttpsSalesforceBaseURL() + CALL_ENDPOINT_LOGIN);
            request.setMethod(CALL_METHOD_POST);
            
            CalloutSettings.CalloutConnectionSettings connParams = CalloutSettings.getCalloutSettings(key); 
            String body = String.format(CalloutsService.CALL_AUTH_BODY, new String[]{CalloutsService.CALL_AUTH_GRANT_TYPE, connParams.ClientId, connParams.ClientSecret, connParams.Username,connParams.Password});
            request.setBody(body);
            system.debug('CalloutsService.createAccessToken request=' + request);
            system.debug('CalloutsService.createAccessToken request.endpoint=' + request.getEndpoint());
            system.debug('CalloutsService.createAccessToken request.header.authorization=' + request.getHeader('Authorization'));
            system.debug('CalloutsService.createAccessToken request.body=' + request.getBody());

            HttpResponse response = sendCallout(h, request);

            String jsona = response.getBody();
            CalloutsService.ResponseBody results = new CalloutsService.ResponseBody();
            results = (CalloutsService.ResponseBody)JSON.deserialize(jsona, CalloutsService.ResponseBody.class);
            system.debug('CalloutsService.createAccessToken results=' + results);
            system.debug('CalloutsService.createAccessToken results.access_token=' + results.access_token);
        
            return results.access_token;
        }
        catch (Exception e) {
            system.debug('CalloutsService.createAccessToken exception=' + e.getMessage());
            if (CalloutsService.instance.catchExceptions) {
                return null;
            } else {
                throw e;
            }
        }
    }

    public static String getAccessToken(String key) {
        String accessToken = CalloutsService.instance.mapKey2AccessToken.get(key);
        if (accessToken == null) {
            accessToken = CalloutsService.createAccessToken(key);
            CalloutsService.instance.mapKey2AccessToken.put(key, accessToken);
        }
        return accessToken;
    }


    /**
    @brief  Method to invoke callout.
    @param  h - Http object.
    @param  request - HttpRequest object.
    @return HttpResponse object.
    @author Piotr Bardziński
    */
    public static HttpResponse sendCallout(Http h, HttpRequest request) {
        system.debug('CalloutsService.sendCallout Limits.getCallouts=' + Limits.getCallouts());
        if (Limits.getCallouts() == Limits.getLimitCallouts()) {
            throw new CalloutsServiceException('Callout limit has been exceeded. You cannot use more than ' + Limits.getLimitCallouts() + ' callouts in one transaction.');
            return null;
        }
        else {
            system.debug('CalloutsService.sendCallout request=' + request);
            HttpResponse response = h.send(request);
            system.debug('CalloutsService.sendCallout response=' + response);
            system.debug('CalloutsService.sendCallout response.body=' + response.getBody());
            system.debug('CalloutsService.sendCallout response.header.authorization=' + response.getHeader('Authorization'));
            system.debug('CalloutsService.sendCallout response.headerKeys=' + response.getHeaderKeys());            
            return response;
        }
    }

    public static HttpResponse makeCallGET(String requestURL, String configurationName, String strParameter, SObject objParameter) {   
        String accToken = getAccessToken(configurationName);
        return makeCallGET(requestURL, strParameter, objParameter, accToken);
    }
    
    public static HttpResponse makeCallGET(String requestURL, String strParameter, SObject objParameter, String sessionId) {   
        return makeCall(requestURL, CALL_METHOD_GET, strParameter, objParameter, sessionId);
    }
    
    public static HttpResponse makeCall(String requestURL, String method, String configurationName, String strParameter, SObject objParameter) {  
        String accToken = getAccessToken(configurationName);
        return makeCall(requestURL, method, strParameter, objParameter, accToken);
    }
    
    public static HttpResponse makeCall(String requestURL, String method, String strParameter, SObject objParameter, String sessionId) {   
        
        system.debug('### CalloutsService.makeCall sessionId = ' + sessionId);
        system.debug('CalloutsService.makeCall UserInfo.GetUserId=' + UserInfo.GetUserId());
        system.debug('CalloutsService.makeCall UserInfo.GetUserName=' + UserInfo.GetUserName());
        system.debug('CalloutsService.makeCall strParameter=' + strParameter);
        system.debug('CalloutsService.makeCall objParameter=' + objParameter);
        
        Http h = new Http();
        HttpRequest request = new HttpRequest(); 
        HttpResponse response;
            
        try {
            system.debug('### request: ' + request);
            request.setHeader(CalloutsService.CALL_AUTH, CalloutsService.CALL_AUTH_BEARER + sessionId);
            request.setMethod(method);
            system.debug('### request: ' + request);
            String endPoint = URLUtility.getHttpsSalesforceBaseURL() + requestUrl;
            //String endPoint = 'https://cs86.salesforce.com/' + requestUrl;
            if (String.isNotBlank(strParameter)) {
                endPoint += strParameter;
            }
            
            request.setEndpoint(endPoint);
            if (objParameter != null) {
                request.setBody(JSON.serialize(objParameter));
                request.setHeader(CalloutsService.CALL_CONTENT_TYPE, CalloutsService.CALL_CONTENT_TYPE_JSON);
            }
            system.debug('### endPoint: ' + endPoint);
            
            if (!Test.isRunningTest()) {
                response = CalloutsService.sendCallout(h, request);
                system.debug('CalloutsService.makeCall response=' + response);
                system.debug('CalloutsService.makeCall response.getBody(): ' + response.getBody());
                // 'Created' success code, for POST request = 201, 'No Content' success code, for DELETE request = 204
                // so - depending on method passed as argument, we have to check different status in response object
                // DELETE was sent
                Integer succCode = CalloutsService.CALL_METHOD_DELETE_SUCCESS_CODE;
                if (CalloutsService.CALL_METHOD_GET.equals (method)) {
                    // POST was sent
                    succCode = CalloutsService.CALL_METHOD_GET_SUCCESS_CODE;
                }
                else if (CalloutsService.CALL_METHOD_POST.equals(method)) {
                    // POST was sent
                    succCode = CalloutsService.CALL_METHOD_POST_SUCCESS_CODE;
                }
                else if (CalloutsService.CALL_METHOD_DELETE.equals(method)) {
                    // POST was sent
                    succCode = CalloutsService.CALL_METHOD_DELETE_SUCCESS_CODE;
                } 
                if (response.getStatusCode() != succCode) {
                    throw new CalloutsServiceException('Callout failed: ' + response);
                }
            }
            
            return  response;
        }
        catch (Exception e) {
            system.debug('CalloutsService.makeCall exception=' + e.getMessage());
            if (CalloutsService.instance.catchExceptions) {
                return response;
            }
            else {
                throw e;
            }
        }
    }    

    public static void deleteUserTerritory(Id userTerritoryId, String settingsKey) {
        String accToken = CalloutsService.getAccessToken(settingsKey);
        CalloutsService.makeCall(CALL_ENDPOINT_USER_TERRITORY, CALL_METHOD_DELETE, userTerritoryId, null, accToken);
        System.debug('CalloutsService.createUserTerritory accToken=' + accToken);        
    }

    public static void createUserTerritory(Id userId, Id territoryId, String settingsKey) {
        String accToken = CalloutsService.getAccessToken(settingsKey);
        //UserTerritory newUserTerritory = new UserTerritory(TerritoryId = territoryId, UserId = userId);

        //CalloutsService.makeCall(CALL_ENDPOINT_USER_TERRITORY, CALL_METHOD_POST, null, newUserTerritory, accToken);
        System.debug('CalloutsService.createUserTerritory accToken=' + accToken);
        //return null;
    }

    @future(callout=true)
    public static void createUserTerritory_Future(Id userId, Id territoryId, String settingsKey) {
        CalloutsService.createUserTerritory(userId, territoryId, settingsKey);
    }

    public static void setCatchExceptions(Boolean ce) {
        CalloutsService.instance.catchExceptions = ce;
    }

    /**
    @class  responseBody
    @brief  Helper class for callouts.
    @author Piotr Bardziński
    */
    public class ResponseBody {
        String id;
        String issued_at;
        String instance_url;
        String signature;
        public String access_token {get; set;}
        
    }  
    
    private class CalloutsServiceException extends Exception {}   
}