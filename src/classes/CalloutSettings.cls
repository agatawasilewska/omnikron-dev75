/**
 @brief Helper class used for Callout_Setting__c object
 @author Piotr Bardziński
 @version 1.0
 @date 2016-02-10
 @details Omikron
 */

public class CalloutSettings {
    /**
     * Allowed Names for Callout_Setting__c.
     */
    public static String CALLOUT_SFDC_KEY = 'SFDC';
    public static String CALLOUT_USER_TERRITORY_CREATE_KEY = 'UserTerritoryCreate';
    public static String CALLOUT_REPLACEMENTS_KEY = 'UnReplacement';
    public static String CALLOUT_INTERNAL_REPLACEMENTS_KEY = 'UnInternalReplacement';

    public static CalloutConnectionSettings getCalloutSettings(String key) {
        List<Callout_Setting__c> lCalloutSettings = [SELECT Technical_User__r.UserName, Technical_User_Password__c, Client_Id__c, Client_secret__c 
                                FROM    Callout_Setting__c
                                WHERE   Name = :key];

        if (lCalloutSettings == null || lCalloutSettings.size() == 0) {
            throw new CalloutSettingsException(String.format('Callout_Setting__c for {0} not defined.', new String[]{key}));
        }

        return new CalloutConnectionSettings(lCalloutSettings[0]);
    }

    public class CalloutConnectionSettings {
        public String Username { get; protected set; }
        public String Password { get; protected set; }
        public String ClientId { get; protected set; }
        public String ClientSecret { get; protected set; }

        public CalloutConnectionSettings(Callout_Setting__c cs) {
            this.Username = cs.Technical_User__r.UserName;
            this.Password = cs.Technical_User_Password__c;
            this.ClientId = cs.Client_Id__c;
            this.ClientSecret = cs.Client_secret__c;            
        }
    }

    private class CalloutSettingsException extends Exception {}
}