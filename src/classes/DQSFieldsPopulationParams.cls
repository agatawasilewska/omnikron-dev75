public without sharing class DQSFieldsPopulationParams {
    /*** Instance variables ***/
    private Map<String, Map<String, String>> mapObjectToField { get; set; }

    /*** Instance methods ***/
    /* Constructors */
    private DQSFieldsPopulationParams() {
        Map<String, DQSFieldsPopulationParam__c> mapParams;
        if (Test.isRunningTest()) {
            mapParams = createDQSFieldsPopulationParams(false);
        }
        else {
            mapParams = DQSFieldsPopulationParam__c.getAll();
        }   
        constructObjectToFieldMap(mapParams);
    }

    /*** Other Static variables ***/
    private static DQSFieldsPopulationParams instance   {   // cache instance
        get {
            if (instance == null) {
                instance = new DQSFieldsPopulationParams();
            }
            return instance;
        }
        //set {}
    }

    private void constructObjectToFieldMap(Map<String, DQSFieldsPopulationParam__c> mapParams) {
        mapObjectToField = new Map<String, Map<String, String>>();
        for (DQSFieldsPopulationParam__c param : mapParams.values()) {
            Map<String, String> objectFields = mapObjectToField.get(param.Object_Name__c);
            if (objectFields == null) {
                objectFields = new Map<String, String>();
                mapObjectToField.put(param.Object_Name__c, objectFields);
            }
            objectFields.put(param.Field_Name__c, param.Field_Id__c);
        }
    }

    public static String getFieldId(SObjectType sObjectType, String fieldName) {
        String res;
        Map<String, String> objectFields = instance.mapObjectToField.get(sObjectType.getDescribe().getName());
        if (objectFields != null) {
            res = objectFields.get(fieldName);
        }
        return res;
    }

    /**
    @brief  Method to delete all parameters from DQS Fields Population Parameters custom setting.
    @return Integer - number of deleted parameters.
    @author Piotr Bardziński
    */
    public static Integer deleteDQSFieldsPopulationParams() {
        list<DQSFieldsPopulationParam__c> lParams = DQSFieldsPopulationParam__c.getAll().values(); 

        if (lParams != null && lParams.size() > 0) {
            delete lParams;
            return lParams.size();
        }
        return 0;
    }

    /**
    @brief  Method to create all  parameters (with default values) from DQS Fields Population Parameters custom setting with save to Database.
    @return map<String, DQSFieldsPopulationParam__c> - map of created parameters.
    @author Piotr Bardziński
    */
    public static Map<String, DQSFieldsPopulationParam__c> createDQSFieldsPopulationParams() {
        return createDQSFieldsPopulationParams(true);
    }
    
    /**
    @brief  Method to create all  parameters (with default values) from DQS Fields Population Parameters custom setting with optional save to Database.
    @param  withInsert - Boolean parameter which decides at saving data.
    @return map<String, DQSFieldsPopulationParam__c> - list of created parameters.
    @author Piotr Bardziński
    */
    public static Map<String, DQSFieldsPopulationParam__c> createDQSFieldsPopulationParams(Boolean withInsert) {
        Map<String, DQSFieldsPopulationParam__c> fieldParams = new Map<String, DQSFieldsPopulationParam__c>();
        fieldParams.put('Contact.LastName', new DQSFieldsPopulationParam__c(Name='Contact.LastName', Field_Id__c='name_lastcon2', Field_Name__c='LastName', Object_Name__c='Contact'));
        fieldParams.put('Contact.MailingState', new DQSFieldsPopulationParam__c(Name='Contact.MailingState', Field_Id__c='con19state', Field_Name__c='MailingState', Object_Name__c='Contact'));
        fieldParams.put('Contact.MailingStreet', new DQSFieldsPopulationParam__c(Name='Contact.MailingStreet', Field_Id__c='con19street', Field_Name__c='MailingStreet', Object_Name__c='Contact'));
        fieldParams.put('Contact.FirstName', new DQSFieldsPopulationParam__c(Name='Contact.FirstName', Field_Id__c='name_firstcon2', Field_Name__c='FirstName', Object_Name__c='Contact'));
        fieldParams.put('Contact.MailingPostalCode', new DQSFieldsPopulationParam__c(Name='Contact.MailingPostalCode', Field_Id__c='con19zip', Field_Name__c='MailingPostalCode', Object_Name__c='Contact'));
        fieldParams.put('Contact.MailingCity', new DQSFieldsPopulationParam__c(Name='Contact.MailingCity', Field_Id__c='con19city', Field_Name__c='MailingCity', Object_Name__c='Contact'));
        fieldParams.put('Contact.MailingCountry', new DQSFieldsPopulationParam__c(Name='Contact.MailingCountry', Field_Id__c='con19country', Field_Name__c='MailingCountry', Object_Name__c='Contact'));
        fieldParams.put('Lead.State', new DQSFieldsPopulationParam__c(Name='Lead.State', Field_Id__c='lea16state', Field_Name__c='State', Object_Name__c='Lead'));
        fieldParams.put('Lead.Country', new DQSFieldsPopulationParam__c(Name='Lead.Country', Field_Id__c='lea16country', Field_Name__c='Country', Object_Name__c='Lead'));
        fieldParams.put('Lead.City', new DQSFieldsPopulationParam__c(Name='Lead.City', Field_Id__c='lea16city', Field_Name__c='City', Object_Name__c='Lead'));
        fieldParams.put('Lead.FirstName', new DQSFieldsPopulationParam__c(Name='Lead.FirstName', Field_Id__c='name_firstlea2', Field_Name__c='FirstName', Object_Name__c='Lead'));
        fieldParams.put('Lead.Street', new DQSFieldsPopulationParam__c(Name='Lead.Street', Field_Id__c='lea16street', Field_Name__c='Street', Object_Name__c='Lead'));
        fieldParams.put('Lead.PostalCode', new DQSFieldsPopulationParam__c(Name='Lead.PostalCode', Field_Id__c='lea16zip', Field_Name__c='PostalCode', Object_Name__c='Lead'));
        fieldParams.put('Lead.LastName', new DQSFieldsPopulationParam__c(Name='Lead.LastName', Field_Id__c='name_lastlea2', Field_Name__c='LastName', Object_Name__c='Lead'));
        fieldParams.put('Lead.Latitude', new DQSFieldsPopulationParam__c(Name='Lead.Latitude', Field_Id__c='00N0Y00000PK8Jx', Field_Name__c='Latitude__c', Object_Name__c='Lead'));
        fieldParams.put('Lead.Longitude', new DQSFieldsPopulationParam__c(Name='Lead.Longitude', Field_Id__c='00N0Y00000PKBGo', Field_Name__c='Longitude__c', Object_Name__c='Lead'));
        fieldParams.put('Account.BillingStreet', new DQSFieldsPopulationParam__c(Name='Account.BillingStreet', Field_Id__c='acc17street', Field_Name__c='BillingStreet', Object_Name__c='Account'));
        fieldParams.put('Account.BillingCountry', new DQSFieldsPopulationParam__c(Name='Account.BillingCountry', Field_Id__c='acc17country', Field_Name__c='BillingCountry', Object_Name__c='Account'));
        fieldParams.put('Account.BillingCity', new DQSFieldsPopulationParam__c(Name='Account.BillingCity', Field_Id__c='acc17city', Field_Name__c='BillingCity', Object_Name__c='Account'));
        fieldParams.put('Account.Name', new DQSFieldsPopulationParam__c(Name='Account.Name', Field_Id__c='acc2', Field_Name__c='Name', Object_Name__c='Account'));
        fieldParams.put('Account.BillingState', new DQSFieldsPopulationParam__c(Name='Account.BillingState', Field_Id__c='acc17state', Field_Name__c='BillingState', Object_Name__c='Account'));
        fieldParams.put('Account.BillingPostalCode', new DQSFieldsPopulationParam__c(Name='Account.BillingPostalCode', Field_Id__c='acc17zip', Field_Name__c='BillingPostalCode', Object_Name__c='Account'));

        if (withInsert) {
            insert fieldParams.values();
        }

        return fieldParams;
    }
}