public class PageControllerUtility {
    /**
     *  Class for defining columns behaviour on lists displayed on pages.
     */         
    public class ColumnDefinition {
        /**
         *  Name of field that is displayed in column (may be SObject or wrapper field name).
         */         
        public string DisplayField { get; protected set; }
        /**
         *  Name of field that is displayed as URL source (field is presented as link of not empty).
         */         
        public string IdField { get; protected set; }       
        /**
         *  Title of the column.
         */         
        public string HeaderLabel { get; protected set; }
        /**
         *  Name of SObject field that is used for sorting.
         */         
        //public string OrderFieldName { get; protected set; }
        //public string ActionObject { get; protected set; }
        //public string ActionFieldName { get; protected set; }
        /**
         *  css style added to column
         */     
        public string StyleClass { get; protected set; }
        /**
         *  css style added to cell in table (rows with data - not header)
         */     
        public string StyleClassCell { get; protected set; }        
        /**
         *  Is field editable or read only.
         */
        public boolean ReadOnly { get; protected set; } 
        /**
         *  Constructor.
         */
        public ColumnDefinition(String displayField, String headerLabel) {
            this(displayField, null, headerLabel, null, null, true);
        }
        /**
         *  Constructor.
         */
        public ColumnDefinition(String displayField, String idField, String headerLabel) {
            this(displayField, idField, headerLabel, null, null, true);
        }   
        /**
         *  Constructor.
         */
        public ColumnDefinition(String displayField, String idField, String headerLabel, String styleClass) {
            this(displayField, idField, headerLabel, styleClass, null, true);
        }
        /**
         *  Constructor.
         */
        public ColumnDefinition(String displayField, String idField, String headerLabel/*, String orderFieldName*/, String styleClass, String styleClassCell, boolean readOnly) {
            this.DisplayField = displayField;
            this.IdField = idField;
            this.HeaderLabel = headerLabel;
            //this.OrderFieldName = orderFieldName;
            this.StyleClass = styleClass;
            this.StyleClassCell = styleClassCell;
            this.ReadOnly = readOnly;
        }
    }
}