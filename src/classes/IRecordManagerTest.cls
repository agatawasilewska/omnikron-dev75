@isTest
private class IRecordManagerTest {
    /*
    DQSWebServiceWrapper.Address createAddressObject(SObject obj);
    void updateAddressWithDQSData(SObject obj, DQSWebServiceWrapper.AddressCheckResponseData dqsRecord);
    DQSWebServiceWrapper.AddressCheckResponse validateAddress(SObject obj);
    DQSWebServiceWrapper.DuplicateCheckResponse checkDuplicate(SObject obj);

    DQSWebServiceWrapper.DuplicateCheckResponse checkDuplicateInAccounts(SObject obj);
    DQSWebServiceWrapper.DuplicateCheckResponse checkDuplicateInContacts(SObject obj);
    DQSWebServiceWrapper.DuplicateCheckResponse checkDuplicateInLeads(SObject obj);

     */
    private static testMethod void isOmikronFieldChangedTest() {
        isOmikronFieldChangedTestUtility('Account');
        // isOmikronFieldChangedTestUtility('Contact');
        // isOmikronFieldChangedTestUtility('Lead');
    }
    
    private static IRecordManager getManager(String objType) {
        Type baseType = Type.forName(objType);

        if (baseType == Account.class) {
            return AccountService.instance;
        } else if (baseType == Contact.class) {
            return ContactService.instance;
        } else if (baseType == Lead.class) {
            return LeadService.instance;
        } 

        return null;
    }

    private static testMethod void createAddressObjectTest() {
        {
            Account sObj = new Account();
            DQSWebServiceWrapper.Address address1 = new DQSWebServiceWrapper.Address(sObj);
            DQSWebServiceWrapper.Address address2 = AccountService.instance.createAddressObject(sObj);
            System.assertEquals(address1, address2);
        }

        {
            Contact sObj = new Contact();
            DQSWebServiceWrapper.Address address1 = new DQSWebServiceWrapper.Address(sObj);
            DQSWebServiceWrapper.Address address2 = ContactService.instance.createAddressObject(sObj);
            System.assertEquals(address1, address2);
        }

        {
            Lead sObj = new Lead();
            DQSWebServiceWrapper.Address address1 = new DQSWebServiceWrapper.Address(sObj);
            DQSWebServiceWrapper.Address address2 = LeadService.instance.createAddressObject(sObj);
            System.assertEquals(address1, address2);
        }
    }

    static void isOmikronFieldChangedTestUtility(String objType) {
        Type baseType = Type.forName(objType);
        IRecordManager manager;
        sObject sObj = (sObject)baseType.newInstance();

        if (baseType == Account.class) {
            manager = AccountService.instance;
            sObj.put('Name', 'Test' + objType);
        } else if (baseType == Contact.class) {
            manager = ContactService.instance;
            sObj.put('FirstName', 'Test' + objType);
            sObj.put('LastName', 'Test' + objType);
        } else if (baseType == Lead.class) {
            manager = LeadService.instance;
            sObj.put('Name', 'Test' + objType);
        } else {
            System.assert(false, 'Wrong object type given to test fixture.');
        }

        ObjectTestUtilities objUtils = new ObjectTestUtilities(objType);
                

        Set<String> usedFieldNames = new Set<String>(manager.getUpsertFieldNames());
        System.assert(!usedFieldNames.isEmpty(), objType + 'Service does not use any fields for upsert!');
        System.debug(usedFieldNames);
        List<Schema.SObjectField> otherFieldNamesSchema = Schema.getGlobalDescribe().get(objType).getDescribe().fields.getMap().values();
        Set<String> otherFieldNames = new Set<String>();
        for (Schema.SObjectField field : otherFieldNamesSchema) {
            otherFieldNames.add(field.getDescribe().getName());
        }
        otherFieldNames.removeAll(usedFieldNames);
        System.debug(otherFieldNames);
        
        insert sObj;

        Test.startTest();
        for (String field : usedFieldNames) {
            if (!objUtils.isFieldEditable(field)) {
                continue;
            }
            sObject testSObj = sObj.clone(true, true, true, true);
            while (testSObj.get(field) == sObj.get(field)) {
                testSObj.put(field, objUtils.getRandomFieldValue(field));
            }
            System.assert(manager.isOmikronFieldChanged(sObj, testSObj), field + ' didn\'t signal change in ' + objType);
        }
        if (!otherFieldNames.isEmpty()) {
            for (String field : otherFieldNames) {
                if (!objUtils.isFieldEditable(field)) {
                   continue;
                }
                sObject testSObj = sObj.clone(true, true, true, true);
                while (testSObj.get(field) == sObj.get(field)) {
                    testSObj.put(field, objUtils.getRandomFieldValue(field));
                }
                System.assert(!manager.isOmikronFieldChanged(sObj, testSObj), field + ' signalled change in ' + objType);
            }
        }
        Test.stopTest();
    }

    private static testMethod void fieldExistsTest() {
        List<String> testTypes = new List<String> { 'Account', 'Contact', 'Lead' };
        for (String objType : testTypes) {
            Set<String> fieldNames = Schema.getGlobalDescribe().get(objType).getDescribe().fields.getMap().keySet();
            IRecordManager manager = getManager(objType);
            System.assert(manager != null);
            for (String field : fieldNames) {
                System.assert(manager.fieldExists(field), field + ' does not exist in ' + objType);
            }
        }
    }

    private static testMethod void configFieldsTest() {
        List<String> testTypes = new List<String> { 'Account', 'Contact', 'Lead' };
        for (String objType : testTypes) {
            IRecordManager manager = getManager(objType);
            System.assert(manager != null);
            System.assert(manager.searchInAccounts());
            System.assert(manager.searchInContacts());
            System.assert(manager.searchInLeads());
            System.assert(!manager.searchOnlyInMyRecords());
            System.assert(manager.validateAddressNew());
            System.assert(manager.validateAddressEdit());
        }
    }
}