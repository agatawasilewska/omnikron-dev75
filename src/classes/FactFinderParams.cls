/**
 @file      FactFinderParams
 @author    Piotr Bardziński
 @version   2.01
 @date      2016-02-02
 @brief     Class used to manage Account Fact-Finder parameters.
 @details
 @section   DESCRIPTION
 Omikron
*/

public without sharing class FactFinderParams {

    /* Exception */
    private class FactFinderParamsException extends Exception {}
    
    /*** Static variables ***/
    /* Names of parameters stored in Custom Setting */
    private static final String PN_FF_USERNAME = 'Fact-Finder Username';
    private static final String PN_FF_PASSWORD = 'Fact-Finder Password';
    private static final String PN_FF_PROTOCOL = 'Fact-Finder Protocol';
    private static final String PN_FF_ENDPOINT = 'Fact-Finder Endpoint';
    private static final String PN_FF_PREFIX = 'Fact-Finder Prefix';
    private static final String PN_FF_SUFFIX = 'Fact-Finder Suffix';
    private static final String PN_FF_PN_USERNAME = 'Fact-Finder Username param name';
    private static final String PN_FF_PN_PASSWORD = 'Fact-Finder Password param name';
    private static final String PN_FF_PN_TIMESTAMP = 'Fact-Finder Timestamp param name';
    private static final String PN_FF_PROXY_ENDPOINT = 'Fact-Finder Proxy Endpoint';
    private static final String PN_FF_PROXY_USERNAME = 'Fact-Finder Proxy Username param name';
    private static final String PN_FF_PROXY_PASSWORD = 'Fact-Finder Proxy Password param name';
    
    /*** Instance variables ***/
    private map<String, FactFinderParam__c> mapParams {get; set;}
    
    /*** Instance methods ***/
    /* Constructors */
    private FactFinderParams() {

        if (Test.isRunningTest()) {
            this.mapParams = createFactFinderParams(false);
            
        }
        else {
            this.mapParams = FactFinderParam__c.getAll();
        }   
        
    }  
    
    /*** Other Static variables ***/
    private static FactFinderParams instance   {   // cache instance
        get {
            if (instance == null) {
                instance = new FactFinderParams();
            }
            return instance;
        }
        //set {}
    }

    public static String FF_USERNAME {
        get {
            return getValueFromConfig(PN_FF_USERNAME);
        }
    }

    public static String FF_PASSWORD {
        get {
            return getValueFromConfig(PN_FF_PASSWORD);
        }
    }

    public static String FF_PROTOCOL {
        get {
            return getValueFromConfig(PN_FF_PROTOCOL);
        }
    }

    public static String FF_ENDPOINT {
        get {
            return getValueFromConfig(PN_FF_ENDPOINT);
        }
    }

    public static String FF_PREFIX {
        get {
            return getValueFromConfig(PN_FF_PREFIX);
        }
    }

    public static String FF_SUFFIX {
        get {
            return getValueFromConfig(PN_FF_SUFFIX);
        }
    }

    public static String FF_PN_USERNAME {
        get {
            return getValueFromConfig(PN_FF_PN_USERNAME);
        }
    }

    public static String FF_PN_PASSWORD {
        get {
            return getValueFromConfig(PN_FF_PN_PASSWORD);
        }
    }

    public static String FF_PN_TIMESTAMP {
        get {
            return getValueFromConfig(PN_FF_PN_TIMESTAMP);
        }
    }

    public static String FF_PROXY_ENDPOINT {
        get {
            return getValueFromConfig(PN_FF_PROXY_ENDPOINT);
        }
    }

    public static String FF_PROXY_USERNAME {
        get {
            return getValueFromConfig(PN_FF_PROXY_USERNAME);
        }
    }

    public static String FF_PROXY_PASSWORD {
        get {
            return getValueFromConfig(PN_FF_PROXY_PASSWORD);
        }
    }

    public static String FACT_FINDER_URI {
        get {
            FACT_FINDER_URI = FF_PROTOCOL + '://' + FF_ENDPOINT;
            Long timeStamp = System.currentTimeMillis();
            FACT_FINDER_URI += '?' + FF_PN_TIMESTAMP + '=' + timeStamp;
            FACT_FINDER_URI += '&' + FF_PN_USERNAME + '=' + FF_USERNAME;
            String stringToEncode = FF_PREFIX + timeStamp + FF_PASSWORD + FF_SUFFIX;
            system.debug('### FF_PASSWORD: ' + FF_PASSWORD);
            system.debug('### stringToEncode: ' + stringToEncode);
            String finalPassword = EncodingUtil.convertToHex(Crypto.generateDigest('MD5', Blob.valueOf(stringToEncode)));
            system.debug('### finalPassword: ' + finalPassword);
            FACT_FINDER_URI += '&' + FF_PN_PASSWORD + '=' + finalPassword;
            system.debug('### FACT_FINDER_URI: ' + FACT_FINDER_URI);
            
            return FACT_FINDER_URI;
        }
    }

    public static String FACT_FINDER_URI_WITH_SSID {
        get {
            FACT_FINDER_URI_WITH_SSID = FF_PROTOCOL + '://' + FF_ENDPOINT;
            FACT_FINDER_URI_WITH_SSID += '?ssid=';
            /*//Long timeStamp = System.currentTimeMillis();
            FACT_FINDER_URI += '?' + FF_PN_TIMESTAMP + '=' + timeStamp;
            //FACT_FINDER_URI += '&' + FF_PN_USERNAME + '=' + FF_USERNAME;
            String stringToEncode = FF_PREFIX + timeStamp + FF_PASSWORD + FF_SUFFIX;
            system.debug('### FF_PASSWORD: ' + FF_PASSWORD);
            system.debug('### stringToEncode: ' + stringToEncode);
            String finalPassword = EncodingUtil.convertToHex(Crypto.generateDigest('MD5', Blob.valueOf(stringToEncode)));
            system.debug('### finalPassword: ' + finalPassword);
            FACT_FINDER_URI += '&' + FF_PN_PASSWORD + '=' + finalPassword;
            system.debug('### FACT_FINDER_URI: ' + FACT_FINDER_URI);*/
            
            return FACT_FINDER_URI_WITH_SSID;
        }
    }
    
    public static String FACT_FINDER_PROXY_URI {
        get {
            //FACT_FINDER_PROXY_URI = FF_PROTOCOL + '://' + FF_PROXY_ENDPOINT;
            FACT_FINDER_PROXY_URI = 'http://' + FF_PROXY_ENDPOINT;
            FACT_FINDER_PROXY_URI += '/' + FF_PROXY_USERNAME;
            FACT_FINDER_PROXY_URI += '/' + FF_PROXY_PASSWORD + '/' ;
            system.debug('### FACT_FINDER_PROXY_URI: ' + FACT_FINDER_PROXY_URI);
            
            return FACT_FINDER_PROXY_URI;
        }
    }   
    
   /*** Static methods ***/
    
    /**
    @brief  Method to get parameter value from Account Fact-Finder Parameters custom setting based on its name.
    @param  name - string parameter name.
    @return String value of parameter.
    @author Piotr Bardziński
    */
    private static String getValueFromConfig(String name) {
        try {
            String param = instance.mapParams.get(name).Value__c;
            if (param == null) {
                param = '';
            }
            else {
                param = param.trim();
            }
            return param;
        }
        catch (NullPointerException e) {
            String errorMessage = 'FactFinderParams.getValueFromConfig: There is no defined parameter: \'' + name + '\' in Account Fact-Finder Parameters Custom Setting.';
            system.debug(LoggingLevel.ERROR, '### ' + errorMessage);
            throw new FactFinderParamsException(errorMessage);
            return null;
        }
    }
    
    /**
    @brief  Method to delete all parameters from Account Fact-Finder Parameters custom setting.
    @return Integer - number of deleted parameter.
    @author Piotr Bardziński
    */
    public static Integer deleteFactFinderParams() {

        list<FactFinderParam__c> lParams = FactFinderParam__c.getAll().values(); 

        if (lParams != null && lParams.size() > 0) {
            delete lParams;
            return lParams.size();
        }
        return 0;
    }

    /**
    @brief  Method to create all  parameters (with default values) from Account Fact-Finder Parameters custom setting with save to Database.
    @return map<String, FactFinderParam__c> - map of created parameters.
    @author Piotr Bardziński
    */
    public static map<String, FactFinderParam__c> createFactFinderParams() {
        return createFactFinderParams(true);
    }
    
    /**
    @brief  Method to create all  parameters (with default values) from Account Fact-Finder Parameters custom setting with optional save to Database.
    @param  withInsert - Boolean parameter which decides at saving data.
    @return map<String, FactFinderParam__c> - list of created parameters.
    @author Piotr Bardziński
    */
    public static map<String, FactFinderParam__c> createFactFinderParams(Boolean withInsert) {
        
        map<String, FactFinderParam__c> mapParams = new map<String, FactFinderParam__c>();
        
        /* Searching conditions */                                      
        mapParams.put(PN_FF_USERNAME,
                        new FactFinderParam__c( Name = PN_FF_USERNAME,
                                                        Value__c = 'salesforce',
                                                        Description__c = 'Fact-Finder username.'));
        mapParams.put(PN_FF_PASSWORD,
                        new FactFinderParam__c( Name = PN_FF_PASSWORD,
                                                        //Value__c = 'd8118f1bb6bd9998031053176a2c4bee',
                                                        Value__c = '505c05efd44239b4535344090cdcc5bd', // 2YwWwfi61THb38TSkeRg
                                                        Description__c = 'Fact-Finder username\'s password encrypted by MD5 method.'));

        mapParams.put(PN_FF_PROTOCOL,
                        new FactFinderParam__c( Name = PN_FF_PROTOCOL,
                                                        Value__c = 'https',
                                                        Description__c = 'Fact-Finder protocol (http or https).'));
                                                                       
        mapParams.put(PN_FF_ENDPOINT,
                        new FactFinderParam__c( Name = PN_FF_ENDPOINT,
                                                        Value__c = 'dev6.fact-finder.de/salesforce-integration/auth/index.html',
                                                        Description__c = 'Fact-Finder integration endpoint (URI).'));
                                                                       
        mapParams.put(PN_FF_PREFIX,
                        new FactFinderParam__c( Name = PN_FF_PREFIX,
                                                        Value__c = 'FACT-FINDER',
                                                        Description__c = 'Fact-Finder prefix for encoded string.'));
                                                                       
        mapParams.put(PN_FF_SUFFIX,
                        new FactFinderParam__c( Name = PN_FF_SUFFIX,
                                                        //Value__c = 'FACT-FINDER',
                                                        Value__c = 'SALESFORCE',
                                                        Description__c = 'Fact-Finder suffix for encoded string.'));
                                                                       
        mapParams.put(PN_FF_PN_USERNAME,
                        new FactFinderParam__c( Name = PN_FF_PN_USERNAME,
                                                        Value__c = 'username',
                                                        Description__c = 'Fact-Finder endpoint username parameter.'));
                                                                       
        mapParams.put(PN_FF_PN_PASSWORD,
                        new FactFinderParam__c( Name = PN_FF_PN_PASSWORD,
                                                        Value__c = 'password',
                                                        Description__c = 'Fact-Finder endpoint password parameter.'));
                                                                       
        mapParams.put(PN_FF_PN_TIMESTAMP,
                        new FactFinderParam__c( Name = PN_FF_PN_TIMESTAMP,
                                                        Value__c = 'timestamp',
                                                        Description__c = 'Fact-Finder endpoint timestamp parameter.'));
                                                        
        mapParams.put(PN_FF_PROXY_ENDPOINT,
                        new FactFinderParam__c( Name = PN_FF_PROXY_ENDPOINT,
                                                        Value__c = 'dev6.fact-finder.de/ff-proxy/auth/login',
                                                        Description__c = 'Fact-Finder proxy endpoint.'));
                                                                       
        mapParams.put(PN_FF_PROXY_USERNAME,
                        new FactFinderParam__c( Name = PN_FF_PROXY_USERNAME,
                                                        Value__c = 'factfinder',
                                                        Description__c = 'Fact-Finder proxy endpoint username.'));
                                                                       
        mapParams.put(PN_FF_PROXY_PASSWORD,
                        new FactFinderParam__c( Name = PN_FF_PROXY_PASSWORD,
                                                        Value__c = 'password',
                                                        Description__c = 'Fact-Finder proxy endpoint password.'));
                
        if (withInsert) {
            insert mapParams.Values();
        }
        return mapParams;
    }

}