public with sharing class NewContactWizardDQSController {
    public IRecordManager recordManager { get; protected set; }
    public SObject currentObject { get; protected set; }

    public NewContactWizardDQSController(ApexPages.StandardController std) {
        recordManager = ContactService.instance;    
        currentObject = std.getRecord();
    }
}