public interface IRecordManager {
    Boolean isOmikronFieldChanged(SObject oldObj, SObject newObj);
    
    DQSWebServiceWrapper.Address createAddressObject(SObject obj);
    void updateAddressWithDQSData(SObject obj, DQSWebServiceWrapper.AddressCheckResponseData dqsRecord);
    DQSWebServiceWrapper.AddressCheckResponse validateAddress(SObject obj);
    DQSWebServiceWrapper.DuplicateCheckResponse checkDuplicateInAccounts(SObject obj);
    DQSWebServiceWrapper.DuplicateCheckResponse checkDuplicateInContacts(SObject obj);
    DQSWebServiceWrapper.DuplicateCheckResponse checkDuplicateInLeads(SObject obj);
    Boolean fieldExists(String fieldName);

    List<String> getInputFieldNames();
    List<String> getUpsertFieldNames();

    Boolean searchInAccounts();
    Boolean searchInContacts();
    Boolean searchInLeads();
    Boolean searchOnlyInMyRecords();
    Boolean validateAddressNew();
    Boolean validateAddressEdit();
}