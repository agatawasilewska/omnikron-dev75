<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_latitude</fullName>
        <field>Latitude</field>
        <formula>VALUE( SUBSTITUTE(Latitude__c, &apos;,&apos;, &apos;.&apos;) )</formula>
        <name>Populate latitude</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_longitude</fullName>
        <field>Longitude</field>
        <formula>VALUE( SUBSTITUTE(Longitude__c, &apos;,&apos;, &apos;.&apos;) )</formula>
        <name>Populate longitude</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate lat and lon</fullName>
        <actions>
            <name>Populate_latitude</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Populate_longitude</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2) OR (3 AND 4)</booleanFilter>
        <criteriaItems>
            <field>Lead.Latitude</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Latitude__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Latitude</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Latitude__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
