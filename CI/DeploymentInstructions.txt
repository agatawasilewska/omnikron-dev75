1. Set up Ant and make sure You have:
   A. SF Migration Tools installed and build.xml file has correct path
   B. Working credentials (password needs security token as well) in access.properties
2. Deploy calling 'ant deployDeplTest' in cmd.exe
3. After successful deployment open Development Console go to 'Debug'->'Open Execute Anonymous Window'
4. Execute following code:
   `DQSParams.initializeParams();
    DQSFieldsPopulationParams.createDQSFieldsPopulationParams();`
    